<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu_controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model("Menu");
        $this->load->library('session');
    }

    public function getMenu()
    {
        $i = 1;
        foreach ($this->Menu->obtenerMenu($this->session->userdata('rol')) as $menu_model) {
            $menu_controller[$i]['idItem'] = $menu_model->idItem;
            $menu_controller[$i]['icono'] = $menu_model->icono;
            $menu_controller[$i]['nombre'] = $menu_model->nombre;
            $menu_controller[$i]['direccion'] = $menu_model->direccion;
            $menu_controller[$i]['item'] = $menu_model->item;
            $i++;
        }

        $menu['url'] = base_url() . 'index.php/Menu/getMenu';
        echo json_encode($menu_controller);
    }

}
