<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Seccional_C extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model('Seccional');
    }

    public function listar()
    {
        $listar = $this->Seccional->listar();
        if (!empty($listar) || $listar != false) {
            echo json_encode($listar);
        } else {
            echo '-1';
        }
    }

}
