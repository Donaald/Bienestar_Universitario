<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Semilleros_C extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('Usuario');
        $this->load->model('Imagen');
        $this->load->model('Semillero');
        $this->load->model('Escenario');
        $this->load->library('session');
    }

    public function index()
    {

    }

    public function crear()
    {
        $docente["docente"] = $this->Usuario->obtener_docente();
        $this->load->view('Cultura/Head');
        $this->load->view('Cultura/NavBar');
        $this->load->view('Cultura/CrearSemillero', $docente);
        $this->load->view('Cultura/Footer');
    }

    public function registarSemillero()
    {
        $this->form_validation->set_rules('nombre', 'Nombre', 'required|max_length[250]');
        $this->form_validation->set_rules('Contenido_Tematico', 'Contenido tematico', 'required');
        $this->form_validation->set_rules('docente', 'Docente', 'required');
        if (empty($_FILES['Imagen']['name'])) {
            $this->form_validation->set_rules('Imagen', 'Imagen', 'required');
        }

        $this->form_validation->set_message('required', '%s es obligatorio.');
        $this->form_validation->set_message('numeric', '%s debe ser numérico.');
        $this->form_validation->set_message('max_length', '%s exede maximo caracteres requeridos.');
        $this->form_validation->set_message('Imagen', "%s no selecciono ninguna imagen");

        if ($this->form_validation->run() == true) {
            if ($this->Imagen->subirImagen('Imagen') == "subio la imagen y esta correcta") {
                $valores = array(
                    'nombre' => $this->input->post('nombre'),
                    'Contenido_Tematico' => $this->input->post('Contenido_Tematico'),
                    'imagen' => $this->Imagen->getNombre("file_name"),
                    'docente' => $this->input->post('docente'),
                    'periodo_idperiodo' => $this->session->userdata('periodo'),
                    'seccional_idseccional' => $this->session->userdata('seccional'),                  
                );
                $this->Semillero->crear($valores);
                redirect('/index.php/Semilleros_C/listar');
            } elseif ($this->Imagen->subirImagen('Imagen') == "subio la imagen pero con errores") {
                $docente["docente"] = $this->Usuario->obtener_docente();
                $this->load->view('Cultura/Head');
                $this->load->view('Cultura/NavBar');
                $this->load->view('Cultura/CrearSemillero', $docente);
                $this->load->view('Cultura/Footer');
            }
        } else {
            $docente["docente"] = $this->Usuario->obtener_docente();
            $this->load->view('Cultura/Head');
            $this->load->view('Cultura/NavBar');
            $this->load->view('Cultura/CrearSemillero', $docente);
            $this->load->view('Cultura/Footer');
        }

    }
    public function listar()
    {
        $datos['semillero'] = $this->Semillero->listar($this->session->userdata('periodo'), $this->session->userdata('seccional'));
        $this->load->view('Cultura/Head');
        $this->load->view('Cultura/NavBar');
        $this->load->view('Cultura/ListarSemillero', $datos);
        $this->load->view('Cultura/Footer');
    }

    public function listar_ajax()
    {
        $datos = $this->Semillero->listar($this->session->userdata('periodo'), $this->session->userdata('seccional'));
        if (!empty($datos) || $datos != false) {
            $j = 0;
            foreach ($datos as $tabla) {
                $data['data'][$j]['Nombre'] = $tabla->nombre;
                $data['data'][$j]['Inscritos'] = $tabla->inscritos;
                $data['data'][$j]['Descripcion'] = $tabla->contenido_tematico;
                $data['data'][$j]['Docente'] = $tabla->nombre_docente . ' ' . $tabla->apellido_docente;
                $data['data'][$j]['Inscritos_Nombre'] = '<td><div class="color-palette-set" align="center"><a id="inscritos" href="' . base_url() . 'index.php/Semilleros_C/inscritos/' . $tabla->idsemillero . '"><div class="bg-red-active color-palette"><span class="fa fa-users"></span></div></a></div></td>';
                $data['data'][$j]['Eliminar'] = '<td><div class="color-palette-set" align="center"><a id="deleteSemillero" href="' . base_url() . 'index.php/Semilleros_C/listarId/' . $tabla->idsemillero . '"><div class="bg-red-active color-palette"><span class="fa fa-trash"></span></div></a></div></td>';
                $data['data'][$j]['Modificar'] = '<td><div class="color-palette-set" align="center"><a id="updateSemillero" href="' . base_url() . 'index.php/Semilleros_C/listarId/' . $tabla->idsemillero . '"><div class="bg-light-blue-active color-palette"><span class="fa fa-pencil-square-o"></span></div></a></div></td>';
                $j++;
            }
            echo json_encode($data);
        } else {
            $data['data'][$j][0] = 'vacio';
            $data['data'][$j][1] = 'vacio';
            $data['data'][$j][2] = 'vacio';
            $data['data'][$j][3] = 'vacio';
            $data['data'][$j][4] = 'vacio';
            $data['data'][$j][5] = 'vacio';
            $data['data'][$j][6] = 'vacio';
            echo json_encode($data);
        }
    }
    public function listarId($idSemillero)
    {
        $datos = $this->Semillero->listar_id($idSemillero);
        if (!empty($datos) || $datos != false) {
            $datos[0]->url_base = base_url();
            echo json_encode($datos);
        } else {
            echo '-1';
        }
    }

    public function inscripcion($id)
    {
        $this->form_validation->set_rules('id', 'id universitario', 'required|numeric|greater_than[0]');
        $this->form_validation->set_rules('documento', 'numero documento identidad', 'required|numeric|greater_than[0]');

        $this->form_validation->set_message('required', '%s es obligatorio.');
        $this->form_validation->set_message('numeric', '%s debe ser numérico.');
        $this->form_validation->set_message('greater_than', '%s el numero no puede ser 0 o negativo');

        if ($this->form_validation->run() == true) {
            $datos['id'] = $this->input->post('id');
            $datos['documento'] = $this->input->post('documento');
            $datos['idCursoLibre'] = $id;
            $info = $this->Semillero->registroUsuarioSemillero($datos);
            if ($info != false) {
                foreach ($info as $value) {
                    $dato = $value->infoUser;
                }
                echo $dato;
            } else {
                echo '-1';
            }
        } else {
            echo '-' . validation_errors();
        }
    }

    public function inscribirse()
    {
        $datosSemilleros['semilleros'] = $this->Semillero->listar($this->session->userdata('periodo'), $this->session->userdata('seccional'));
        if (!empty($datosSemilleros['semilleros'])) {
            echo json_encode($datosSemilleros['semilleros']);
        } else {
            echo '-1';
        }
    }
    public function modificarSemillero()
    {
        $this->form_validation->set_rules('nombre', 'Nombre', 'required|max_length[250]');
        $this->form_validation->set_rules('Contenido_Tematico', 'Contenido tematico', 'required');
        $this->form_validation->set_rules('docente', 'Docente', 'required');

        $this->form_validation->set_message('required', '%s es obligatorio.');
        $this->form_validation->set_message('max_length', '%s exede maximo caracteres requeridos.');

        if ($this->form_validation->run() == true) {

            $datos['nombre'] = $this->input->post('nombre');
            $datos['docente'] = $this->input->post('docente');
            $datos['idsemillero'] = $this->input->post('idSemillero');
            $datos['contenido_tematico'] = $this->input->post('Contenido_Tematico');

            $verificacion = $this->Semillero->modificar($datos);
            echo $verificacion;
            if (empty($verificacion)) {
                echo 'correcto';
            } else {
                echo '-1';
            }
        } else {
            echo validation_errors();
        }
    }

    public function eliminar($idSemillero)
    {
        $informacion = $this->Semillero->listar_id($idSemillero);
        mysqli_next_result($this->db->conn_id);
        if (!empty($informacion) || $informacion != false) {
            $this->Semillero->eliminar($idSemillero);
            echo $informacion[0]->nombre;
        } else {
            echo '-1';
        }
    }

    public function inscritos($idSemillero)
    {
        $datosInscritos['inscritos'] = $this->Semillero->inscritos($idSemillero);
        if (!empty($datosInscritos['inscritos']) || $datosInscritos['inscritos'] != false) {
            echo json_encode($datosInscritos['inscritos']);
        } else {
            echo '-1';
        }
    }

    public function puestaEscena()
    {
        $datos['semilleros'] = $this->Semillero->listar_semillero_docente_completo($this->session->userdata('idUniversitario'));
        mysqli_next_result($this->db->conn_id);
        $datos['escenarios'] = $this->Escenario->listar($this->session->userdata('periodo'),$this->session->userdata('seccional'));       
        $this->load->view('Cultura/Head');
        $this->load->view('Cultura/NavBar');
        $this->load->view('Cultura/PuestaEscena', $datos);
        $this->load->view('Cultura/Footer');
       
    }

}
