<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CursoLibre_C extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->model('CursoLibre');
        $this->load->model('Usuario');
        $this->load->model('Imagen');
        $this->load->library('session');
    }

    public function crear()
    {
        if ($this->session->userdata('rol') == 'ADMINISTRADOR UNO') {
            $docente["docente"] = $this->Usuario->obtener_docente();
            $this->load->view('Cultura/Head');
            $this->load->view('Cultura/NavBar');
            $this->load->view('Cultura/Crear_Cursos_Libres', $docente);
            $this->load->view('Cultura/Footer');
        }
    }

    public function registarCursoLibre()
    {
        if ($this->session->userdata('rol') == 'ADMINISTRADOR UNO') {
            $this->form_validation->set_rules('nombre', 'Nombre', 'required|max_length[250]');
            $this->form_validation->set_rules('cantidad_cupos', 'Cantidad de cupos', 'required|numeric|max_length[4]|greater_than[0]');
            $this->form_validation->set_rules('cantidad_horas', 'Cantidad horas', 'required|numeric|max_length[4]|greater_than[0]');
            $this->form_validation->set_rules('Contenido_Tematico', 'Contenido tematico', 'required');
            $this->form_validation->set_rules('docente', 'Docente', 'required');
            if (empty($_FILES['Imagen']['name'])) {
                $this->form_validation->set_rules('Imagen', 'Imagen', 'required');
            }

            $this->form_validation->set_message('required', '%s es obligatorio.');
            $this->form_validation->set_message('numeric', '%s debe ser numérico.');
            $this->form_validation->set_message('max_length', '%s exede maximo caracteres requeridos.');
            $this->form_validation->set_message('Imagen', "%s no selecciono ninguna imagen");
            $this->form_validation->set_message('greater_than', '%s el numero no puede ser 0 o negativo');

            if ($this->form_validation->run() == true) {
                if ($this->Imagen->subirImagen('Imagen') == "subio la imagen y esta correcta") {
                    $this->CursoLibre->nuevoCursoLibre($this->input->post('nombre'), $this->input->post('cantidad_cupos'), $this->input->post('Contenido_Tematico'), $this->input->post('cantidad_horas'), $this->Imagen->getNombre("file_name"), $this->input->post('docente'), $this->session->userdata('periodo'), $this->session->userdata('seccional'));
                    redirect('/index.php/CursoLibre_C/listar/');
                } elseif ($this->Imagen->subirImagen('Imagen') == "subio la imagen pero con errores") {
                    $docente["docente"] = $this->Usuario->obtener_docente();
                    $this->load->view('Cultura/Head');
                    $this->load->view('Cultura/NavBar');
                    $this->load->view('Cultura/Crear_Cursos_Libres', $docente);
                    $this->load->view('Cultura/Footer');
                }
            } else {
                $docente["docente"] = $this->Usuario->obtener_docente();
                $this->load->view('Cultura/Head');
                $this->load->view('Cultura/NavBar');
                $this->load->view('Cultura/Crear_Cursos_Libres', $docente);
                $this->load->view('Cultura/Footer');
            }
        }
    }

    public function modificarCursoLibre()
    {
        if ($this->session->userdata('rol') == 'ADMINISTRADOR UNO') {
            $this->form_validation->set_rules('nombre', 'Nombre', 'required|max_length[250]');
            $this->form_validation->set_rules('cantidad_cupos', 'Cantidad de cupos', 'required|numeric|max_length[4]|greater_than[0]');
            $this->form_validation->set_rules('cantidad_horas', 'Cantidad horas', 'required|numeric|max_length[4]|greater_than[0]');
            $this->form_validation->set_rules('Contenido_Tematico', 'Contenido tematico', 'required');
            $this->form_validation->set_rules('docente', 'Docente', 'required');

            $this->form_validation->set_message('required', '%s es obligatorio.');
            $this->form_validation->set_message('numeric', '%s debe ser numérico.');
            $this->form_validation->set_message('max_length', '%s exede maximo caracteres requeridos.');
            $this->form_validation->set_message('greater_than', '%s el numero no puede ser 0 o negativo');

            if ($this->form_validation->run() == true) {

                $datos['nombre'] = $this->input->post('nombre');
                $datos['cupo_maximo'] = $this->input->post('cantidad_cupos');
                $datos['docente'] = $this->input->post('docente');
                $datos['duracion'] = $this->input->post('cantidad_horas');
                $datos['idCurso_libre'] = $this->input->post('idCusoLibre');
                $datos['contenido_tematico'] = $this->input->post('Contenido_Tematico');

                $verificacion = $this->CursoLibre->modificar($datos);
                echo $verificacion;
                if (empty($verificacion)) {
                    echo 'correcto';
                } else {
                    echo '-1';
                }
            } else {
                echo validation_errors();
            }
        }
    }

    public function listar()
    {
        if ($this->session->userdata('rol') == 'ADMINISTRADOR UNO') {
            $datos['cursosLibres'] = $this->CursoLibre->listar($this->session->userdata('periodo'), $this->session->userdata('seccional'));
            $this->load->view('Cultura/Head');
            $this->load->view('Cultura/NavBar');
            $this->load->view('Cultura/Listar_Cursos_Libres', $datos);
            $this->load->view('Cultura/Footer');
        }
    }

    public function listar_ajax()
    {
        if ($this->session->userdata('rol') == 'ADMINISTRADOR UNO') {
            $datos = $this->CursoLibre->listar($this->session->userdata('periodo'), $this->session->userdata('seccional'));
            if (!empty($datos) || $datos != false) {
                $j = 0;
                foreach ($datos as $tabla) {
                    $data['data'][$j][0] = $tabla->nombre;
                    $data['data'][$j][1] = $tabla->cupo_maximo;
                    $data['data'][$j][2] = $tabla->inscritos;
                    $data['data'][$j][3] = $tabla->duracion;
                    $data['data'][$j][4] = $tabla->contenido_tematico;
                    $data['data'][$j][5] = $tabla->nombre_docente . ' ' . $tabla->apellido_docente;
                    $data['data'][$j][6] = '<td><div class="color-palette-set" align="center"><a id="deleteCursoLibre" href="' . base_url() . 'index.php/CursoLibre_C/listarId/' . $tabla->idCurso_libre . '"><div class="bg-red-active color-palette"><span class="fa fa-trash"></span></div></a></div></td>';
                    $data['data'][$j][7] = '<td><div class="color-palette-set" align="center"><a id="updateCursoLibre" href="' . base_url() . 'index.php/CursoLibre_C/listarId/' . $tabla->idCurso_libre . '"><div class="bg-light-blue-active color-palette"><span class="fa fa-pencil-square-o"></span></div></a></div></td>';
                    $j++;
                }
                echo json_encode($data);
            } else {
                $data['data'][$j][0] = 'vacio';
                $data['data'][$j][1] = 'vacio';
                $data['data'][$j][2] = 'vacio';
                $data['data'][$j][3] = 'vacio';
                $data['data'][$j][4] = 'vacio';
                $data['data'][$j][5] = 'vacio';
                $data['data'][$j][6] = 'vacio';
                $data['data'][$j][7] = 'vacio';
                echo json_encode($data);
            }
        }
    }

    public function listarId($idCursoLibre)
    {
        $datos = $this->CursoLibre->listar_id($idCursoLibre);
        if (!empty($datos) || $datos != false) {
            $datos[0]->url_base = base_url();
            echo json_encode($datos);
        } else {
            echo '-1';
        }
    }

    public function eliminarParticipanteCursoLibre($idParticipante, $idCursoLibre)
    {

        $eliminar = $this->CursoLibre->eliminarParticipanteCursolibre($idParticipante, $idCursoLibre);
        if (empty($eliminar) || $eliminar != false) {
            echo 'correcto';
        } else {
            echo 'No fue exitosa la operacion solicitada';
        }

    }

    public function eliminar($idCursoLibre)
    {
        if ($this->session->userdata('rol') == 'ADMINISTRADOR UNO') {
            $informacion = $this->CursoLibre->listar_id($idCursoLibre);
            mysqli_next_result($this->db->conn_id);
            if (!empty($informacion) || $informacion != false) {
                $this->CursoLibre->eliminar($idCursoLibre);
                echo $informacion[0]->nombre;
            } else {
                echo '-1';
            }
        }
    }

    public function obtenerDocentes()
    {
        if ($this->session->userdata('rol') == 'ADMINISTRADOR UNO') {
            $docente = $this->Usuario->obtener_docente();
            if ($docente != false) {
                echo json_encode($docente);
            } else {
                echo '-1';
            }
        }
    }

    public function inscribirse()
    {
        $datosCursosLibres['cursosLibres'] = $this->CursoLibre->listar($this->session->userdata('periodo'), $this->session->userdata('seccional'));
        if (!empty($datosCursosLibres['cursosLibres'])) {
            echo json_encode($datosCursosLibres['cursosLibres']);
        } else {
            echo '-1';
        }
    }

    public function inscripcion($id)
    {
        $this->form_validation->set_rules('id', 'id universitario', 'required|numeric|greater_than[0]');
        $this->form_validation->set_rules('documento', 'numero documento identidad', 'required|numeric|greater_than[0]');

        $this->form_validation->set_message('required', '%s es obligatorio.');
        $this->form_validation->set_message('numeric', '%s debe ser numérico.');
        $this->form_validation->set_message('greater_than', '%s el numero no puede ser 0 o negativo');

        if ($this->form_validation->run() == true) {
            $datos['id'] = $this->input->post('id');
            $datos['documento'] = $this->input->post('documento');
            $datos['idCursoLibre'] = $id;
            $info = $this->CursoLibre->registroUsuarioCursoLibre($datos);
            if ($info != false) {
                foreach ($info as $value) {
                    $dato = $value->infoUser;
                }
                echo $dato;
            } else {
                echo '-1';
            }
        } else {
            echo '-' . validation_errors();
        }
    }

    public function asignarHorario()
    {
        if ($this->session->userdata('rol') == 'DOCENTE') {
            if (!empty($this->input->post('idCursoLibre')) && !empty($this->input->post('horarios')) && $this->input->post('idCursoLibre') != '-1') {
                $valoresHorario = $this->input->post('horarios');
                $this->CursoLibre->agregarHorarioCursoLibre($valoresHorario, $this->input->post('idCursoLibre'));
                $this->horario();
            } else {
                $cursosLibresDocente['error'] = 'El horario no debe estar vacio o el curso libre no debe estar vacio';
                $cursosLibresDocente['cursosLibresDocente'] = $this->CursoLibre->listar_cursoLibre_docente($this->session->userdata('idUniversitario'));
                if (!empty($cursosLibresDocente)) {
                    $this->load->view('Cultura/Head');
                    $this->load->view('Cultura/NavBar');
                    $this->load->view('Cultura/HorarioCursoLibre', $cursosLibresDocente);
                    $this->load->view('Cultura/Footer');
                }
            }
        }
    }

    public function horario()
    {
        if ($this->session->userdata('rol') == 'DOCENTE' && !empty($this->session->userdata('idUniversitario'))) {
            $cursosLibresDocente['cursosLibresDocente'] = $this->CursoLibre->listar_cursoLibre_docente($this->session->userdata('idUniversitario'));
            if (!empty($cursosLibresDocente)) {
                $this->load->view('Cultura/Head');
                $this->load->view('Cultura/NavBar');
                $this->load->view('Cultura/HorarioCursoLibre', $cursosLibresDocente);
                $this->load->view('Cultura/Footer');
            }
        }
    }

    public function asistencia()
    {
        if ($this->session->userdata('rol') == 'DOCENTE' && !empty($this->session->userdata('idUniversitario'))) {
            $datosAsistencia['cursosLibres'] = $this->CursoLibre->listar_cursoLibre_docente_completo($this->session->userdata('idUniversitario'));
            $this->load->view('Cultura/Head');
            $this->load->view('Cultura/NavBar');
            $this->load->view('Cultura/AsistenciaCursosLibres', $datosAsistencia);
            $this->load->view('Cultura/Footer');
        }
    }

    public function estudiantesCursoLibre($idCursoLibre)
    {
        if ($this->session->userdata('rol') == 'DOCENTE' && !empty($this->session->userdata('idUniversitario'))) {
            $datosEstudiantesCursoLibre['inscritos'] = $this->CursoLibre->inscritosCursoLibre($idCursoLibre);
            if (!empty($datosEstudiantesCursoLibre['inscritos']) || $datosEstudiantesCursoLibre['inscritos'] != false) {
                $j = 0;
                foreach ($datosEstudiantesCursoLibre['inscritos'] as $tabla) {
                    $data['data'][$j][0] = $tabla->nombre1 . ' ' . $tabla->nombre2 . ' ' . $tabla->apellido1 . ' ' . $tabla->apellido2;
                    $data['data'][$j][1] = $tabla->asistencias;
                    $data['data'][$j][2] = '<div class="form-group">
                                                <div class="checkbox">
                                                    <label>
                                                        <input name="idInscritos[]" value="' . $tabla->idUsuario . '" type="checkbox">
                                                    </label>
                                                </div>
                                            </div>';
                    $data['data'][$j][3] = '<div class="color-palette-set" align="center">
                                                <a id="eliminarParticipante" href="' . base_url() . 'index.php/CursoLibre_C/listarParticipanteCursoLibre/' . $tabla->idUsuario . '/' . $idCursoLibre . '">
                                                    <div class="bg-red-active color-palette">
                                                        <span class="fa fa-trash"></span>
                                                    </div>
                                                </a>
                                            </div>';
                    $j++;
                }
                echo json_encode($data);
            } else {
                $data['data'][0][0] = 'vacio';
                $data['data'][0][1] = 'vacio';
                $data['data'][0][2] = 'vacio';
                $data['data'][0][3] = 'vacio';
                echo json_encode($data);

            }
        }
    }

    public function listarParticipanteCursoLibre($idParticipante, $idCursoLibre)
    {
        $listar = $this->Usuario->obtener_usuario_id_u($idParticipante);
        if (!empty($listar) || $listar != false) {
            foreach ($listar as $value) {
                $datos['nombre'] = $value->nombre1 . ' ' . $value->nombre2 . ' ' . $value->apellido1 . ' ' . $value->apellido2;
                $datos['idCursoLibre'] = $idCursoLibre;
                $datos['idUsuario'] = $value->idUsuario;
            }
            echo json_encode($datos);

        } else {
            echo '-1';
        }
    }

    public function tomarAsistencia()
    {
        $this->form_validation->set_rules('idCursoLibre', 'Id curso libre', 'required|numeric|greater_than[0]');
        $this->form_validation->set_rules('fecha', 'fecha', 'required');

        $this->form_validation->set_message('required', '%s es obligatorio.');
        $this->form_validation->set_message('numeric', '%s debe ser numérico.');
        $this->form_validation->set_message('greater_than', 'Debe selecionar un curso libre');

        if ($this->session->userdata('rol') == 'DOCENTE' && !empty($this->session->userdata('idUniversitario'))) {
            if ($this->form_validation->run() == true) {
                if (!empty($this->input->post('idInscritos'))) {
                    $this->CursoLibre->tomarAsistenciaCursoLibre($this->input->post('idCursoLibre'), $this->input->post('idInscritos'), $this->input->post('fecha'));
                    $this->asistencia();
                } else {
                    echo 'vacio';
                }
            } else {
                $datosAsistencia['cursosLibres'] = $this->CursoLibre->listar_cursoLibre_docente_completo($this->session->userdata('idUniversitario'));
                $this->load->view('Cultura/Head');
                $this->load->view('Cultura/NavBar');
                $this->load->view('Cultura/AsistenciaCursosLibres', $datosAsistencia);
                $this->load->view('Cultura/Footer');
            }

        }

    }

}
