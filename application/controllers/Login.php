<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->model('Usuario');
        $this->load->model('Periodo');
        $this->load->model('Seccional');
        $this->load->helper('security');
        $this->load->library('session');
    }

    public function index()
    {
        $datos['seccional'] = $this->Seccional->listar();
        $datos['periodo'] = $this->Periodo->listar();
        $this->load->view('Login', $datos);
    }

    public function validarUsuario()
    {
        $this->form_validation->set_rules('id', 'ID Uiversitario', 'required|max_length[9]|numeric|xss_clean');
        $this->form_validation->set_rules('contraseña', 'Número de documento', 'required|xss_clean');
        $this->form_validation->set_rules('periodo', 'Periodo', 'required|xss_clean');
        $this->form_validation->set_rules('seccional', 'Seccional', 'required|xss_clean');

        $this->form_validation->set_message('required', '%s es obligatorio.');
        $this->form_validation->set_message('numeric', '%s debe ser numérico.');
        $this->form_validation->set_message('xss_clean', 'No no no!!! sin malas intenciones.');

        if ($this->form_validation->run() == true) {
            //verifica si los datos son correctos
            $datosObtenidos = $this->Usuario->validacionUsuario($this->input->post('id'), $this->input->post('contraseña'), $this->input->post('seccional'));
            if ($datosObtenidos != false) {

                $datosSession = array(
                    'rol' => $datosObtenidos['rol'],
                    'idPersona' => $datosObtenidos['numeroIdentificacion'],
                    'idUniversitario' => $datosObtenidos['idUniversitario'],
                    'periodo' => $this->input->post('periodo'),
                    'seccional' => $this->input->post('seccional'),
                );
                $this->session->set_userdata($datosSession);

                $this->load->view('Cultura/Head');
                $this->load->view('Cultura/NavBar');
                $this->load->view('Cultura/Content');
                $this->load->view('Cultura/Footer');
            } else {
                $datos['seccional'] = $this->Seccional->listar();
                $datos['periodo'] = $this->Periodo->listar();
                $datos['informacion'] = 'Usuario y/o Contraseña Incorrecto(s)';
                $this->load->view('Login', $datos);
            }
        } else {
            $datos['seccional'] = $this->Seccional->listar();
            $datos['periodo'] = $this->Periodo->listar();
            $datos['informacion'] = ' ';
            $this->load->view('Login', $datos);
        }
    }

    public function logout()
    {
        $valoresSession = array('rol' => '', 'idPersona' => 'periodo', '' => '', 'seccional' => '');
        $this->session->unset_userdata($valoresSession);
        $this->session->sess_destroy();
        redirect('/index.php/Login');
    }

    public function datosSessionUsuario($seccional)
    {
        $listar = $this->Periodo->listarUltimo();
        if (!empty($listar) || $listar != false) {
            foreach ($listar as $value) {             
                $datosSession = array(
                    'periodo' => $value->idperiodo,
                    'seccional' => $seccional,
                );    
                $this->session->set_userdata($datosSession);           
            }            
            echo $this->session->userdata('seccional');           
        } else {
            echo '-1';
        }
    }

}
