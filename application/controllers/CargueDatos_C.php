<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CargueDatos_C extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->model('Usuario');
        $this->load->model('Facultad');
        $this->load->library('upload');
        $config['upload_path'] = './archivos/';
        $config['allowed_types'] = 'csv';
        $this->upload->initialize($config);
    }

    public function index()
    {
        $this->load->view('Cultura/Head');
        $this->load->view('Cultura/NavBar');
        $this->load->view('CargueDatos');
        $this->load->view('Cultura/Footer');
    }

    public function mensajeVista($mensaje)
    {
        $error['error'] = $mensaje;
        $this->load->view('Cultura/Head');
        $this->load->view('Cultura/NavBar');
        $this->load->view('CargueDatos', $error);
        $this->load->view('Cultura/Footer');
    }

    public function eliminarArchivo($nombre)
    {
        unlink('./archivos/' . $nombre);
    }

    public function validarFacultad($idFacultad)
    {
        $facultades = $this->Facultad->listar();
        if (!empty($facultades)) {
            for ($i = 0; $i < count($facultades); $i++) {
                if ($facultades[$i]->nombre == $idFacultad) {
                    return true;
                }
            }
        } else {
            return false;
        }
    }

    public function validar()
    {
        if ($this->upload->do_upload('datos')) {
            try
            {
                $bandera = true;
                $fp = fopen(base_url() . 'archivos/' . $this->upload->data("file_name"), "r");
                $i = 1;
                while ($datos = fgetcsv($fp, 100000, ",")) {
                    $num = count($datos);
                    if ($num >= 11) {
                        $arrayDatos[$i] = array(
                            'Numero_identificacion' => $datos[0],
                            'Tipo_documento' => $datos[1],
                            'Primer_nombre' => $datos[2],
                            'Segundo_nombre' => $datos[3],
                            'Primer_apellido' => $datos[4],
                            'Segundo_apellido' => $datos[5],
                            'Correo_Electronico' => $datos[6],
                            'Genero' => $datos[7],
                            'ID_universitario' => $datos[8],
                            'Rol' => $datos[9],
                            'Facultad' => $datos[10],
                        );
                        if (is_string($arrayDatos[$i]['Numero_identificacion'])) {
                            if (is_string($arrayDatos[$i]['Tipo_documento'])) {
                                if (is_string($arrayDatos[$i]['Primer_nombre'])) {
                                    if (is_string($arrayDatos[$i]['Segundo_nombre'])) {
                                        if (is_string($arrayDatos[$i]['Primer_apellido'])) {
                                            if (is_string($arrayDatos[$i]['Segundo_apellido'])) {
                                                if (filter_var($arrayDatos[$i]['Correo_Electronico'], FILTER_VALIDATE_EMAIL)) {
                                                    if (is_string($arrayDatos[$i]['Genero'])) {
                                                        if (is_numeric($arrayDatos[$i]['ID_universitario'])) {
                                                            if (is_string($arrayDatos[$i]['Rol'])) {
                                                                if (is_string($arrayDatos[$i]['Facultad'])) {
                                                                    $facultades = explode(";", $arrayDatos[$i]['Facultad']);
                                                                    $arrayDatos[$i]['Facultad'] = count($facultades);
                                                                    for ($j = 0; $j < count($facultades); $j++) {
                                                                        if ($this->validarFacultad($facultades[$j])) {
                                                                            $arrayDatos[$i][$j]['Facultad'] = $facultades[$j];
                                                                        } else {
                                                                            $bandera = false;
                                                                            $this->eliminarArchivo($this->upload->data("file_name"));
                                                                            $this->mensajeVista('En la fila numero: ' . $i . ' no existe la facultad ingresada');
                                                                            break 2;
                                                                        }

                                                                    }
                                                                } else {
                                                                    $this->eliminarArchivo($this->upload->data("file_name"));
                                                                    $this->mensajeVista('Error en la columna Facultad en la fila: ' . $i);
                                                                    break;
                                                                }
                                                            } else {
                                                                $this->eliminarArchivo($this->upload->data("file_name"));
                                                                $this->mensajeVista('Error en la columna Rol en la fila: ' . $i);
                                                                break;
                                                            }
                                                        } else {
                                                            $this->eliminarArchivo($this->upload->data("file_name"));
                                                            $this->mensajeVista('Error en la columna ID universitario en la fila: ' . $i);
                                                            break;
                                                        }
                                                    } else {
                                                        $this->eliminarArchivo($this->upload->data("file_name"));
                                                        $this->mensajeVista('Error en la columna Genero en la fila: ' . $i);
                                                        break;
                                                    }
                                                } else {
                                                    $this->eliminarArchivo($this->upload->data("file_name"));
                                                    $this->mensajeVista('Error en la columna Correo Electronico en la fila: ' . $i);
                                                    break;
                                                }
                                            } else {
                                                $this->eliminarArchivo($this->upload->data("file_name"));
                                                $this->mensajeVista('Error en la columna Segundo apellido en la fila: ' . $i);
                                                break;
                                            }
                                        } else {
                                            $this->eliminarArchivo($this->upload->data("file_name"));
                                            $this->mensajeVista('Error en la columna Primer apellido en la fila: ' . $i);
                                            break;
                                        }
                                    } else {
                                        $this->eliminarArchivo($this->upload->data("file_name"));
                                        $this->mensajeVista('Error en la columna Segundo nombre en la fila: ' . $i);
                                        break;
                                    }
                                } else {
                                    $this->eliminarArchivo($this->upload->data("file_name"));
                                    $this->mensajeVista('Error en la columna Primer nombre en la fila: ' . $i);
                                    break;
                                }
                            } else {
                                $this->eliminarArchivo($this->upload->data("file_name"));
                                $this->mensajeVista('Error en la columna Tipo documento en la fila: ' . $i);
                                break;
                            }
                        } else {
                            $this->eliminarArchivo($this->upload->data("file_name"));
                            $this->mensajeVista('Error en la columna Numero identificacion en la fila: ' . $i);
                            break;
                        }
                    } else {
                        $bandera = false;
                        $this->eliminarArchivo($this->upload->data("file_name"));
                        $this->mensajeVista('En la fila numero: ' . $i . ' no existen las columnas completas');
                        break;
                    }
                    $i++;
                }
                if ($bandera) {
                    if ($this->Usuario->nuevaPersona($arrayDatos) !== false) {
                        if ($this->Usuario->nuevoUsuario($arrayDatos) !== false) {
                            $error['correcto'] = 'El cargue de datos fue exitoso';
                            $this->load->view('Cultura/Head');
                            $this->load->view('Cultura/NavBar');
                            $this->load->view('CargueDatos', $error);
                            $this->load->view('Cultura/Footer');
                        } else {
                            $this->Usuario->vaciarUsuario();
                            $this->Usuario->vaciarPersona();
                            $this->mensajeVista('Ocurrio un error insertando los de usuario en la base de datos');
                        }
                    } else {
                        $this->Usuario->vaciarUsuario();
                        $this->Usuario->vaciarPersona();
                        $this->mensajeVista('Ocurrio un error insertando los valores de persona en la base de datos');
                    }
                } else {
                    $this->Usuario->vaciarUsuario();
                    $this->Usuario->vaciarPersona();
                    $this->mensajeVista('Ocurrio un error insertando los valores en la base de datos');
                }
            } catch (Excepcion $e) {
                $this->mensajeVista('Ocurrio un error al momento de manipular el archivo');
            }
        } else {
            $this->mensajeVista($this->upload->display_errors());
        }
    }

}
