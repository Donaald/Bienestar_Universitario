<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Escenario_C extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->model('Usuario');
        $this->load->model('Escenario');
        $this->load->helper('security');
        $this->load->library('session');
    }

    public function index()
    {

    }

    public function crear()
    {
        $this->load->view('Cultura/Head');
        $this->load->view('Cultura/NavBar');
        $this->load->view('Cultura/CrearEscenario');
        $this->load->view('Cultura/Footer');
    }

    public function validarFormulario()
    {
        $this->form_validation->set_rules('nombre', 'Nombre', 'required|max_length[250]');
        $this->form_validation->set_rules('fecha', 'Fecha', 'required');
        $this->form_validation->set_rules('rango_fecha', 'Dias', 'required|callback_rango_fecha_check');
        $this->form_validation->set_rules('lugar', 'Lugar', 'required');
        $this->form_validation->set_rules('justificacion', 'Justificacion', 'required');

        $this->form_validation->set_message('required', '%s es obligatorio.');
        $this->form_validation->set_message('max_length', '%s exede maximo caracteres requeridos.');
        $this->form_validation->set_message('rango_fecha_check', 'Hubo un error con el formato de fecha de Dias');

        if ($this->form_validation->run() == true) {
            $rangoFecha = explode("-", $this->input->post('rango_fecha'));
            $rangoFecha[0] = new DateTime($rangoFecha[0]);
            $rangoFecha[1] = new DateTime($rangoFecha[1]);

            $datos['nombre'] = $this->input->post('nombre');
            $datos['fecha'] = $this->input->post('fecha');
            $datos['duracion'] = $rangoFecha[0]->diff($rangoFecha[1])->days + 1;
            $datos['lugar'] = $this->input->post('lugar');
            $datos['justificacion'] = $this->input->post('justificacion');
            $datos['periodo_idperiodo'] = $this->session->userdata('periodo');
            $datos['seccional_idseccional'] = $this->session->userdata('seccional');
            $datos['estado_idestado'] = 'ACTIVO';

            if (!$this->Escenario->crear($datos)) {
                header('Location: ' . base_url() . 'index.php/Escenario_C/crear');
            }
        } else {
            $this->crear();
        }

    }

    public function rango_fecha_check($rangoFecha)
    {
        try
        {
            $rangoFecha = explode("-", $this->input->post('rango_fecha'));
            $rangoFecha[0] = new DateTime($rangoFecha[0]);
            $rangoFecha[1] = new DateTime($rangoFecha[1]);
            $diff = $rangoFecha[0]->diff($rangoFecha[1]);
            return true;
        } catch (exception $e) {
            return false;
        }
    }
    public function hora_check($string)
    {
        if (preg_match('/^[0-9]{2,2}:[0-9]{2,2}$/', $string)) {
            return true;
        } else {
            return false;
        }
    }

    public function listar()
    {
        $datosEscenarios['Escenarios'] = $this->Escenario->listar($this->session->userdata('periodo'), $this->session->userdata('seccional'));
        if ($datosEscenarios['Escenarios'] !== false || !empty($datosEscenarios['Escenarios'])) {
            $this->load->view('Cultura/Head');
            $this->load->view('Cultura/NavBar');
            $this->load->view('Cultura/ListarEscenario', $datosEscenarios);
            $this->load->view('Cultura/Footer');
        } else {
            echo 'error';
        }
    }
    public function listarUsuarios()
    {
        $datosUsuarios['datosUsuarios'] = $this->Usuario->obtener_usuario();
        if ($datosUsuarios['datosUsuarios'] !== false || !empty($datosUsuarios['datosUsuarios'])) {
            echo json_encode($datosUsuarios['datosUsuarios']);
        } else {
            echo 'error';
        }
    }
    public function listar_ajax()
    {
        $datosEscenarios['Escenarios'] = $this->Escenario->listar($this->session->userdata('periodo'), $this->session->userdata('seccional'));
        if ($datosEscenarios['Escenarios'] !== false || !empty($datosEscenarios['Escenarios'])) {
            echo json_encode($datosEscenarios['Escenarios']);
        } else {
            echo 'error';
        }
    }
    public function listar_id_escenario($idEscenario)
    {
        $datos = $this->Escenario->listar_id($idEscenario);
        if (!empty($datos) || $datos != false) {
            echo json_encode($datos);
        } else {
            echo '-1';
        }
    }

    public function listarActividadesFecha()
    {
        $datos = $this->Escenario->listarActividadesFecha($this->session->userdata('seccional'), $this->session->userdata('periodo'));
        if (!empty($datos) || $datos != false) {
            echo json_encode($datos);
        } else {
            echo '-1';
        }
    }

    public function listar_actividades_escenario($idEscenario)
    {
        $datosActividades = $this->Escenario->listarActividadesEscenario($idEscenario);
        if ($datosActividades != false || !empty($datosActividades)) {
            $j = 0;
            foreach ($datosActividades as $tabla) {
                $data['data'][$j]['nombre'] = $tabla->nombre;
                $data['data'][$j]['duracion'] = $tabla->duracion;
                $data['data'][$j]['descripcion'] = $tabla->descripcion;
                $data['data'][$j]['fecha'] = $tabla->fecha;
                $data['data'][$j]['hora'] = $tabla->hora;
                $data['data'][$j]['lugar'] = $tabla->lugar;
                $data['data'][$j]['publicoObjetivo'] = $tabla->publicoObjetivo;
                $data['data'][$j]['eliminar'] = '<td><div class="color-palette-set" align="center"><button value="' . $idEscenario . '" id="deleteActividad" href="' . base_url() . 'index.php/Escenario_C/listar_actividad_id/' . $tabla->idactividad . '"><div class="bg-red-active color-palette"><span class="fa fa-trash"></span></div></button></div></td>';
                $j++;
            }
            echo json_encode($data);
        } else {
            $data['data'][0]['nombre'] = 'vacio';
            $data['data'][0]['duracion'] = 'vacio';
            $data['data'][0]['descripcion'] = 'vacio';
            $data['data'][0]['fecha'] = 'vacio';
            $data['data'][0]['hora'] = 'vacio';
            $data['data'][0]['lugar'] = 'vacio';
            $data['data'][0]['publicoObjetivo'] = 'vacio';
            $data['data'][0]['eliminar'] = 'vacio';
            echo json_encode($data);
        }

    }
    public function listar_actividades_escenario_asistencia($idEscenario)
    {
        $datosActividades = $this->Escenario->listarActividadesEscenario($idEscenario);
        if ($datosActividades != false || !empty($datosActividades)) {
            $j = 0;
            foreach ($datosActividades as $tabla) {
                $data['data'][$j][0] = $tabla->nombre;
                $data['data'][$j][1] = $tabla->duracion;
                $data['data'][$j][2] = $tabla->fecha;
                $data['data'][$j][3] = $tabla->hora;
                $data['data'][$j][4] = $tabla->lugar;
                $data['data'][$j][5] = '<div class="row">
                                            <div class="col-xs-6">
                                                <div class="color-palette-set" align="center">
                                                    <a href="' . $tabla->idactividad . '" data-toggle="modal" class="tomarAsistencia" data-target="#conId" >
                                                        <div class="bg-yellow color-palette"><span class="fa fa-male"></span></div>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="color-palette-set" align="center">
                                                    <a href="' . $tabla->idactividad . '" data-toggle="modal" class="tomarAsistencia" data-target="#sinId" >
                                                        <div class="bg-yellow color-palette"><span class="fa fa-user-plus"></span></div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>';
                $j++;
            }
            echo json_encode($data);
        } else {
            $data['data'][0][0] = 'vacio';
            $data['data'][0][1] = 'vacio';
            $data['data'][0][2] = 'vacio';
            $data['data'][0][3] = 'vacio';
            $data['data'][0][4] = 'vacio';
            $data['data'][0][5] = 'vacio';
            echo json_encode($data);
        }

    }
    public function listar_actividad_id($idActividad)
    {
        $datos = $this->Escenario->listarActividadId($idActividad);
        if (!empty($datos) || $datos != false) {
            echo json_encode($datos);
        } else {
            echo '-1';
        }
    }
    public function eliminar($idEscenario)
    {
        if (empty($this->Escenario->eliminar($idEscenario))) {
            echo 'correcto';
        } else {
            echo '-1';
        }

    }
    public function eliminarActividad($idActividad)
    {
        if (empty($this->Escenario->eliminarActividad($idActividad))) {
            echo 'correcto';
        } else {
            echo '-1';
        }
    }
    public function agregarActividad()
    {
        $this->form_validation->set_rules('nombre', 'Nombre', 'required|max_length[250]');
        $this->form_validation->set_rules('fecha', 'Fecha', 'required');
        $this->form_validation->set_rules('rango_fecha', 'Dias', 'required|callback_rango_fecha_check');
        $this->form_validation->set_rules('lugar', 'Lugar', 'required');
        $this->form_validation->set_rules('justificacion', 'Justificacion', 'required');
        $this->form_validation->set_rules('publicoObjetivo', 'Publico Objetivo', 'required');
        $this->form_validation->set_rules('hora', 'Hora', 'required|callback_hora_check');

        $this->form_validation->set_message('required', '%s es obligatorio.');
        $this->form_validation->set_message('max_length', '%s exede maximo caracteres requeridos.');
        $this->form_validation->set_message('rango_fecha_check', 'Hubo un error con el formato de fecha de Dias');
        $this->form_validation->set_message('hora_check', 'Hubo un error con el formato de tiempo en Hora');

        if ($this->form_validation->run() == true) {
            $rangoFecha = explode("-", $this->input->post('rango_fecha'));
            $rangoFecha[0] = new DateTime($rangoFecha[0]);
            $rangoFecha[1] = new DateTime($rangoFecha[1]);

            $datos['nombre'] = $this->input->post('nombre');
            $datos['fecha'] = $this->input->post('fecha');
            $datos['duracion'] = $rangoFecha[0]->diff($rangoFecha[1])->days + 1;
            $datos['lugar'] = $this->input->post('lugar');
            $datos['descripcion'] = $this->input->post('justificacion');
            $datos['publicoObjetivo'] = $this->input->post('publicoObjetivo');
            $datos['hora'] = $this->input->post('hora');
            $datos['escenario_idescenario'] = $this->input->post('idEscenario');
            $datos['periodo_idperiodo'] = $this->session->userdata('periodo');
            $datos['seccional_idseccional'] = $this->session->userdata('seccional');
            $datos['estado_idestado'] = 'ACTIVO';

            if (!$this->Escenario->agregarActividad($datos)) {
                echo 'correcto';
            }
        } else {
            echo validation_errors();
        }
    }

    public function asistencia()
    {
        $datosEscenarios['Escenarios'] = $this->Escenario->listar($this->session->userdata('periodo'), $this->session->userdata('seccional'));
        if ($datosEscenarios['Escenarios'] !== false || !empty($datosEscenarios['Escenarios'])) {
            $this->load->view('Cultura/Head');
            $this->load->view('Cultura/NavBar');
            $this->load->view('Cultura/AsistenciaEscenarios', $datosEscenarios);
            $this->load->view('Cultura/Footer');
        } else {
            echo 'error';
        }
    }

    public function tomarAsistencia()
    {

    }

}
