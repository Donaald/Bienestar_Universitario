<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Periodo_C extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');       
        $this->load->library('session');
        $this->load->model('Periodo');
    }

    public function listar()
    {
        $listar = $this->Periodo->listar();
        if (!empty($listar) || $listar != false) {
            echo json_encode($listar);
        } else {
            echo '-1';
        }
    }

    public function listarUltimo()
    {
        $listar = $this->Periodo->listar();
        if (!empty($listar) || $listar != false) {
            echo json_encode($listar);
        } else {
            echo '-1';
        }
    }


 

}
