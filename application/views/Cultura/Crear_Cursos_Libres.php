<div class="content-wrapper">
   <section class="content">
      <?php
         if(!empty(validation_errors()) || !empty($this->upload->display_errors()))
         {
          echo '<div class="alert alert-danger alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h4><i class="icon fa fa-ban"></i>Error!</h4>'.validation_errors()."<br>".$this->upload->display_errors().'</div>';  
         }
         ?>
      <div class="box">
         <div class="box-header with-border">
            <h3 class="box-title">Datos ingreso cursos libres</h3>
         </div>
         <!-- /.box-header -->
         <div class="box-body">
            <?php echo form_open_multipart('index.php/CursoLibre_C/registarCursoLibre');?>
            <div class="row">
               <div class="col-md-6 col-s-12">
                  <?php 
                     if(!empty(form_error('nombre')))
                     { 
                        
                         echo '<div class="form-group has-error"><label>Nombre(*)</label>';
                         $Nombre = array( 
                             'name' => 'nombre',  
                             'class' => 'form-control',
                             'maxlength'=> '250',
                             'type' => 'text'                             
                         );
                         echo form_input($Nombre);
                         echo '</div>';                                                                    
                     }else
                     {
                         echo '<div class="form-group"><label>Nombre(*)</label>';
                         $Nombre = array( 
                             'name' => 'nombre',  
                             'class' => 'form-control',
                             'maxlength'=> '250',
                             'type' => 'text',
                             'value' => set_value("nombre")  
                         );
                         echo form_input($Nombre);
                         echo '</div>';
                     }
                     ?> 
               </div>
               <div class="col-md-6 col-s-12">
                  <?php 
                     if(!empty(form_error('cantidad_cupos')))
                     {   
                         echo'<div class="form-group has-error"><label>Cantidad cupos(*)</label>';
                         $Cantidad_cupos = array( 
                           'name' => 'cantidad_cupos',  
                           'class' => 'form-control',                                        
                           'type' => 'number',
                           'min' => '0'
                       );
                       echo form_input($Cantidad_cupos);
                         echo '</div>';                            
                     }else
                     {
                         echo '<div class="form-group"><label>Cantidad cupos(*)</label>';
                         $Cantidad_cupos = array( 
                           'name' => 'cantidad_cupos',  
                           'class' => 'form-control',                                        
                           'type' => 'number',
                           'min' => '0',
                           'value' => set_value("cantidad_cupos")  
                       );
                       echo form_input($Cantidad_cupos);
                         echo '</div>';
                     }
                     ?> 
               </div>
               <div class="col-md-6 col-s-12">
                  <?php 
                     if(!empty(form_error('docente')))
                     {   
                         echo'<div class="form-group has-error"><label>Docente(*)</label>';
                         $Docente = array( 
                           'name' => 'docente',  
                           'class' => 'form-control select2'                                                              
                       );  
                       $opcionesDocente[''] = 'Selecione nombre del docente';                                     
                       foreach($docente as $value)
                       {                               
                        $opcionesDocente[$value->idUsuario] = $value->nombre." ".$value->apellido;
                       }                
                         echo form_dropdown($Docente, $opcionesDocente);
                         echo '</div>';                            
                     }else
                     {
                         echo '<div class="form-group"><label>Docente(*)</label>';
                         $Docente = array( 
                           'name' => 'docente',  
                           'class' => 'form-control select2',
                           'value' => set_value("docente")
                       );     
                       $opcionesDocente[''] = 'Selecione nombre del docente';                                        
                       foreach($docente as $value)
                       {                               
                        $opcionesDocente[$value->idUsuario] = $value->nombre." ".$value->apellido;
                       }
                       echo form_dropdown($Docente, $opcionesDocente);
                       echo '</div>';
                     }
                     ?>                               
               </div>
               <div class="col-md-6 col-s-12">
                  <?php 
                     if(!empty(form_error('cantidad_horas')))
                     {   
                         echo'<div class="form-group has-error"><label>Cantidad horas(*)</label>';
                         $Cantidad_horas = array( 
                           'name' => 'cantidad_horas',  
                           'class' => 'form-control',                                         
                           'type' => 'number',
                           'min' => '0'  
                       );
                       echo form_input($Cantidad_horas);
                         echo '</div>';                            
                     }else
                     {
                         echo '<div class="form-group"><label>Cantidad horas(*)</label>';
                         $Cantidad_horas = array( 
                           'name' => 'cantidad_horas',  
                           'class' => 'form-control',                                         
                           'type' => 'number',
                           'min' => '0',
                           'value' => set_value("cantidad_horas")
                       );
                       echo form_input($Cantidad_horas);
                         echo '</div>';
                     }
                     ?>                 
               </div>
               <div class="col-md-6 col-s-12">
                  <?php                   
                     if(!empty(form_error('Imagen'))||!empty($this->upload->display_errors()))
                     {   
                         echo'<div class="form-group has-error"><label>Imagen(*)</label>';                       
                         $Imagen = array( 
                           'name' => 'Imagen',  
                           'class' => 'form-control',                                         
                           'type' => 'file',
                           'accept' => '.jpg, .jpeg, .png'                           
                       );
                       echo form_input($Imagen);
                         echo '</div>';                            
                     }else
                     {
                         echo '<div class="form-group"><label>Imagen(*)</label>';                      
                         $Imagen = array( 
                           'name' => 'Imagen',  
                           'class' => 'form-control',                                         
                           'type' => 'file',
                           'accept' => '.jpg, .jpeg, .png',
                           'value' => set_value("Imagen")                            
                       );
                       echo form_input($Imagen);
                         echo '<p class="help-block">Archivos validos .png .jpg .jpeg</p></div>';
                     }
                     ?>                
               </div>
               <div class="col-md-12 ">
                  <?php 
                     if(!empty(form_error('Contenido_Tematico')))
                     {   
                         echo'<div class="form-group has-error"><label>Contenido Tematico(*)</label>';
                         $Contenido_Tematico = array( 
                           'name' => 'Contenido_Tematico',  
                           'class' => 'form-control',                                         
                           'type' => 'textarea',
                           'rows' => 4                                       
                       );
                       echo form_textarea($Contenido_Tematico);
                         echo '</div>';                            
                     }else
                     {
                         echo '<div class="form-group"><label>Contenido Tematico(*)</label>';
                         $Contenido_Tematico = array( 
                           'name' => 'Contenido_Tematico',  
                           'class' => 'form-control',                                         
                           'type' => 'textarea',
                           'rows' => 4,
                           'value' => set_value("Contenido_Tematico")                                   
                       );
                       echo form_textarea($Contenido_Tematico);
                         echo '</div>';
                     }
                     ?>                 
               </div>
               <div class="col-md-12 ">
                  <div class="form-group">
                     <?php
                        $submit = array(                                           
                            'class' => 'btn btn-warning btn-block',
                            'value' => 'Crear'             
                        );
                        echo form_submit($submit);
                        ?> 
                  </div>
               </div>
            </div>
         </div>
         <!-- ./box-body -->           
      </div>
      <!-- /.box -->

   

   </section>
</div>