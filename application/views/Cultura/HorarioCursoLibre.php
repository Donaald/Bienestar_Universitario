<div class="content-wrapper">

      <section class="content">
      <form action="<?php echo base_url() . 'index.php/CursoLibre_C/asignarHorario' ?>" method="post">
          <div class="row">
              <div class="col-md-6 col-xs-12">
                  <div class="info-box bg-yellow">
                      <span class="info-box-icon">
                        <i class="fa fa-calendar"></i>
                      </span>
                      <div class="info-box-content"><br>
                        <select id="selectOpcionesInscripcion" name="idCursoLibre" class="form-control select2">
                          <option value="-1" hidden>Selecciones curso para asignar horario</option>
                        <?php
                          foreach ($cursosLibresDocente as $value) {
                            echo ' <option value="'.$value->idCurso_libre.'">'.$value->nombre.'</option>';  
                          }  
                        ?>
                        </select>
                      </div>
                    </div>                    
              </div>
              <div class="col-md-6 col-xs-12">

              <?php
              if(!empty($error))
              {
               echo '<div class="alert alert-danger alert-dismissible">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
               <h4><i class="icon fa fa-ban"></i>Error!</h4><h4>'.$error.'</h4></div>';  
              }
              ?>
                    
                    </div>
            </div>
           
        <div class="box box-warning">
          <div class="box-body">
            <table class="table table-bordered">
              <tr>
                <th class="text-center col-md-2">Hora</th>
                <th class="text-center col-md-2">Lunes</th>
                <th class="text-center col-md-2">Martes</th>
                <th class="text-center col-md-2">Miercoles</th>
                <th class="text-center col-md-2">Jueves</th>
                <th class="text-center col-md-2">Viernes</th>
              </tr>
              <tr>
                <td class="text-center">8:00 - 9:00</td>
                <td class="text-center">
                  <label class="checkbox-inline">
                    <input type="checkbox" name='horarios[]' value="Lunes_8">
                  </label>
                </td>
                <td class="text-center">
                  <label class="checkbox-inline">
                    <input type="checkbox" name='horarios[]' value="Martes_8">
                  </label>
                </td>
                <td class="text-center">
                  <label class="checkbox-inline">
                    <input type="checkbox" name='horarios[]' value="Miercoles_8">
                  </label>
                </td>
                <td class="text-center">
                  <label class="checkbox-inline">
                    <input type="checkbox" name='horarios[]' value="Jueves_8">
                  </label>
                </td>
                <td class="text-center">
                  <label class="checkbox-inline">
                    <input type="checkbox" name='horarios[]' value="Viernes_8">
                  </label>
                </td>
              </tr>
              <tr>
                <td class="text-center">9:00 - 10:00</td>
                <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Lunes_9">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Martes_9">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Miercoles_9">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Jueves_9">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Viernes_9">
                    </label>
                  </td>
              </tr>
              <tr>
                <td class="text-center">10:00 - 11:00</td>
                <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Lunes_10">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Martes_10">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Miercoles_10">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Jueves_10">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Viernes_10">
                    </label>
                  </td>
              </tr>
              <tr>
                <td class="text-center">11:00 - 12:00</td>
                <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Lunes_11">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Martes_11">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Miercoles_11">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Jueves_11">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Viernes_11">
                    </label>
                  </td>
              </tr>
              <tr>
                <td class="text-center">12:00 - 13:00</td>
                <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Lunes_12">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Martes_12">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Miercoles_12">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Jueves_12">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Viernes_12">
                    </label>
                  </td>
              </tr>
              <tr>
                <td class="text-center">13:00 - 14:00</td>
                <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Lunes_13">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Martes_13">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Miercoles_13">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Jueves_13">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Viernes_13">
                    </label>
                  </td>
              </tr>
              <tr>
                  <td class="text-center">14:00 - 15:00</td>
                  <td class="text-center">
                      <label class="checkbox-inline">
                        <input type="checkbox" name='horarios[]' value="Lunes_14">
                      </label>
                    </td>
                    <td class="text-center">
                      <label class="checkbox-inline">
                        <input type="checkbox" name='horarios[]' value="Martes_14">
                      </label>
                    </td>
                    <td class="text-center">
                      <label class="checkbox-inline">
                        <input type="checkbox" name='horarios[]' value="Miercoles_14">
                      </label>
                    </td>
                    <td class="text-center">
                      <label class="checkbox-inline">
                        <input type="checkbox" name='horarios[]' value="Jueves_14">
                      </label>
                    </td>
                    <td class="text-center">
                      <label class="checkbox-inline">
                        <input type="checkbox" name='horarios[]' value="Viernes_14">
                      </label>
                    </td>
              </tr>
              <tr>
                <td class="text-center">15:00 - 16:00</td>
                <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Lunes_15">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Martes_15">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Miercoles_15">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Jueves_15">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Viernes_15">
                    </label>
                  </td>
              </tr>
              <tr>
                <td class="text-center">16:00 - 17:00</td>
                <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Lunes_16">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Martes_16">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Miercoles_16">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Jueves_16">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Viernes_16">
                    </label>
                  </td>
              </tr>
              <tr>
                <td class="text-center">17:00 - 18:00</td>
                <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Lunes_17">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Martes_17">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Miercoles_17">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Jueves_17">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Viernes_17">
                    </label>
                  </td>
              </tr>
              <tr>
                <td class="text-center">18:00 - 19:00</td>
                <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Lunes_18">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Martes_18">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Miercoles_18">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Jueves_18">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Viernes_18">
                    </label>
                  </td>
              </tr>
              <tr>
                <td class="text-center">19:00 - 20:00</td>
                <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Lunes_19">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Martes_19">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Miercoles_19">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Jueves_19">
                    </label>
                  </td>
                  <td class="text-center">
                    <label class="checkbox-inline">
                      <input type="checkbox" name='horarios[]' value="Viernes_19">
                    </label>
                  </td>
              </tr>
            </table>
            <br>
            <br>
            <input type="submit" id="enviarHorario" value="Asignar Horario" class="btn btn-warning btn-block">

            <!-- /.box-body -->

          </div>
          </form>

      </section>
      </div>