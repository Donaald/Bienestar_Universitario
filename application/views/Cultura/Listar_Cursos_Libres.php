<div class="content-wrapper">
            <!-- Main content -->
            <section class="content">
               <div class="box">
                  <div class="box-header">
                     <h2 class="box-title">Listado de cursos libres</h2>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                     <table class="table table-bordered table-striped">
                        <thead>
                           <tr>
                              <th>Nombre</th>
                              <th>Maximo Cupos</th>
                              <th>Inscritos</th>
                              <th>Cantidad horas</th>
                              <th>Descripcion</th>
                              <th>Docente</th>
                              <th>Eliminar</th>
                              <th>Modificar</th>
                           </tr>
                        </thead>
                        <tbody>
<?php
if(!empty($cursosLibres))
{
      foreach ($cursosLibres as $valor) {
            echo '<tr>';
            echo '<td>' . $valor->nombre . '</td>';
            echo '<td>' . $valor->cupo_maximo . '</td>';
            echo '<td>' . $valor->inscritos . '</td>';
            echo '<td>' . $valor->duracion . '</td>';
            echo '<td>' . $valor->contenido_tematico . '</td>';
            echo '<td>' . $valor->nombre_docente . ' ' . $valor->apellido_docente . '</td>';
            echo '<td><div class="color-palette-set" align="center"><a id="deleteCursoLibre" href="' . base_url() . 'index.php/CursoLibre_C/listarId/' . $valor->idCurso_libre . '"><div class="bg-red-active color-palette"><span class="fa fa-trash"></span></div></a></div></td>';
            echo '<td><div class="color-palette-set" align="center"><a id="updateCursoLibre" href="' . base_url() . 'index.php/CursoLibre_C/listarId/' . $valor->idCurso_libre . '"><div class="bg-light-blue-active color-palette"><span class="fa fa-pencil-square-o"></span></div></a></div></td>';
            echo '</tr>';
        }
}else
{
      echo '<tr>';
      echo '<td> Vacio </td>';
      echo '<td> Vacio </td>';
      echo '<td> Vacio </td>';
      echo '<td> Vacio </td>';
      echo '<td> Vacio </td>';
      echo '<td> Vacio </td>';
      echo '<td> Vacio </td>';
      echo '<td> Vacio </td>';
      echo '</tr>';
}

?>
                           </tbody>
                     </table>
                  </div>
                  <!-- /.box-body -->
               </div>
               <!-- /.box -->

               </div>
               <!-- /.modal -->
               <!-- /.Modal -->
               <div class="modal fade" id="modal_modificar">
                  <div class="modal-dialog">
                     <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                           <span aria-hidden="true">&times;</span></button>
                           <h4 class="modal-title">Modificar datos Curso libre</h4>
                        </div>
                        <div class="modal-body">
                        <form id="formularioModificar" action="<?php echo base_url() . 'index.php/CursoLibre_C/modificarCursoLibre' ?>" method="post">
                           <div class="row">
                              <div class="col-md-6 col-s-12">
                                 <div class="form-group">
                                    <label for="Nombre">Nombre</label>
                                    <input name="nombre" id="nombre" class="form-control" type="text" value="">
                                 </div>
                              </div>
                              <div class="col-md-6 col-s-12">
                                 <div class="form-group">
                                    <label for="Cantidad cupos">Cantidad cupos</label>
                                    <input name="cantidad_cupos" id="cantidad_cupos" class="form-control" type="number" value="">
                                 </div>
                              </div>
                              <div class="col-md-6 col-s-12">
                                 <div class="form-group">
                                    <label for="Docente">Docente</label>
                                    <div id="docente"></div>
                                 </div>
                              </div>
                              <div class="col-md-6 col-s-12">
                                 <div class="form-group">
                                    <label for="Cantidad horas">Cantidad horas</label>
                                    <input id="cantidad_horas" name="cantidad_horas" class="form-control" type="number" value="">
                                 </div>
                              </div>
                              <div class="col-md-12 ">
                                 <div class="form-group">
                                    <textarea name="Contenido_Tematico" id="Contenido_Tematico" class="form-control" rows="5" placeholder=""></textarea>
                                 </div>
                              </div>
                              <input type="hidden" id="idCusoLibre" name="idCusoLibre">
                           </div>
                        </div>
                        <div class="modal-footer">
                           <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
                           <button id="actualizarCursoLibre" type="submit" class="btn btn-primary">Guardar cambios</button>
                        </div>
                        </form>
                     </div>
                     <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
               </div>
               <!-- /.modal -->
            </section>
            <!-- /.content -->

