<div class="content-wrapper">
            <!-- Main content -->
            <section class="content">
               <div class="box">
                  <div class="box-header">
                     <h2 class="box-title">Listado de cursos libres</h2>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                     <table class="table table-bordered table-striped">
                        <thead>
                           <tr>
                              <th>Nombre</th>
                              <th>Inscritos</th>
                              <th>Descripcion</th>
                              <th>Docente</th>
                              <th>Inscritos</th>
                              <th>Eliminar</th>
                              <th>Modificar</th>
                           </tr>
                        </thead>
                        <tbody>
<?php
if (!empty($semillero)) {
    foreach ($semillero as $valor) {
        echo '<tr>';
        echo '<td>' . $valor->nombre . '</td>';
        echo '<td>' . $valor->inscritos . '</td>';
        echo '<td>' . $valor->contenido_tematico . '</td>';
        echo '<td>' . $valor->nombre_docente . ' ' . $valor->apellido_docente . '</td>';
        echo '<td><div class="color-palette-set" align="center"><a id="inscritos" href="' . base_url() . 'index.php/Semilleros_C/inscritos/' . $valor->idsemillero . '"><div class="bg-red-active color-palette"><span class="fa fa-users"></span></div></a></div></td>';
        echo '<td><div class="color-palette-set" align="center"><a id="deleteSemillero" href="' . base_url() . 'index.php/Semilleros_C/listarId/' . $valor->idsemillero . '"><div class="bg-red-active color-palette"><span class="fa fa-trash"></span></div></a></div></td>';
        echo '<td><div class="color-palette-set" align="center"><a id="updateSemillero" href="' . base_url() . 'index.php/Semilleros_C/listarId/' . $valor->idsemillero . '"><div class="bg-light-blue-active color-palette"><span class="fa fa-pencil-square-o"></span></div></a></div></td>';
        echo '</tr>';
    }
} else {
    echo '<tr>';
    echo '<td> Vacio </td>';
    echo '<td> Vacio </td>';
    echo '<td> Vacio </td>';
    echo '<td> Vacio </td>';
    echo '<td> Vacio </td>';
    echo '<td> Vacio </td>';
    echo '<td> Vacio </td>';
    echo '<td> Vacio </td>';
    echo '</tr>';
}

?>
                           </tbody>
                     </table>
                  </div>
                  <!-- /.box-body -->
               </div>
               <!-- /.box -->

         
            <!-- /.Modal -->
      <div class="modal fade" id="modal_modificar">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title">Modificar datos Curso libre</h4>
            </div>
            <div class="modal-body">
               <form id="formularioModificarSemillero" action="<?php echo base_url() . 'index.php/Semilleros_C/modificarSemillero' ?>" method="post">
                  <div class="row">
                     <div class="col-md-6 col-s-12">
                        <div class="form-group">
                           <label for="Nombre">Nombre</label>
                           <input name="nombre" id="nombre" class="form-control" type="text" value="">
                        </div>
                     </div>                   
                     <div class="col-md-6 col-s-12">
                        <div class="form-group">
                           <label for="Docente">Docente</label>
                           <div id="docente"></div>
                        </div>
                     </div>                    
                     <div class="col-md-12 ">
                        <div class="form-group">
                           <textarea name="Contenido_Tematico" id="Contenido_Tematico" class="form-control" rows="5" placeholder=""></textarea>
                        </div>
                     </div>
                     <input type="hidden" id="idSemillero" name="idSemillero">
                  </div>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
            <button id="actualizarSemillero" type="submit" class="btn btn-primary">Guardar cambios</button>
            </div>
            </form>
         </div>
         <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
   </div>
   <!-- /.modal -->

  
</section>
<!-- /.content -->
</div>