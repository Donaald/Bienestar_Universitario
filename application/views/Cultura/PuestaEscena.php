<div class="content-wrapper">
            <!-- Content Header (Page header) --> 
            <!-- Main content -->
            <section class="content">
               <div class="box box-warning">
                  <div class="box-header with-border">
                     <i class="fa fa-tag"></i>            
                     <h3 class="box-title">Escenarios</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                     <div class="row">
                        <div class="col-md-3 col-s-12">
                        <?php 
                     if(!empty(form_error('escenario')))
                     {   
                         echo'<div class="form-group has-error"><label>Escenario(*)</label>';
                         $Escenario = array( 
                           'id' => 'escenario',
                           'name' => 'escenario',  
                           'class' => 'form-control select2'                                                              
                       );  
                       $opcionesEscenario['-1'] = 'Selecione el Escenario';                                     
                       foreach($escenarios as $value)
                       {                               
                        $opcionesEscenario[$value->idescenario] = $value->nombre;
                       }                
                         echo form_dropdown($Escenario, $opcionesEscenario);
                         echo '</div>';                            
                     }else
                     {
                         echo '<div class="form-group"><label>Escenario(*)</label>';
                         $Escenario = array( 
                           'id' => 'escenario',
                           'name' => 'escenario',  
                           'class' => 'form-control select2',
                           'value' => set_value("escenario")
                       );     
                       $opcionesEscenario['-1'] = 'Selecione el Escenario';                                        
                       foreach($escenarios as $value)
                       {                               
                        $opcionesEscenario[$value->idescenario] = $value->nombre;
                       }
                       echo form_dropdown($Escenario, $opcionesEscenario);
                       echo '</div>';
                     }
                     ?>   
                        </div>
                        <div class="col-md-3 col-s-12">
                           <div class="form-group" id="selectActividades">
                              <label>Actividad(*)</label>
                              <select id="actividades" name="actividad" class="form-control" style="width: 100%;">
                                 <option hidden>Selecionar Actividad</option>
                                 <option>Actividad 1</option>
                                 <option>Actividad 2</option>
                                 <option>Actividad 3</option>
                                 <option>Actividad 4</option>
                                 <option>Actividad 5</option>
                                 <option>Actividad 6</option>
                                 <option>Actividad 7</option>
                                 <option>Actividad 8</option>
                                 <option>Actividad 9</option>
                                 <option>Actividad 10</option>
                                 <option>Actividad 11</option>
                                 <option>Actividad 12</option>
                              </select>
                           </div>
                           <!-- /.form-group -->   
                        </div>
                        <div class="col-md-3 col-s-12">
                           <div class="form-group">
                              <label>Grupo Representativo</label>
                              <select class="form-control" style="width: 100%;">
                                 <option hidden>Selecionar Grupo</option>
                                 <option>Grupo 1</option>
                                 <option>Grupo 2</option>
                                 <option>Grupo 3</option>
                                 <option>Grupo 4</option>
                              </select>
                           </div>
                           <!-- /.form-group -->   
                        </div>
                        <div class="col-md-3 col-s-12">
                           <div class="form-group">
                              <label>Horas Libres</label>
                              <input type="number" min="0" class="form-control">
                           </div>
                           <!-- /.form-group -->   
                        </div>
                        <div class="col-xs-12">
                           <button type="button" class="btn btn-warning btn-block">Asignar</button>  
                        </div>
                     </div>
                  </div>
                  <!-- /.box-body -->
               </div>
               <!-- /.box -->
               <div class="box box-warning">
                  <div class="box-header with-border">
                     <i class="fa fa-tag"></i>            
                     <h3 class="box-title">Eventos Externos</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                     <div class="row">
                        <div class="col-md-3 col-s-12">
                           <div class="form-group">
                              <label>Grupo Representativo</label>
                              <select class="form-control" style="width: 100%;">
                                 <option hidden>Selecionar Grupo</option>
                                 <option>Grupo 1</option>
                                 <option>Grupo 2</option>
                                 <option>Grupo 3</option>
                                 <option>Grupo 4</option>
                              </select>
                           </div>
                           <!-- /.form-group -->  
                        </div>
                        <div class="col-md-3 col-s-12">
                           <div class="form-group">
                              <label>Nombre del evento</label>
                              <input type="text" class="form-control">
                           </div>
                        </div>
                        <div class="col-md-3 col-s-12">
                           <div class="form-group">
                              <label>Lugar</label>
                              <input type="text" class="form-control">
                           </div>
                        </div>
                        <div class="col-md-3 col-s-12">
                           <div class="form-group">
                              <label>Horas Libres</label>
                              <input type="number" min="0" class="form-control">
                           </div>
                           <!-- /.form-group -->   
                        </div>
                        <div class="col-xs-12">
                           <button type="button" class="btn btn-warning btn-block">Participar</button>  
                        </div>
                     </div>
                  </div>
                  <!-- /.box-body -->
               </div>
               <!-- /.box -->
               <div class="box box-warning">
                  <div class="box-header with-border">
                     <i class="fa fa-tag"></i>            
                     <h3 class="box-title">Apoyo Dependencias</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                     <div class="row">
                        <div class="col-md-3 col-s-12">
                           <div class="form-group">
                              <label>Grupo Representativo</label>
                              <select class="form-control" style="width: 100%;">
                                 <option hidden>Selecionar Grupo</option>
                                 <option>Grupo 1</option>
                                 <option>Grupo 2</option>
                                 <option>Grupo 3</option>
                                 <option>Grupo 4</option>
                              </select>
                           </div>
                           <!-- /.form-group -->  
                        </div>
                        <div class="col-md-3 col-s-12">
                           <div class="form-group">
                              <label>Nombre del evento</label>
                              <input type="text" class="form-control">
                           </div>
                        </div>
                        <div class="col-md-3 col-s-12">
                           <div class="form-group">
                              <label>Lugar</label>
                              <input type="text" class="form-control">
                           </div>
                        </div>
                        <div class="col-md-3 col-s-12">
                           <div class="form-group">
                              <label>Departamento</label>
                              <input type="text" class="form-control">
                           </div>
                        </div>
                        <div class="col-md-3 col-s-12">
                           <div class="form-group">
                              <label>Horas Libres</label>
                              <input type="number" min="0" class="form-control">
                           </div>
                           <!-- /.form-group -->   
                        </div>
                        <div class="col-xs-12">
                           <button type="button" class="btn btn-warning btn-block">Participar</button>  
                        </div>
                     </div>
                  </div>
                  <!-- /.box-body -->
               </div>
               <!-- /.box -->
            </section>
         </div>