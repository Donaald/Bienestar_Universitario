<div class="content-wrapper">

          <section class="content">
          <?php
         if(!empty(validation_errors()))
         {
          echo '<div class="alert alert-danger alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h4><i class="icon fa fa-ban"></i>Error!</h4>'.validation_errors().'</div>';  
         }
         ?>
             <div class="box">
                <div class="box-header with-border">
                   <h3 class="box-title">Datos ingreso Escenarios</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form action="<?php echo base_url() . 'index.php/Escenario_C/validarFormulario' ?>" method="post">
                   <div class="row">
                      <div class="col-md-6 col-s-12">
                      <?php
if (!empty(form_error('nombre'))) {

    echo '<div class="form-group has-error"><label>Nombre(*)</label>';
    $Nombre = array(
        'name' => 'nombre',
        'class' => 'form-control',
        'maxlength' => '250',
        'type' => 'text',
    );
    echo form_input($Nombre);
    echo '</div>';
} else {
    echo '<div class="form-group"><label>Nombre(*)</label>';
    $Nombre = array(
        'name' => 'nombre',
        'class' => 'form-control',
        'maxlength' => '250',
        'type' => 'text',
        'value' => set_value("nombre"),
    );
    echo form_input($Nombre);
    echo '</div>';
}
?>
                      </div>
                      <div class="col-md-6 col-s-12">

                      <?php
if (!empty(form_error('fecha'))) {
    echo '<div class="form-group has-error">
                       <label>Fecha(*)</label>
                       <div class="input-group date">
                         <div class="input-group-addon">
                           <i class="fa fa-calendar"></i>
                         </div>
                         <input type="text" name="fecha" class="form-control pull-right" value="' . set_value('fecha') . '" id="datepicker">
                       </div>
                     </div>';

} else {
    echo '<div class="form-group">
                      <label>Fecha(*)</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="fecha" class="form-control pull-right" value="' . set_value('fecha') . '" id="datepicker">
                      </div>
                    </div>';
}
?>


                      </div>
                      <div class="col-md-6 col-s-12">


                      <?php
if (!empty(form_error('rango_fecha'))) {
    echo '<div class="form-group has-error">
                       <label>Duracion (dias)(*)</label>
                       <div class="input-group date">
                         <div class="input-group-addon">
                           <i class="fa fa-calendar"></i>
                         </div>
                         <input type="text" name="rango_fecha" class="form-control pull-right" value="' . set_value('rango_fecha') . '" id="reservation">
                       </div>
                     </div>';

} else {
    echo '<div class="form-group">
                      <label>Duracion (dias)(*)</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="rango_fecha" class="form-control pull-right" value="' . set_value('rango_fecha') . '" id="reservation">
                      </div>
                    </div>';
}
?>


                      </div>
                      <div class="col-md-6 col-s-12">
                      <?php
if (!empty(form_error('lugar'))) {

    echo '<div class="form-group has-error"><label>Lugar(*)</label>';
    $lugar = array(
        'name' => 'lugar',
        'class' => 'form-control',
        'maxlength' => '250',
        'type' => 'text',
    );
    echo form_input($lugar);
    echo '</div>';
} else {
    echo '<div class="form-group"><label>Lugar(*)</label>';
    $lugar = array(
        'name' => 'lugar',
        'class' => 'form-control',
        'maxlength' => '250',
        'type' => 'text',
        'value' => set_value("lugar"),
    );
    echo form_input($lugar);
    echo '</div>';
}
?>
                      </div>
                      <div class="col-md-12 ">

                      <?php
if (!empty(form_error('justificacion'))) {
    echo '<div class="form-group has-error"><label>Justificacion(*)</label>';
    $justificacion = array(
        'name' => 'justificacion',
        'class' => 'form-control',
        'type' => 'textarea',
        'rows' => 5,
    );
    echo form_textarea($justificacion);
    echo '</div>';
} else {
    echo '<div class="form-group"><label>Justificacion(*)</label>';
    $justificacion = array(
        'name' => 'justificacion',
        'class' => 'form-control',
        'type' => 'textarea',
        'rows' => 5,
        'value' => set_value("justificacion"),
    );
    echo form_textarea($justificacion);
    echo '</div>';
}
?>



                      </div>
                      <div class="col-md-12 ">
                         <div class="form-group">
                            <button type="submit" class="btn btn-warning btn-block">Crear</button>
                         </div>
                      </div>
                   </div>
                </div>
                </form>
                <!-- ./box-body -->
             </div>
             <!-- /.box -->
          </section>
          <!-- /.content -->
       </div>
       <!-- /.content-wrapper -->