<div class="content-wrapper">
      <section class="content">

      <?php
if (!empty(validation_errors())) {
    echo '<div class="row">
                <div class="col-sm-2"></div>
                  <div class="col-sm-8 col-xs-12">
                    <div class="callout callout-danger">
                      <h4>Error!</h4>
                      <p>' . validation_errors() . '</p>
                    </div>
                  </div>
                <div class="col-sm-2"></div>
              </div>';
}
?>
      <form action="<?php echo base_url() . 'index.php/CursoLibre_C/tomarAsistencia' ?>" method="post">
          <br>
          <div class="box box-warning">
            <div class="box-header">
              <div class="row">
                <div class="col-md-6">
                <?php
if (!empty(form_error('fecha'))) {
    echo '<div class="form-group has-error">
                       <label>Fecha:</label>
                       <div class="input-group date">
                         <div class="input-group-addon">
                           <i class="fa fa-calendar"></i>
                         </div>
                         <input type="text" name="fecha" class="form-control pull-right" value="' . set_value('fecha') . '" id="datepicker">
                       </div>
                     </div>';

} else {
    echo '<div class="form-group">
                      <label>Fecha:</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="fecha" class="form-control pull-right" value="' . set_value('fecha') . '" id="datepicker">
                      </div>
                    </div>';
}
?>

                </div>
                <div class="col-md-6">
                <?php
if (!empty(form_error('idCursoLibre'))) {
    echo '<div class="form-group has-error"><label>Cursos Libres:(*)</label>';
    $cursoLibre = array(
        'id' => 'cursosLibresDocente',
        'name' => 'idCursoLibre',
        'class' => 'form-control',
    );
    $opcionesCursoLibre['-1'] = 'Selecione el curso libre';
    foreach ($cursosLibres as $value) {
        $opcionesCursoLibre[$value->idCurso_libre] = $value->nombre;
    }
    echo form_dropdown($cursoLibre, $opcionesCursoLibre);
    echo '</div>';
} else {
    echo '<div class="form-group"><label>Cursos Libres:(*)</label>';
    $cursoLibre = array(
        'id' => 'cursosLibresDocente',
        'name' => 'idCursoLibre',
        'class' => 'form-control',
        'value' => set_value("idCursoLibre"),
    );
    $opcionesCursoLibre['-1'] = 'Selecione el curso libre';
    foreach ($cursosLibres as $value) {
        $opcionesCursoLibre[$value->idCurso_libre] = $value->nombre;
    }
    echo form_dropdown($cursoLibre, $opcionesCursoLibre);
    echo '</div>';
}
?>

                  </select>
                </div>
              </div>
            </div>
            <div class="box-body">
              <table class="table table-bordered table-striped">
              <thead>
              <tr>
                 <th>Estudiante</th>
                 <th>Clases asistidas</th>
                 <th>Asistencia</th>
                 <th>Eliminar</th>
              </tr>
           </thead>
                <tbody align="center">
                </tbody>
              </table>
              <br>
              <br>


                  <input type="submit" value="Tomar Asistencia" class="btn btn-warning btn-block">

            </div>
          </div>
          </form>



      </section>
    </div>