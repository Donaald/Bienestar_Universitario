<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">
<header class="main-header">
   <!-- Logo -->
   <a href="" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b>LTE</span>
   </a>
   <!-- Header Navbar: style can be found in header.less -->
   <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
         <ul title="Salir" class="nav navbar-nav">          
            <li class="">
               <a href=<?php echo '"'.base_url().'index.php/Login/logout"'; ?>
               <i class="fa fa-sign-out"></i>          
               </a>
            </li>
         </ul>
      </div>
   </nav>
</header>
<!-- Left side column. contains the logo and sidebar -->
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
   <!-- sidebar: style can be found in sidebar.less -->
   <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
         <div class="pull-left image">
            <img src="" class="img-circle" alt="">
         </div>
         <div class="pull-left info">
            <p>Donald Torres</p>
         </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul id="menu" class="sidebar-menu" data-widget="tree">
     
      </ul>
   </section>
   <!-- /.sidebar -->
</aside>
<!-- Content Wrapper. Contains page content -->