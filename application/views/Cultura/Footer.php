<footer class="main-footer">
<div class="pull-right hidden-xs">
   <b>Version</b> 1.0
</div>
<strong>Copyright &copy; 2017 <a href="../../VistasCompletas/Extras/Autores.html">Creadores.</a></strong> Todos los derechos reservados.
</footer>
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<!-- jQuery 3 -->
<script src="<?php echo base_url(); ?>Vistas/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url(); ?>Vistas/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>Vistas/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>Vistas/dist/js/adminlte.min.js"></script>
<!-- Sweet Alert 2 -->
<script src="<?php echo base_url(); ?>Vistas/dist/js/sweetalert2.js"></script>

<!-- DataTables -->
<script src="<?php echo base_url(); ?>Vistas/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>Vistas/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url(); ?>Vistas/bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo base_url(); ?>Vistas/dist/js/menuDinamico.js"></script>
<script src="<?php echo base_url(); ?>Vistas/dist/js/notificaciones.js"></script>
<script src="<?php echo base_url(); ?>Vistas/dist/js/peticiones.js"></script>
<script src="<?php echo base_url(); ?>Vistas/dist/js/CRUDcursoLibre.js"></script>
<script src="<?php echo base_url(); ?>Vistas/dist/js/CRUDusuarioCursoLibre.js"></script>
<script src="<?php echo base_url(); ?>Vistas/dist/js/CRUDescenario.js"></script>
<script src="<?php echo base_url(); ?>Vistas/dist/js/CRUDsemillero.js"></script>
<script src="<?php echo base_url(); ?>Vistas/dist/js/bootstrap-notify.min.js"></script>
<!-- date-range-picker -->
<script src="<?php echo base_url(); ?>Vistas/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url(); ?>Vistas/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap time picker -->
<script src="<?php echo base_url(); ?>Vistas/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!--   Sharrre Library    -->
<script src="http://demos.creative-tim.com/material-dashboard-pro/assets/js/jquery.sharrre.js"></script>
<!-- DatePicker -->
<script src="<?php echo base_url(); ?>Vistas/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>Vistas/bower_components/bootstrap-datepicker/js/locales/bootstrap-datepicker.es.js" charset="UTF-8"></script>

<script src="http://www.chartjs.org/dist/2.6.0/Chart.bundle.js"></script>
<script src="http://www.chartjs.org/samples/latest/utils.js"></script>

<script type="text/javascript">
$(function () {
    $('#datepicker').datepicker({
      autoclose: true,
      language: 'es',
      format:'yyyy/mm/dd'
    });

  $('table').DataTable(
    {
        "language":
        {
          "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        }
    }

  );
  $('#reservation').daterangepicker(
    {
      format:'yyyy/mm/dd',
      timePickerIncrement: 365
    }
  );
   //Timepicker
   $('.timepicker').timepicker({
      showInputs: false,
      showMeridian: false      
    })
         

});
</script>





</body>
</html>