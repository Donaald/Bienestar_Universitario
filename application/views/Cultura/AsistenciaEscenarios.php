<div class="content-wrapper">
                  <!-- Main content -->
                  <section class="content">
                  <div id="contenidoEscenarios">
                        <?php
$dataTables = '';
foreach ($Escenarios as $value) {
    echo '<div class="box box-warning collapsed-box">';
    echo '<div class="box-header with-border">';
    echo '<h3 class="box-title">';
    echo '<b>' . $value->nombre . '</b>';
    echo '</h3>';
    echo '<div class="box-tools pull-right">';
    echo '<button id="" href="' . base_url() . 'index.php/Escenario_C/listar_actividades_escenario_asistencia/' . $value->idescenario . '" value="' . $value->idescenario . '"  class="btn btn-box-tool listarActividad" data-widget="collapse">';
    echo '<i class="fa fa-plus"></i>';
    echo '</button>';
    echo '</div>';
    echo '</div>';
    echo '<div class="box-body no-padding">';
    echo '<div class="box box-solid with-border">';
    echo '<div class="box-body">';
    echo '<dl class="dl-horizontal">';
    echo '<dt>Fecha:</dt>';
    echo '<dd>' . $value->fecha . '</dd>';
    echo '<dt>Duracion:</dt>';
    echo '<dd>' . $value->duracion . ' dia(s)</dd>';
    echo '<dt>Lugar:</dt>';
    echo '<dd>' . $value->lugar . '</dd>';
    echo '<dt>Justificacion:</dt>';
    echo '<dd>' . $value->justificacion . '</dd>';
    echo ' </dl>';
    echo '</div> ';
    echo '</div>';
    echo '<table id="' . $value->idescenario . '" class="table table-bordered table-striped">';
    echo '<thead>';
    echo '<tr>';
    echo '<th>Nombre</th>';
    echo '<th>Duracion</th>';
    echo '<th>Fecha</th>';
    echo '<th>Hora</th>';
    echo '<th>Lugar</th>';
    echo '<th>Asistencia</th>';
    echo '</tr>';
    echo '</thead>';
    echo '<tbody>';
    echo '</tbody>';
    echo '</table>';
    echo '</div> ';
    echo '</div>';

    $dataTables .= "$('table#" . $value->idescenario . "').DataTable(
                                    {
                                        'language':
                                        {
                                          'url': 'https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
                                        }
                                    }

                                  );
                                  ";

}

?>

                             </div>







               <!-- /.Modal -->
               <div class="modal fade" id="conId">
                  <div class="modal-dialog">
                     <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                           <span aria-hidden="true">&times;</span></button>
                           <h4 class="modal-title">Actividad 1</h4>
                        </div>
                        <div class="modal-body">
                            <form id="formularioTomarAsistencia" action="<?php echo base_url().'index.php/Escenario_C/tomarAsistencia' ?>" method="post">
                           <div class="row">
                              <div class="col-xs-12">
                                 <div class="form-group">
                                    <label>ID Universitario</label>
                                    <select class="form-control select2" name="idUniversitario" style="width:100%;">
                                       <option>000111111</option>
                                       <option>000111112</option>
                                       <option>000111113</option>
                                       <option>000111114</option>
                                       <option>000111115</option>
                                       <option>000111116</option>
                                       <option>000111117</option>
                                       <option>000211111</option>
                                       <option>000311112</option>
                                       <option>000411113</option>
                                       <option>000511114</option>
                                       <option>000611115</option>
                                       <option>000711116</option>
                                       <option>000811117</option>
                                    </select>
                                 </div>
                                 <div class="col-xs-12 ">
                                  <input type="hidden" name="idactividad" id="idactividad">                                 
                                 <div class="form-group">
                                    <button type="submit" name="conid" value="conid" class="btn btn-warning btn-block">Asistio</button>
                                 </div>
                              </div>
                              </div>                             
                           </div>
                           </form>
                        </div>
                     </div>
                     <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
               </div>
               <!-- /.modal -->
               <!-- /.Modal -->
               <div class="modal fade" id="sinId">
                  <div class="modal-dialog">
                     <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                           <span aria-hidden="true">&times;</span></button>
                           <h4 class="modal-title">Actividad 1</h4>
                        </div>
                        <div class="modal-body">
                            <form id="formularioTomarAsistencia" action="<?php echo base_url().'index.php/Escenario_C/tomarAsistencia' ?>" method="post">
                           <div class="row">
                              <div class="col-md-6 col-s-12">
                                 <div class="form-group">
                                    <label>Tipo de documento</label>
                                    <select class="form-control" style="width:100%;">
                                       <option>C.C</option>
                                       <option>T.I</option>
                                       <option>T.E</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="col-md-6 col-s-12">
                                 <div class="form-group">
                                    <label>Num. documento</label>
                                    <input type="text" class="form-control">
                                 </div>
                              </div>
                              <div class="col-md-6 col-s-12">
                                 <div class="form-group">
                                    <label>Primer nombre</label>
                                    <input type="text" class="form-control">
                                 </div>
                              </div>
                              <div class="col-md-6 col-s-12">
                                 <div class="form-group">
                                    <label>Segundo nombre</label>
                                    <input type="text" class="form-control">
                                 </div>
                              </div>
                              <div class="col-md-6 col-s-12">
                                 <div class="form-group">
                                    <label>Primer apellido</label>
                                    <input type="text" class="form-control">
                                 </div>
                              </div>
                              <div class="col-md-6 col-s-12">
                                 <div class="form-group">
                                    <label>Segundo apellido</label>
                                    <input type="text" class="form-control">
                                 </div>
                              </div>
                              <div class="col-md-6 col-s-12">
                                 <div class="form-group">
                                    <label>Correo electronico</label>
                                    <input type="email" class="form-control">
                                 </div>
                              </div>
                              <div class="col-md-6 col-s-12">
                                 <div class="form-group">
                                    <label>Genero</label>
                                    <select class="form-control" style="width:100%;">
                                       <option hidden></option>
                                       <option>Masculino</option>
                                       <option>Femenino</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="col-s-12 ">
                              <input type="hidden" name="idactividad" id="idactividad">                                 
                                 <div class="form-group">
                                    <button type="submit" name="sinid" value="sinid" class="btn btn-warning btn-block">Asistio</button>
                                 </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
               </div>
               <!-- /.modal -->
            </section>
            <!-- /.content -->
         </div>
         <!-- /.content-wrapper -->