<div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <!-- Main content -->
            <section class="content">
                <div class="row">


                <?php
                
                    foreach($cursosLibres as $valores)
                    {
                        echo '<div class="col-md-3 col-xs-12">';
                        echo '<div class="box box-default">';
                        echo '<div class="box-body box-profile">';
                        echo '<div align="center">';
                        echo '<img id="perfil" class="" src="'.base_url().'archivos/'.$valores->imagen.'">';
                        echo '</div>';
                        echo '<h3 class="profile-username text-center"><div style="vertical-align: middle;"><p class="" style="height: 50px;">'.$valores->nombre.'</p></div></h3>';
                        echo '<p class="text-muted text-center">Docente: '.$valores->nombre_docente.' '.$valores->apellido_docente.'</p>';
                        echo '<ul class="list-group list-group-unbordered">';
                        echo '<li class="list-group-item">';
                        echo '<b>Inscritos:</b>';
                        echo '<p class="pull-right">20</p>';
                        echo '</li>';
                        echo '<li class="list-group-item">';    
                        echo '<b>Cupos totales:</b>';  
                        echo '<p class="pull-right">'.$valores->cupo_maximo.'</p>';  
                        echo '</li>';  
                        echo '<li class="list-group-item">';  
                        echo '<b>Cantidad de horas:</b>';  
                        echo '<p class="pull-right">'.$valores->duracion.'</p>';  
                        echo '</li>';  
                        echo '<li class="list-group-item text-center">';  
                        echo '<div class="row">';  
                        echo '<div class="col-lg-6 col-md-12">';       
                        echo '<b class="btn btn-default btn-block" data-toggle="modal" data-target="#horarioCursoLibre">Horario</b>'; 
                        echo '</div>'; 
                        echo '<div class="col-lg-6 col-md-12">'; 
                        echo '<b class="btn btn-default btn-block" data-toggle="modal" data-target="#contenidoTematico">Descripcion</b>'; 
                        echo '</div>'; 
                        echo '</div>'; 
                        echo '</li>'; 
                        echo '</ul>'; 
                        echo '<a href="'.base_url().'index.php/CursoLibre_C/inscripcion/'.$valores->idCurso_libre.'" class="btn btn-warning btn-block inscripcion">'; 
                        echo '<b>Incribirse</b>'; 
                        echo '</a>'; 
                        echo '</div>'; 
                        echo '</div>'; 
                        echo '</div>';                    
                    }

                ?>



                    
               
                

                </div>


                <!-- /.Modal -->
                <div class="modal fade" id="contenidoTematico">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h4 align="center" class="modal-title">Piano</h4>
                            </div>
                            <div class="modal-body">
                                aqui va la descripcion del curso libre
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->

                <div class="modal fade" id="horarioCursoLibre">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h4 align="center" class="modal-title">Piano</h4>
                            </div>
                            <div class="modal-body">
                                <ul class="list-group list-group-unbordered">
                                    <li class="list-group-item  text-center">
                                        <b>Horario:</b>
                                        <br>
                                        <table class="table table-condensed text-center">
                                            <tr>
                                                <th>Dia</th>
                                                <th>Hora</th>
                                            </tr>
                                            <tr>
                                                <td>Lunes</td>
                                                <td>8:00 - 9:00</td>
                                            </tr>
                                            <tr>
                                                <td>Lunes</td>
                                                <td>9:00 - 10:00</td>
                                            </tr>
                                            <tr>
                                                <td>Jueves</td>
                                                <td>14:00 - 15:00</td>
                                            </tr>
                                            <tr>
                                                <td>Jueves</td>
                                                <td>15:00 - 16:00</td>
                                            </tr>
                                        </table>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->



            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
