<div class="content-wrapper">
                  <!-- Main content -->
                  <section class="content">


 <div class="box box-warning">
                              <div class="box-header with-border text-center">
                                    <h2 class="box-title">Escenarios</h2>
                              </div>
                              <div class="box-body">
                                    <br>

                                    <div class="row">
                                          <div class="col-md-4">
                                                <div class="form-group" id="contenedorlistaescenario">
                                          <select class="form-control select2 " id="escenario" style="width: 100%;">
                                          <option value="-1" hidden>Selecione un Escenario</option>

                                          <?php

foreach ($Escenarios as $value) {

    echo '<option value="' . $value->idescenario . '">' . $value->nombre . '</option>';

}
?>
                                                 </select>
                                                 </div>
                                          </div>                                          
                                          <div class="col-md-4">
                                                <button id="agregarActividad" class="btn btn-warning btn-block">Nueva Actividad</button>
                                          </div>
                                          <div class="col-md-4">
                                                <button class="btn btn-warning btn-block eliminarEscenario">Eliminar Escenario</button>
                                          </div>
                                    </div>
                                    <br>
                                    <div id="infoEscenario">
                                         
                                          </div>
                                          <br>
                                          <div id="tabla">
                                                <table class="table table-bordered table-hover">
                                                      <thead>
                                                            <tr>
                                                                  <th>Nombre</th>
                                                                  <th>Duracion</th>
                                                                  <th>Descripcion</th>
                                                                  <th>Fecha</th>
                                                                  <th>Hora</th>
                                                                  <th>Lugar</th>
                                                                  <th>Publico objetivo</th>
                                                                  <th>Eliminar</th>                                                                  
                                                            </tr>
                                                      </thead>
                                                      <tbody>
                                                   
                                                      </tbody>
                                                </table>
                                          </div>
                                    </div>
                              </div>






                        <div class="modal fade" id="agregar-actividad">
                              <form id="formularioNuevaActividad" action="<?php echo base_url() . 'index.php/Escenario_C/agregarActividad' ?>" method="post">
                              <div class="modal-dialog">
                                    <div class="modal-content">
                                          <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                </button>
                                                <h4 class="modal-title">Agregar nueva actividad</h4>
                                          </div>
                                          <div class="modal-body">
                                                <div class="row">
                                                      <div class="col-md-12 col-s-12">
                                                            <div class="form-group">
                                                                  <label for="Nombre">Nombre</label>
                                                                  <input class="form-control" id="nombre" name="nombre" type="text" required value="">
                                                            </div>
                                                      </div>
                                                      <div class="col-md-6 col-s-12">
                                                      <div class="bootstrap-timepicker">
                                                      <div class="form-group">
                                                        <label>Hora</label>
                                                        <div class="input-group">
                                                          <input type="text" id="hora" name="hora" class="form-control timepicker">
                                                          <div class="input-group-addon">
                                                            <i class="fa fa-clock-o"></i>
                                                          </div>
                                                        </div>
                                                        <!-- /.input group -->
                                                      </div>
                                                      <!-- /.form group -->
                                                    </div>
                                                      </div>
                                                      <div class="col-md-6 col-s-12">
                                                            <div class="form-group">
                                                                  <label>Fecha</label>
                                                                  <div class="input-group date">
                                                                        <div class="input-group-addon">
                                                                              <i class="fa fa-calendar"></i>
                                                                        </div>
                                                                        <input type="text" name="fecha" class="form-control pull-right" required id="datepicker">
                                                                  </div>
                                                                  <!-- /.input group -->
                                                            </div>
                                                      </div>
                                                      <div class="col-md-6 col-s-12">
                                                            <div class="form-group">
                                                                  <label>Duracion (dias)</label>
                                                                  <div class="input-group">
                                                                        <div class="input-group-addon">
                                                                              <i class="fa fa-calendar"></i>
                                                                        </div>
                                                                        <input type="text" name="rango_fecha" class="form-control pull-right" required id="reservation">
                                                                  </div>
                                                            </div>
                                                      </div>
                                                      <div class="col-md-6 col-s-12">
                                                            <div class="form-group">
                                                                  <label>Lugar</label>
                                                                  <input class="form-control" id="lugar" name="lugar" type="text" required value="">
                                                            </div>
                                                      </div>
                                                      <div class="col-md-12 ">
                                                            <div class="form-group">
                                                                  <label>Descripcion</label>
                                                                  <textarea class="form-control" id="justificacion" name="justificacion" rows="3" required placeholder=""></textarea>
                                                            </div>
                                                      </div>
                                                      <div class="col-md-12 ">
                                                            <div class="form-group">
                                                                  <label>Publico objetivo</label>
                                                                  <textarea class="form-control" id="publicoObjetivo" name="publicoObjetivo" rows="3" required placeholder=""></textarea>
                                                            </div>
                                                      </div>
                                                      <div class="col-md-12 ">
                                                            <div class="form-group">
                                                            <input id="idEscenario_HIDDEN" class="form-control" name="idEscenario" type="hidden" value="">
                                                                  <button type="submit" class="btn btn-warning btn-block">Crear</button>
                                                            </div>
                                                      </div>
                                                </div>
                                          </div>
                                    </div>
                                    <!-- /.modal-content -->
                              </div>
                              <!-- /.modal-dialog -->
                              </form>
                        </div>
                        <!-- /.modal -->










                  </section>
                  <!-- /.content -->
            </div>