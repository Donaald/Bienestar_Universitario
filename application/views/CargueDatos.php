<div class="content-wrapper">
      <section class="content">
          <section class="content-header">
              <h1>
                Cargar datos del sistema.
                <small><a href="" data-toggle="modal" data-target="#modal-default">Mas informacion</a></small>
              </h1>
            </section>
            <br>
            <?php
         if(!empty($error))
         {
          echo '<div class="alert alert-danger alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h4><i class="icon fa fa-ban"></i>Error!</h4>'.$error.'</div>';  
         }
         ?>
        <?php
         if(!empty($correcto))
         {
          echo '<div class="alert alert-warning alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h4><i class="icon fa fa-ban"></i>Felicidades!</h4>'.$correcto.'</div>';  
         }
         ?>

    <?php echo form_open_multipart('index.php/CargueDatos_C/validar'); ?>
        <div class="input-group margin">
          <input type="file" name="datos" class="form-control">
          <span class="input-group-btn">
            <button type="submit" class="btn btn-warning btn-flat">Subir</button>
          </span>
        </div>
        <!-- /input-group -->
    </form>


        <div class="modal fade" id="modal-default">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header text-center">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title">Instrucciones</h4>
                </div>
                <div class="modal-body">
                    <ul>
                        <li>El archivo debe ser CSV</li>
                        <li>El delimitador del archivo debe ser (,)</li>
                        <li>Las columnas seran las siguientes:
                          <ul>
                            <li>Numero de identificacion</li>
                            <li>Tipo de documento</li>
                            <li>Primer nombre</li>
                            <li>Segundo nombre</li>
                            <li>Primer apellido</li>
                            <li>Segundo apellido</li>
                            <li>Correo Electronico</li>
                            <li>Genero</li>
                            <li>ID universitario</li>                           
                            <li>Rol</li>
                            <li>Facultad(s)</li>
                          </ul>
                        </li>
                        <li>Si un usuario esta matriculado en mas de una facultad el delimitador de las facultades sera (;)</li>
                        <li>Antes de subir dicho archivo debe haber creado manualmente todas las facultades</li>
                        <li>Los roles seran los siguientes
                            <ul>
                                <li>ESTUDIANTE</li>
                                <li>DOCENTE</li>
                                <li>ADMINISTRADOR UNO</li>
                                <li>ADMINISTRADOR DOS</li>
                              </ul>
                        </li>
                        <li>El rol administrador uno debera ser asignado al director(a) de cultura</li>
                        <li>El rol administrador dos debera ser asignado a el secretario o la secretaria del director(a) de cultura</li>
                      </ul>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>

                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /.modal -->




      </section>
    </div>