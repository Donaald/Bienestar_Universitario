<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="stylesheet" href="<?php echo base_url(); ?>Vistas/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>Vistas/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>Vistas/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>Vistas/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>Vistas/dist/css/login.css"/>
  <link rel="stylesheet" href="<?php echo base_url(); ?>Vistas/dist/css/AdminLTE.min.css">


</head>
<body>
  <div id="espacio">
    <br>
    <br>
    <br>

  </div>
  <div class="container">

    <div class="row">
    <div class="col-xs-12 col-md-3"></div>
      <div class="col-xs-12 col-md-6">
     <?php
if (!empty(validation_errors()) || !empty($informacion)) {  
    echo '<div class="callout callout-danger"><h4>Error!</h4><p>' . validation_errors() . $informacion .'</p></div>';
}
?>

      </div>
      <div class="col-xs-12 col-md-3"></div>
    </div>
    <br>
    <div class="col-md-4"></div>
    <div class="col-md-4 text-center">
      <div class="login-box-body">
        <div class="box-body">
          <div>
            <img id="logo" src="<?php echo base_url(); ?>Vistas/dist/img/logo.png" alt="logo">
            <br>
            <br>
          </div>
          <p>Ingresa tus datos para iniciar</p>
          <form action=<?php echo '"'. base_url().'index.php/Login/validarUsuario"'?> method="post">
         
                  <?php 
                   if(!empty(form_error('id')))
                   {   
                       echo '<div class="form-group has-error">';
                       echo' <div class="form-group has-feedback">            
                                <span class="glyphicon glyphicon-user form-control-feedback"></span>';
                       $Id = array( 
                         'name' => 'id',  
                         'class' => 'form-control',                                         
                         'type' => 'text',
                         'placeholder' => 'ID Uiversitario'
                        
                     );
                     echo form_input($Id);
                       echo '</div>';  
                       echo '</div>';                           
                   }else
                   {
                       echo '<div class="form-group">';
                       echo '<div class="form-group has-feedback">            
                       <span class="glyphicon glyphicon-user form-control-feedback"></span>';
                       $Id = array( 
                         'name' => 'id',  
                         'class' => 'form-control',                                         
                         'type' => 'text',
                         'placeholder' => 'ID Uiversitario',
                         'value' => set_value("id")
                     );
                     echo form_input($Id);
                       echo '</div>';
                       echo '</div>';
                   }
                   if(!empty(form_error('contraseña')))
                   {   
                       echo '<div class="form-group has-error">';
                       echo' <div class="form-group has-feedback">            
                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>';
                       $Contraseña = array( 
                         'name' => 'contraseña',  
                         'class' => 'form-control',                                         
                         'type' => 'password',
                         'placeholder' => 'Número de documento'
                        
                     );
                     echo form_input($Contraseña);
                       echo '</div>';    
                       echo '</div>';                        
                   }else
                   {
                       echo '<div class="form-group">';
                       echo '<div class="form-group has-feedback">            
                       <span class="glyphicon glyphicon-lock form-control-feedback"></span>';
                       $Contraseña = array( 
                         'name' => 'contraseña',  
                         'class' => 'form-control',                                         
                         'type' => 'password',
                         'placeholder' => 'Número de documento',
                         'value' => set_value("contraseña")
                     );
                     echo form_input($Contraseña);
                       echo '</div>';
                       echo '</div>';
                   }
                     if(!empty(form_error('periodo')))
                     {  
                      echo '<div class="form-group has-error">';
                         $Periodo = array( 
                           'name' => 'periodo',  
                           'class' => 'form-control'                                                              
                       );  
                       $opcionesPeriodo[''] = 'Selecione el periodo';                                     
                       foreach($periodo as $value)
                       {                               
                        $opcionesPeriodo[$value->idperiodo] = $value->idperiodo;
                       }                
                         echo form_dropdown($Periodo, $opcionesPeriodo);   
                         echo '<br>';                                           
                     }else
                     {            
                      echo '<div class="form-group">';            
                         $Periodo = array( 
                           'name' => 'periodo',  
                           'class' => 'form-control',
                           'value' => set_value("periodo")
                       );     
                       $opcionesPeriodo[''] = 'Selecione el periodo';                                   
                       foreach($periodo as $value)
                       {                               
                        $opcionesPeriodo[$value->idperiodo] = $value->idperiodo;
                       }
                       echo form_dropdown($Periodo, $opcionesPeriodo);
                       echo '</div>';
                     
                     }                    
                     if(!empty(form_error('seccional')))
                     {                
                      echo '<div class="form-group has-error">';          
                         $Seccional = array( 
                           'name' => 'seccional',  
                           'class' => 'form-control'                                                              
                       );  
                       $opcionesSeccional[''] = 'Selecione la seccional';                                     
                       foreach($seccional as $value)
                       {                               
                        $opcionesSeccional[$value->idseccional] = $value->idseccional;
                       }                
                         echo form_dropdown($Seccional, $opcionesSeccional);      
                         echo '</div>';                               
                     }else
                     {           
                      echo '<div class="form-group">';             
                         $Seccional = array( 
                           'name' => 'seccional',  
                           'class' => 'form-control',
                           'value' => set_value("seccional")
                       );     
                        $opcionesSeccional[''] = 'Selecione la seccional';                                     
                       foreach($seccional as $value)
                       {                               
                        $opcionesSeccional[$value->idseccional] = $value->idseccional;
                       }                
                         echo form_dropdown($Seccional, $opcionesSeccional);  
                         echo '</div>';                     
                     }
                     ?> 
          <input type="submit" class="btn btn-danger col-md-12 col-xs-12" value="Iniciar sesión">
          <br>
          <br>
          <br>
          <a href="#">¿Has olvidado tu contraseña?</a>
          </form>
        </div>
      </div>
      <div class="col-md-4"></div>
    </div>
</body>

</html>