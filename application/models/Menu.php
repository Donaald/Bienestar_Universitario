<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function obtenerMenu($rol)
    {

        try {
            $sql = "SELECT idItem,icono,nombre,direccion,item FROM item INNER JOIN menu ON Menu_idMenu = idMenu WHERE rol ='" . $rol . "' ORDER BY item";
            $consulta = $this->db->query($sql);
            return $consulta->result();
        } catch (exception $e) {
            return false;
        }
    }

}
