<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Imagen extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('upload');
        $config['upload_path'] = "./archivos/";
        $config['allowed_types'] = 'jpeg|jpg|png';
        $config['max_size'] = '500000';
        $this->upload->initialize($config);
    }

    public function subirImagen($imagen)
    {
        try
        {
            if (!$this->upload->do_upload($imagen)) {
                return "subio la imagen pero con errores";
            } else {
                return "subio la imagen y esta correcta";
            }
        } catch (Excepcion $e) {
            return false;
        }
    }

    public function getNombre()
    {
        try
        {
            return $this->upload->data("file_name");
        } catch (Excepcion $e) {
            return false;
        }
    }

}
