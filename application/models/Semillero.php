<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Semillero extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function crear($datos)
    {
        $valores = array
            (
            'nombre' => $datos['nombre'],
            'contenido_tematico' => $datos['Contenido_Tematico'],
            'imagen' => $datos['imagen'],
            'docente' => $datos['docente'],
            'periodo_idperiodo' => $datos['periodo_idperiodo'],
            'seccional_idseccional' => $datos['seccional_idseccional'],
            'estado_idestado' => 'ACTIVO',
        );
        try
        {
            $this->db->insert("semillero", $valores);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    public function modificar($datos)
    {
        try
        {
            $this->db->where('idsemillero', $datos['idsemillero']);
            $this->db->update('semillero', $datos);
        } catch (Exception $e) {
            return false;
        }
    }
    public function registroUsuarioSemillero($datos)
    {
        try
        {
            $consulta = $this->db->query('CALL agregar_usuario_semillero(' . $datos['id'] . ',' . $datos['idCursoLibre'] . ',' . $datos['documento'] . ');');
            return $consulta->result();

        } catch (Exception $e) {
            return false;
        }
    }
    public function eliminar($idSemillero)
    {
        try
        {
            $this->db->where('idsemillero', $idSemillero);
            $this->db->set('estado_idestado','INACTIVO');
            $this->db->update('semillero');            
        } catch (Exception $e) {
            return false;
        }
    }
    public function listar($periodo,$seccional)
    {
        try
        {
            $consulta = $this->db->query('CALL listar_semillero("'.$seccional.'","'.$periodo.'");');
            return $consulta->result();
        } catch (Exception $e) {
            return false;
        }
    }
    public function listar_id($idSemillero)
    {
        try
        {
            $consulta = $this->db->query('CALL listar_semillero_id('.$idSemillero.');');
            return $consulta->result();
        } catch (Exception $e) {
            return false;
        }
    }
    public function inscritos($idSemillero)
    {
        try
        {
            $consulta = $this->db->query('CALL inscritos_semillero('.$idSemillero.');');
            return $consulta->result();
        } catch (Exception $e) {
            return false;
        }
    }
    public function listar_semillero_docente_completo($idDocente)
    {
        try
        {
            $consulta = $this->db->query('CALL listar_semillero_docente(' . $idDocente . ');');
            return $consulta->result();

        } catch (Exception $e) {
            return false;
        }
    }

}
