<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Facultad extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function crear()
    {

    }

    public function listar()
    {
        try
        {
            $consulta = $this->db->get('facultad');
            return $consulta->result();
        } catch (exception $e) {
            return false;
        }        
    }
    public function listarId($idFacultad)
    {
        
    }

    public function eliminar($idFacultad)
    {
        
    }

}
