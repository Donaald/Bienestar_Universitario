<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Usuario extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function validacionUsuario($usuario, $pw,$seccional)
    {
        try {
            $query = $this->db->query("SELECT contraseña,rol,Persona_numero_identificacion,idUsuario,seccional_idseccional  FROM usuario WHERE idUsuario =" . $usuario);
            if ($query->num_rows() == 1) {
                $datos = $query->first_row();
                if ($datos->contraseña == $pw && $datos->seccional_idseccional == $seccional) {
                    return array('numeroIdentificacion' => $datos->Persona_numero_identificacion, 'rol' => $datos->rol, 'idUniversitario' => $datos->idUsuario);
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    public function obtener_docente()
    {
        try {
            $consulta = $this->db->query('CALL listar_docente()');
            return $consulta->result();
        } catch (exception $e) {
            return false;
        }

    }

    public function obtener_docente_id($id_universitario)
    {
        try {
            $consulta = $this->db->query('CALL listar_docente_id(' . $id_universitario . ')');
            return $consulta->row_array();
        } catch (exception $e) {
            return false;
        }
    }

    public function obtener_usuario_id_u($id_universitario)
    {
        try {
            $consulta = $this->db->query('CALL listar_usuario_id(' . $id_universitario . ')');
            return $consulta->result();
        } catch (exception $e) {
            return false;
        }
    }

    public function obtener_usuario()
    {
        try
        {
            $consulta = $this->db->get('usuario');
            return $consulta->result();
        } catch (exception $e) {
            return false;
        }
    }

    public function nuevoUsuario($datos)
    {
        try
        {
            for ($i = 1; $i <= count($datos); $i++) {
                $valores = array(
                    'idUsuario' => $datos[$i]['ID_universitario'],
                    'contraseña' => $datos[$i]['Numero_identificacion'],
                    'rol' => $datos[$i]['Rol'],
                    'persona_numero_identificacion' => $datos[$i]['Numero_identificacion'],
                );
                $this->db->insert("usuario", $valores);
                for ($j = 0; $j < $datos[$i]['Facultad']; $j++) {
                    $valores = array(
                        'usuario_idUsuario' => $datos[$i]['ID_universitario'],
                        'facultad_nombre' => $datos[$i][$j]['Facultad'],
                    );
                    $this->db->insert("usuario_facultad", $valores);
                }
            }
        } catch (Exception $e) {
            return false;
        }
    }

    public function nuevaPersona($datos)
    {
        try
        {
            for ($i = 1; $i <= count($datos); $i++) {
                $valores = array(
                    'numero_identificacion' => $datos[$i]['Numero_identificacion'],
                    'tipo_documento' => $datos[$i]['Tipo_documento'],
                    'nombre1' => $datos[$i]['Primer_nombre'],
                    'nombre2' => $datos[$i]['Segundo_nombre'],
                    'apellido1' => $datos[$i]['Primer_apellido'],
                    'apellido2' => $datos[$i]['Segundo_apellido'],
                    'correo_electronico' => $datos[$i]['Correo_Electronico'],
                    'sexo' => $datos[$i]['Genero'],
                );
                $this->db->insert("persona", $valores);
            }

        } catch (Exception $e) {
            return false;
        }

    }

    public function vaciarPersona()
    {
        try
        {
            $sql = "DELETE FROM persona WHERE numero_identificacion != '0'";
            $consulta = $this->db->query($sql);
        } catch (Exception $e) {
            return false;
        }
    }

    public function vaciarUsuario()
    {
        try
        {
            $sql1 = "DELETE FROM usuario_facultad WHERE usuario_idUsuario >= 0";
            $consulta1 = $this->db->query($sql1);
            $sql = "DELETE FROM usuario WHERE idUsuario >= 0";
            $consulta = $this->db->query($sql);           
        } catch (Exception $e) {
            return false;
        }
    } 

}
