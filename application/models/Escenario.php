<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Escenario extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function crear($datos)
    {
        try
        {
            $this->db->insert('escenario', $datos);
        } catch (exception $e) {
            return false;
        }
    }

    public function listar($periodo, $seccional)
    {
        try
        {
            $consulta = $this->db->get_where('escenario', array('periodo_idperiodo' => $periodo, 'seccional_idseccional' => $seccional, 'estado_idestado' => 'ACTIVO'));
            return $consulta->result();
        } catch (exception $e) {
            return false;
        }
    }
    public function listar_id($idEscenario)
    {
        try
        {
            $consulta = $this->db->get_where('escenario', array('idescenario' => $idEscenario));
            return $consulta->result();
        } catch (exception $e) {
            return false;
        }
    }

    public function agregarActividad($datos)
    {
        try
        {
            $this->db->insert('actividad', $datos);
        } catch (exception $e) {
            return false;
        }
    }

    public function listarActividadesEscenario($idEscenario)
    {
        try
        {
            $consulta = $this->db->get_where('actividad', array('escenario_idescenario' => $idEscenario, 'estado_idestado' => 'ACTIVO'));
            return $consulta->result();
        } catch (exception $e) {
            return false;
        }
    }
    public function listarActividadesFecha($seccional, $periodo)
    {
        try
        {
            $consulta = $this->db->query('SELECT a.idactividad,a.nombre AS nombreActividad,a.fecha,a.duracion,a.lugar,a.descripcion,a.publicoObjetivo,e.nombre AS nombreEscenario,a.hora FROM actividad AS a INNER JOIN escenario AS e ON e.idescenario = a.escenario_idescenario WHERE a.fecha >= CURDATE() AND a.periodo_idperiodo = "' . $periodo . '" AND a.seccional_idseccional = "' . $seccional . '" AND a.estado_idestado = "ACTIVO"  ORDER BY a.fecha,a.hora ASC;');
            return $consulta->result();
        } catch (exception $e) {
            return false;
        }
    }
    public function listarActividadId($idActividad)
    {
        try
        {
            $consulta = $this->db->get_where('actividad', array('idactividad' => $idActividad));
            return $consulta->result();
        } catch (exception $e) {
            return false;
        }
    }
    public function eliminar($idEscenario)
    {
        try
        {
            $this->db->where('idescenario', $idEscenario);
            $this->db->set('estado_idestado', 'INACTIVO');
            $this->db->update('escenario');
        } catch (Exception $e) {
            return false;
        }
    }
    public function eliminarActividad($idActividad)
    {
        try
        {
            $this->db->where('idactividad', $idActividad);
            $this->db->set('estado_idestado', 'INACTIVO');
            $this->db->update('actividad');
        } catch (Exception $e) {
            return false;
        }
    }

}
