<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Seccional extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function listar()
    {
        try
        {
            $consulta = $this->db->get('seccional');
            return $consulta->result();
        } catch (exception $e) {
            return false;
        }
    }
   

}