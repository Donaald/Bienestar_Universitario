<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Periodo extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function listar()
    {
        try
        {
            $consulta = $this->db->get('periodo');
            return $consulta->result();
        } catch (exception $e) {
            return false;
        }
    }

    public function listarUltimo()
    {
        try
        {
            $consulta = $this->db->query('SELECT idperiodo FROM periodo ORDER BY fecha DESC LIMIT 1;');
            return $consulta->result();
        } catch (exception $e) {
            return false;
        }
    }

}
