<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CursoLibre extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function nuevoCursoLibre($nombre, $cupoMaximo, $contenidoTematico, $duracion, $direccionImagen, $docente, $periodo, $seccional)
    {
        $valores = array
            (
            'nombre' => $nombre,
            'cupo_maximo' => $cupoMaximo,
            'contenido_tematico' => $contenidoTematico,
            'duracion' => $duracion,
            'imagen' => $direccionImagen,
            'docente' => $docente,
            'periodo_idperiodo' => $periodo,
            'seccional_idseccional' => $seccional,
            'estado_idestado' => 'ACTIVO',
        );
        try
        {
            $this->db->insert("curso_libre", $valores);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function modificar($datos)
    {
        try
        {
            $this->db->where('idCurso_libre', $datos['idCurso_libre']);
            $this->db->update('curso_libre', $datos);
        } catch (Exception $e) {
            return false;
        }
    }

    public function eliminar($idCursoLibre)
    {
        try
        {
            $this->db->set('estado_idestado', 'INACTIVO');
            $this->db->where('idCurso_libre', $idCursoLibre);
            $this->db->update('curso_libre');
        } catch (Exception $e) {
            return false;
        }
    }

    public function listar($periodo,$seccional)
    {
        try
        {
            $consulta = $this->db->query('CALL listar_curso_libre("'.$seccional.'","'.$periodo.'");');
            return $consulta->result();
        } catch (Exception $e) {
            return false;
        }
    }
    public function listar_id($idCursoLibre)
    {
        try
        {
            $consulta = $this->db->query('CALL listar_curso_libre_id(' . $idCursoLibre . ');');
            return $consulta->result();
        } catch (Exception $e) {
            return false;
        }
    }

    public function listar_cursoLibre_docente($idDocente)
    {
        try
        {
            $consulta = $this->db->query('CALL listar_cursoLibre_docente_horario(' . $idDocente . ');');
            return $consulta->result();
        } catch (Exception $e) {
            return false;
        }
    }

    public function registroUsuarioCursoLibre($datos)
    {
        try
        {
            $consulta = $this->db->query('CALL agregar_usuario_cursoLibre(' . $datos['id'] . ',' . $datos['idCursoLibre'] . ',' . $datos['documento'] . ');');
            return $consulta->result();

        } catch (Exception $e) {
            return false;
        }
    }
    public function agregarHorarioCursoLibre($horario, $idCursoLibre)
    {
        try
        {
            for ($i = 0; $i < count($horario); $i++) {
                $cadena = explode('_', $horario[$i]);
                $valores = array
                    (
                    'dia' => $cadena[0],
                    'hora' => $cadena[1] * 10000,
                    'curso_libre_idCurso_libre' => $idCursoLibre,
                );
                $this->db->insert("horario", $valores);
            }

        } catch (Exception $e) {
            return false;
        }
    }

    public function inscritosCursoLibre($idCursoLibre)
    {
        try
        {
            $consulta = $this->db->query('CALL inscritos_cursoLibre(' . $idCursoLibre . ');');
            return $consulta->result();

        } catch (Exception $e) {
            return false;
        }
    }

    public function listar_cursoLibre_docente_completo($idDocente)
    {
        try
        {
            $consulta = $this->db->query('CALL listar_cursoLibre_docente(' . $idDocente . ');');
            return $consulta->result();

        } catch (Exception $e) {
            return false;
        }
    }

    public function tomarAsistenciaCursoLibre($idCursoLibre, $idInscritos, $fecha)
    {
        try
        {
            for ($i = 0; $i < count($idInscritos); $i++) {
                $valores = array
                    (
                    'usuario_idUsuario' => $idInscritos[$i],
                    'curso_libre_idCurso_libre' => $idCursoLibre,
                    'fecha' => date("Y-m-d", strtotime($fecha)),
                );
                $this->db->insert("asistencia_cursoLibre", $valores);
            }

        } catch (Exception $e) {
            return false;
        }
    }

    public function eliminarParticipanteCursolibre($idUniversitario, $idCursoLibre)
    {
        try
        {
            $this->db->where('usuario_idUsuario', $idUniversitario);
            $this->db->where('curso_libre_idCurso_libre', $idCursoLibre);
            $this->db->delete('usuario_curso_libre');
        } catch (Exception $e) {
            return false;
        }
    }

}
