function base_url() {
    var base = window.location.href.split('/');
    return base[0] + '//' + base[2] + '/' + base[3];
}

//$(document).ready(function () {

$(document).ready(function () {
    $.ajax({
        url: base_url() + '/index.php/Seccional_C/listar',
        type: 'POST',
        success: function (resultado) {
            var datosSeccional = JSON.parse(resultado);
            var cadena = '<select id="selectSeccionales" class="selectpicker" data-style="btn btn-warning" title="Selecionar Seccional" data-size="7">';

            for (let index = 0; index < datosSeccional.length; index++) {
                cadena += '<option value="' + datosSeccional[index]['idseccional'] + '">' + datosSeccional[index]['idseccional'] + '</option>';
            }
            cadena += '</select>';
            $('#secionales').html(cadena);

        }
    });

});

$(document).on('change', '#selectSeccionales', function () {
    $.ajax({
        url: base_url() + '/index.php/Login/datosSessionUsuario/' + $('#selectSeccionales').val(),
        type: 'POST',
        success: function (resultado) {
            if (resultado != '-1') {
                swal({
                    title: 'Correcto!',
                    html: 'Cambio a seccional <v>' + resultado + '</b>',
                    confirmButtonText: 'OK!',
                    confirmButtonClass: 'btn btn-warning',
                    buttonsStyling: false
                }).catch(swal.noop)
            } else {
                swal({
                    title: 'Error',
                    confirmButtonText: 'OK!',
                    confirmButtonClass: 'btn btn-warning',
                    buttonsStyling: false
                }).catch(swal.noop)
            }

        }
    });
});

function drawCursosLibres() {

    $.ajax({
        url: base_url() + '/index.php/CursoLibre_C/inscribirse',
        success: function (result) {
            var datosJSON = JSON.parse(result);
            var card = '';
            for (index = 0; index < datosJSON.length; index++) {
                card += '<div class="col-md-4">';
                card += '<div class="card card-blog">';
                card += '<div class="card-image">';
                card += '<a href="#">';
                card += '<img id="perfil" class="img img-raised" src="archivos/' + datosJSON[index]['imagen'] + '"/>';
                card += '</a>';
                card += '</div>';
                card += '<div class="card-content" align="center">';
                card += '<h5 class="category text-default">' + datosJSON[index]['nombre_docente'] + ' ' + datosJSON[index]['apellido_docente'] + '</h5>';
                card += '<h4 class="card-title" style="height: 50px;">';
                card += datosJSON[index]['nombre'];
                card += '</h4>';
                card += '<ul class="list-group" align="left">';
                card += '<li class="list-group-item">';
                card += '<b>Inscritos:</b>';
                card += '<p class="pull-right">' + datosJSON[index]['inscritos'] + '</p>';
                card += '</li>';
                card += '<li class="list-group-item">';
                card += '<b>Cupos totales:</b>';
                card += '<p class="pull-right">' + datosJSON[index]['cupo_maximo'] + '</p>';
                card += '</li>';
                card += '<li class="list-group-item">';
                card += '<b>Cantidad de horas:</b>';
                card += '<p class="pull-right">' + datosJSON[index]['duracion'] + '</p>';
                card += '</li>';
                card += '</ul>';
                card += '<div class="row">';
                card += '<div class="col-lg-6 col-md-12">';
                card += '<b class="btn btn-default btn-block" data-toggle="modal" data-target="#horario">Horario</b>';
                card += '</div>';
                card += '<div class="col-lg-6 col-md-12">';
                card += '<a href="' + base_url() + '/index.php/CursoLibre_C/listarId/' + datosJSON[index]['idCurso_libre'] + '" class="btn btn-default btn-block descripcion">Descripcion</a>';
                card += '</div>';
                card += '</div>';
                card += '<a href="' + base_url() + '/index.php/CursoLibre_C/inscripcion/' + datosJSON[index]['idCurso_libre'] + '" class="btn btn-warning btn-block inscripcion">';
                card += '<b>Incribirse</b>';
                card += '</a>';
                card += '</div>';
                card += '</div>';
                card += '</div>';
            }

            $("#cursosLibres_contenido").html(card);
        }
    });

}

function drawSemilleros() {
    $.ajax({
        url: base_url() + '/index.php/Semilleros_C/inscribirse',
        success: function (result) {
            var datosJSON = JSON.parse(result);
            var card = '';
            for (index = 0; index < datosJSON.length; index++) {
                card += '<div class="col-md-4">'
                card += '<div class="card card-blog">'
                card += '<div class="card-image">'
                card += '<a href="#pablo">'
                card += '<img id="perfil" class="img img-raised" src="archivos/' + datosJSON[index]['imagen'] + '" />'
                card += '</a>'
                card += '</div>'
                card += '<div class="card-content" align="center">'
                card += '<h5 class="category text-default">Donald Armando Torres Arteaga</h5>'
                card += '<h4 class="card-title" style="height: 50px;">'
                card += datosJSON[index]['nombre'];
                card += '</h4>'
                card += '<ul class="list-group" align="left">'
                card += '<li class="list-group-item">'
                card += '<b>Inscritos:</b>'
                card += '<p class="pull-right">' + datosJSON[index]['inscritos'] + '</p>'
                card += '</li>'
                card += '</ul>'
                card += '<div class="row">'
                card += '<div class="col-xs-12">'
                card += '<b class="btn btn-default btn-block descripcionSemillero" href="' + base_url() + '/index.php/Semilleros_C/listarId/' + datosJSON[index]['idsemillero'] + '">Descripcion</b>'
                card += '</div>'
                card += '</div>'
                card += '<a href="' + base_url() + '/index.php/Semilleros_C/inscripcion/' + datosJSON[index]['idsemillero'] + '" class="btn btn-warning btn-block inscripcionSemillero">'
                card += '<b>Incribirse</b>'
                card += '</a>'
                card += '</div>'
                card += '</div>'
                card += '</div>'
            }

            $("#semilleros_contenido").html(card);
        }
    });

}


$(document).on('click', '#cursosLibres', function () {
    if ($('#selectSeccionales').val() == '') {
        swal({
            title: 'Error',
            text: 'Selecione una sucursal',
            confirmButtonText: 'OK!',
            confirmButtonClass: 'btn btn-warning',
            buttonsStyling: false,
            type: 'error'
        }).catch(swal.noop)
    } else {
        drawCursosLibres();
    }

});
$(document).on('click', '#semilleros', function () {
    if ($('#selectSeccionales').val() == '') {
        swal({
            title: 'Error',
            text: 'Selecione una sucursal',
            confirmButtonText: 'OK!',
            confirmButtonClass: 'btn btn-warning',
            buttonsStyling: false,
            type: 'error'
        }).catch(swal.noop)
    } else {
        drawSemilleros();
    }
});
$(document).on('click', '.inscripcion', function () {

    var url_inscripcion = $(this).attr('href');
    swal({
        title: 'Ingrese sus datos',
        html: '<br><div class="form-group has-warning">' +
            '<label class="control-label">Id Universitario</label>' +
            '<input id="idUniversitario" name="id" type="password" class="form-control"/>' +
            '</div>' +
            '<br>' +
            '<div class="form-group has-warning">' +
            '<label class="control-label">Documento Identidad</label>' +
            '<input id="documentoIdentidad" name="documento" type="password" class="form-control"/>' +
            '</div>',
        confirmButtonText: 'Siguiente!',
        confirmButtonClass: 'btn btn-warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        cancelButtonClass: 'btn btn-warning btn-simple',
        buttonsStyling: false
    }).then(function (result) {
        var parametros = {
            "id": $('#idUniversitario').val(),
            "documento": $('#documentoIdentidad').val()
        };
        $.ajax({
            url: url_inscripcion,
            type: 'POST',
            data: parametros,
            success: function (resultado) {
                if (resultado !== 'Cupo maximo completo' && resultado !== 'Ya pertenece al curso libre' && resultado !== 'Datos incorrectos' && resultado[0] !== '-') {
                    swal({
                        title: 'Todo Hecho!',
                        html: 'Su registo al curso libre: <b>' + resultado + '</b> se completo con exito',
                        confirmButtonText: 'Correcto!',
                        confirmButtonClass: 'btn btn-warning btn-simple',
                        buttonsStyling: false,
                        type: 'success'
                    }).catch(swal.noop)
                    drawCursosLibres();
                } else {
                    resultado[0] = '';
                    swal({
                        title: 'Error',
                        html: resultado,
                        confirmButtonText: 'Salir!',
                        confirmButtonClass: 'btn btn-danger btn-simple',
                        buttonsStyling: false,
                        type: 'error'
                    }).catch(swal.noop)
                }
            }
        });
    }).catch(swal.noop)
    return false;

});
$(document).on('click', '.inscripcionSemillero', function () {

    var url_inscripcion = $(this).attr('href');
    swal({
        title: 'Ingrese sus datos',
        html: '<br><div class="form-group has-warning">' +
            '<label class="control-label">Id Universitario</label>' +
            '<input id="idUniversitario" name="id" type="password" class="form-control"/>' +
            '</div>' +
            '<br>' +
            '<div class="form-group has-warning">' +
            '<label class="control-label">Documento Identidad</label>' +
            '<input id="documentoIdentidad" name="documento" type="password" class="form-control"/>' +
            '</div>',
        confirmButtonText: 'Siguiente!',
        confirmButtonClass: 'btn btn-warning',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        cancelButtonClass: 'btn btn-warning btn-simple',
        buttonsStyling: false
    }).then(function (result) {
        var parametros = {
            "id": $('#idUniversitario').val(),
            "documento": $('#documentoIdentidad').val()
        };
        $.ajax({
            url: url_inscripcion,
            type: 'POST',
            data: parametros,
            success: function (resultado) {
                if (resultado !== 'Ya pertenece al Semillero' && resultado !== 'Datos incorrectos' && resultado[0] !== '-') {
                    swal({
                        title: 'Todo Hecho!',
                        html: 'Su registo al curso libre: <b>' + resultado + '</b> se completo con exito',
                        confirmButtonText: 'Correcto!',
                        confirmButtonClass: 'btn btn-warning btn-simple',
                        buttonsStyling: false,
                        type: 'success'
                    }).catch(swal.noop)
                    drawSemilleros();
                } else {
                    resultado[0] = '';
                    swal({
                        title: 'Error',
                        html: resultado,
                        confirmButtonText: 'Salir!',
                        confirmButtonClass: 'btn btn-danger btn-simple',
                        buttonsStyling: false,
                        type: 'error'
                    }).catch(swal.noop)
                }
            }
        });
    }).catch(swal.noop)
    return false;

});
$(document).on('click', '.descripcion', function () {
    var url_idC = $(this).attr('href');
    $.ajax({
        url: url_idC,
        type: 'POST',
        success: function (resultado) {
            var valorCursoLibre = JSON.parse(resultado);
            $("#nombreCursoLibre").html('<b>' + valorCursoLibre[0]['nombre'] + '</b>');
            $("#contenidoDescripcion").html('<h5>' + valorCursoLibre[0]['contenido_tematico'] + '</h5>');
            $("#contenidoTematico").modal('show');

        }
    });
    return false;
});
$(document).on('click', '.descripcionSemillero', function () {
    var url_idS = $(this).attr('href');
    $.ajax({
        url: url_idS,
        type: 'POST',
        success: function (resultado) {
            var valorSemillero = JSON.parse(resultado);
            $("#nombreSemillero").html('<b>' + valorSemillero[0]['nombre'] + '</b>');
            $("#contenidoDescripcionSemillero").html('<h5>' + valorSemillero[0]['contenido_tematico'] + '</h5>');
            $("#contenidoTematicoSemillero").modal('show');
        }
    });
    return false;
});



$(document).on('click', '#escenarios', function () {

    if ($('#selectSeccionales').val() == '') {
        swal({
            title: 'Error',
            text: 'Selecione una sucursal',
            confirmButtonText: 'OK!',
            confirmButtonClass: 'btn btn-warning',
            buttonsStyling: false,
            type: 'error'
        }).catch(swal.noop)
    } else {
        $.ajax({
            url: base_url() + '/index.php/Escenario_C/listarActividadesFecha',
            success: function (resultado) {
                var valorEscenario = JSON.parse(resultado);
                var cadena = '<div class="card card-plain">';
                cadena += '<div class="card-content">';
                cadena += '<ul class="timeline">';

                for (var i = 0; i < valorEscenario.length; i++) {
                    if (i % 2 == 0) {
                        cadena += '<li class="timeline-inverted">';
                        cadena += '<div class="timeline-badge warning">';
                        cadena += '<i class="material-icons">date_range</i>';
                        cadena += '</div>';
                        cadena += '<div class="timeline-panel">';
                        cadena += '<div class="timeline-heading">';
                        cadena += '<span class="label label-warning">' + valorEscenario[i]['fecha'] + '</span>';
                        cadena += '</div>';
                        cadena += '<div class="timeline-body">';
                        cadena += '<h3 class="timeline-header">' + valorEscenario[i]['nombreActividad'] + '</h3>';
                        cadena += '<br>';
                        cadena += '<div class="timeline-body">';
                        cadena += '<strong><i class="fa fa-info-circlemargin-r-5"></i> Escenario</strong><br>';
                        cadena += '<p class="text-muted">' + valorEscenario[i]['nombreEscenario'] + '</p>';
                        cadena += '<hr>';
                        cadena += '<strong><i class="fa fa-map-marker margin-r-5"></i> Ubicacion</strong>';
                        cadena += '<p class="text-muted">' + valorEscenario[i]['lugar'] + '</p>';
                        cadena += '<hr>';
                        cadena += '<strong><i class="fa fa-file-text-o margin-r-5"></i> Contenido Tematico';
                        cadena += '</strong>';
                        cadena += '<p class="text-muted">' + valorEscenario[i]['descripcion'] + '</p>';
                        cadena += '<hr>';
                        cadena += '<strong><i class="fa fa-file-text-o margin-r-5"></i> Publico objetivo';
                        cadena += '</strong>';
                        cadena += '<p class="text-muted">' + valorEscenario[i]['publicoObjetivo'] + '</p>';
                        cadena += '</div>';
                        cadena += '</div>';
                        cadena += '<h6 class="text-right">';
                        cadena += '<i class="ti-time">' + valorEscenario[i]['hora'] + '</i>';
                        cadena += '</h6>';
                        cadena += '</div>';
                        cadena += '</li>';
                    } else {
                        cadena += '<li class="timeline">';
                        cadena += '<div class="timeline-badge warning">';
                        cadena += '<i class="material-icons">date_range</i>';
                        cadena += '</div>';
                        cadena += '<div class="timeline-panel">';
                        cadena += '<div class="timeline-heading">';
                        cadena += '<span class="label label-warning">' + valorEscenario[i]['fecha'] + '</span>';
                        cadena += '</div>';
                        cadena += '<div class="timeline-body">';
                        cadena += '<h3 class="timeline-header">' + valorEscenario[i]['nombreActividad'] + '</h3>';
                        cadena += '<br>';
                        cadena += '<div class="timeline-body">';
                        cadena += '<strong><i class="fa fa-info-circlemargin-r-5"></i> Escenario</strong><br>';
                        cadena += '<p class="text-muted">' + valorEscenario[i]['nombreEscenario'] + '</p>';
                        cadena += '<hr>';
                        cadena += '<strong><i class="fa fa-map-marker margin-r-5"></i> Ubicacion</strong>';
                        cadena += '<p class="text-muted">' + valorEscenario[i]['lugar'] + '</p>';
                        cadena += '<hr>';
                        cadena += '<strong><i class="fa fa-file-text-o margin-r-5"></i> Contenido Tematico';
                        cadena += '</strong>';
                        cadena += '<p class="text-muted">' + valorEscenario[i]['descripcion'] + '</p>';
                        cadena += '<hr>';
                        cadena += '<strong><i class="fa fa-file-text-o margin-r-5"></i> Publico objetivo';
                        cadena += '</strong>';
                        cadena += '<p class="text-muted">' + valorEscenario[i]['publicoObjetivo'] + '</p>';
                        cadena += '</div>';
                        cadena += '</div>';
                        cadena += '<h6 class="text-right">';
                        cadena += '<i class="ti-time">' + valorEscenario[i]['hora'] + '</i>';
                        cadena += '</h6>';
                        cadena += '</div>';
                        cadena += '</li>';
                    }
                }
                cadena += '</ul>';
                cadena += '</div>'
                cadena += '</div>';
                $('#contenidoTimeline').html(cadena);

            }
        });
    }



});



//});