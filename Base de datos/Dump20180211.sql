-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: repositorio
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.25-MariaDB-

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acta_comite_investigacion`
--

DROP TABLE IF EXISTS `acta_comite_investigacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acta_comite_investigacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL,
  `fecha` date NOT NULL,
  `hora_inicio` time NOT NULL,
  `hora_finalizacion` time NOT NULL,
  `lugar` varchar(45) NOT NULL,
  `proxima_reunion` datetime NOT NULL,
  `moderador` varchar(100) NOT NULL,
  `secretario` varchar(100) NOT NULL,
  `hora_proxima` time DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acta_comite_investigacion`
--

LOCK TABLES `acta_comite_investigacion` WRITE;
/*!40000 ALTER TABLE `acta_comite_investigacion` DISABLE KEYS */;
INSERT INTO `acta_comite_investigacion` VALUES (1,'Titulo','2018-02-08','17:15:00','18:15:00','Lugar','2018-02-23 00:00:00','MOderador','Secretario','08:15:00'),(2,'Acta prueba','2018-02-09','09:15:00','10:15:00','Algun lugar','2018-02-23 00:00:00','Moderador','Secretario','14:15:00'),(3,'Acta prueba 3','2018-02-20','11:30:00','12:30:00','Lugar','2018-02-06 00:00:00','Moderador','Se','11:30:00'),(4,'Nada','2018-02-24','11:45:00','11:45:00','nada','2018-02-22 00:00:00','Ninguno','Ninguno','11:45:00'),(5,'asdasdasd','2018-02-24','11:45:00','11:45:00','asdasd','2018-02-03 00:00:00','asdas','asdasd','11:45:00'),(6,'asdasdasd','2018-02-18','11:45:00','11:45:00','asdasd','2018-02-10 00:00:00','Ninguno','asdsadasd','11:45:00');
/*!40000 ALTER TABLE `acta_comite_investigacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `anotacion`
--

DROP TABLE IF EXISTS `anotacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anotacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `a1` varchar(45) DEFAULT NULL,
  `a2` longtext,
  `b1` varchar(45) DEFAULT NULL,
  `b2` longtext,
  `c1` varchar(45) DEFAULT NULL,
  `c2` longtext,
  `d1` varchar(45) DEFAULT NULL,
  `d2` longtext,
  `e1` varchar(45) DEFAULT NULL,
  `e2` longtext,
  `f1` varchar(45) DEFAULT NULL,
  `f2` longtext,
  `g1` varchar(45) DEFAULT NULL,
  `g2` longtext,
  `h1` varchar(45) DEFAULT NULL,
  `h2` longtext,
  `i1` varchar(45) DEFAULT NULL,
  `i2` longtext,
  `j1` varchar(45) DEFAULT NULL,
  `j2` longtext,
  `k1` varchar(45) DEFAULT NULL,
  `k2` longtext,
  `l1` varchar(45) DEFAULT NULL,
  `l2` longtext,
  `m1` varchar(45) DEFAULT NULL,
  `m2` longtext,
  `n1` varchar(45) DEFAULT NULL,
  `n2` longtext,
  `nn1` varchar(45) DEFAULT NULL,
  `nn2` longtext,
  `o1` varchar(45) DEFAULT NULL,
  `o2` longtext,
  `p1` varchar(45) DEFAULT NULL,
  `p2` longtext,
  `q1` varchar(45) DEFAULT NULL,
  `q2` longtext,
  `r1` varchar(45) DEFAULT NULL,
  `r2` longtext,
  `s1` varchar(45) DEFAULT NULL,
  `s2` longtext,
  `t1` varchar(45) DEFAULT NULL,
  `t2` longtext,
  `u1` varchar(45) DEFAULT NULL,
  `u2` longtext,
  `v1` varchar(45) DEFAULT NULL,
  `v2` longtext,
  `w1` varchar(45) DEFAULT NULL,
  `w2` longtext,
  `x1` varchar(45) DEFAULT NULL,
  `x2` longtext,
  `y1` varchar(45) DEFAULT NULL,
  `y2` longtext,
  `z1` varchar(45) DEFAULT NULL,
  `z2` longtext,
  `usuario_id` bigint(20) DEFAULT NULL,
  `entrega_id` bigint(20) DEFAULT NULL,
  `documento` varchar(140) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `sugerencias` longtext,
  `fecha` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_anotacion_usuario1_idx` (`usuario_id`),
  KEY `fk_anotacion_entrega1_idx` (`entrega_id`),
  CONSTRAINT `fk_anotacion_entrega1` FOREIGN KEY (`entrega_id`) REFERENCES `entrega` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_anotacion_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anotacion`
--

LOCK TABLES `anotacion` WRITE;
/*!40000 ALTER TABLE `anotacion` DISABLE KEYS */;
INSERT INTO `anotacion` VALUES (2,'A','kasld','C','ñlskdflñ','A','ñlksfdklk','B','kljsddfk','NA','ksdfjklj','B','lksdfjkl','A','ksdfjklj','C','lkasjdkl','NA','lksajdfklj','A','lksjdafkjkl','NA','kljsadkljkl','A','kljsfdkljklds','NA','lkjasdkljkals','B','kljasdkljl','A','lkjsadklfjkl','NA','lksajdklj','B','lkjasdkljkl','A','lkjakslddjklj','B','lkasjdkljakl','A','lkjaklsdjkl','NA','lkjaskldjkl','B','lkajsddklj','C','kljasdklj','A','lkjasdkljkl','C','kasjdklj','A','lkjasdklj','NA','klajsdklja',25432417334460431,25432417334460430,'25432417334460430',1,'Ninguna','2018-01-25 17:53:04'),(3,'A','sdfsf','B','sdsdf','B','sdfssdf','A','f','A','','A','','A','','B','','A','','A','','A','','A','','B','','A','','A','','A','','A','','A','','A','','A','','B','','B',' ','A','','B','','A','','A','','A','',25449236241842176,25432417334460430,'25432417334460430',1,'asdasd','2018-02-02 09:56:01'),(6,'A','','A','','A','','A','','A','','A','','A','','A','','A','','A','','A','','A','','A','','A','','A','','A','','A','','A','','A','','A','','A','','A','','A','','A','','A','','A','','A','',25432417334460431,25432417334460432,'25432417334460432',2,'hjgjhgjh','2018-02-02 11:45:51');
/*!40000 ALTER TABLE `anotacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `convocatoria`
--

DROP TABLE IF EXISTS `convocatoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `convocatoria` (
  `id` bigint(20) NOT NULL,
  `descripcion` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `convocatoria`
--

LOCK TABLES `convocatoria` WRITE;
/*!40000 ALTER TABLE `convocatoria` DISABLE KEYS */;
INSERT INTO `convocatoria` VALUES (25432417334460419,'Convocatoria 1'),(25463934223908864,'Convocatoria 2'),(25466606918303744,'PERIODO 2018 -1 ');
/*!40000 ALTER TABLE `convocatoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_proyecto`
--

DROP TABLE IF EXISTS `detalle_proyecto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_proyecto` (
  `id` bigint(20) NOT NULL,
  `titulo` varchar(140) NOT NULL,
  `objetivoGeneral` mediumtext NOT NULL,
  `objetivosEspecificos` mediumtext NOT NULL,
  `productosEsperados` mediumtext NOT NULL,
  `director` varchar(60) NOT NULL,
  `coDirector` varchar(60) NOT NULL,
  `proyecto_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_detalle_proyecto_proyecto1_idx` (`proyecto_id`),
  CONSTRAINT `fk_detalle_proyecto_proyecto1` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_proyecto`
--

LOCK TABLES `detalle_proyecto` WRITE;
/*!40000 ALTER TABLE `detalle_proyecto` DISABLE KEYS */;
INSERT INTO `detalle_proyecto` VALUES (2147483647,'Proyecto 1','Objetivo 1','Especifico 1','Producto 1','Director 1','Codirector 1',25432417334460428),(25449906592284677,'proyecto 23','asd,ja','aksdjk','akjklsjda','klajsdkl','askldjkl',25449906592284676),(25449906592284680,'Proyecto 45','Ninguno','asjdkl','asdjklj','askldjkl','askdjkl',25449906592284679),(25457839984082945,'Proyecto 111','Objetivo 1','Especifico 1','Producto 1','Director 1','Codirector 1',25432417334460428),(25466606918303750,'proyecto prueba 1','proyecto prueba 1','proyecto prueba 1','proyecto prueba 1','proyecto prueba 1','proyecto prueba 1',25466606918303749),(25466657048625154,'prueba anselmo 1','prueba anselmo 1','prueba anselmo 1','prueba anselmo 1','prueba anselmo 1','prueba anselmo 1',25466657048625153),(25467227742404610,'asdadasd','asdasdasd','asdasdasd','asdasdasd','asdasdas','asdasdasd',25467227742404609),(25467373033095170,'asdad','asdasd','asdasd','adsasd','adsasd','asdasd',25467373033095169),(25467373033095174,'asdasd','adasd','asdasd','adsad','adsasd','dadad',25467373033095173),(25467373033095178,'zxczc','zxcxzc','zxczxc','zxczxc','zxcxz','zxczxc',25467373033095177),(25467945438150659,'prueba 11','prueba 11','prueba 11','prueba 11','prueba 11','prueba 11',25467945438150658),(25467945438150663,'prueba 22','prueba 22','prueba 22','prueba 22','prueba 22','prueba 22',25467945438150662);
/*!40000 ALTER TABLE `detalle_proyecto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entrega`
--

DROP TABLE IF EXISTS `entrega`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entrega` (
  `id` bigint(20) NOT NULL,
  `fechaEntrega` date NOT NULL,
  `fechaFin` date NOT NULL,
  `documento` varchar(200) NOT NULL,
  `carta` varchar(200) NOT NULL,
  `proyecto_id` bigint(20) NOT NULL,
  `detalle_proyecto_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_entrega_proyecto1_idx` (`proyecto_id`),
  KEY `fk_entrega_detalle_proyecto1_idx` (`detalle_proyecto_id`),
  CONSTRAINT `entrega_detalle_proyecto_FK` FOREIGN KEY (`detalle_proyecto_id`) REFERENCES `detalle_proyecto` (`id`),
  CONSTRAINT `fk_entrega_proyecto` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entrega`
--

LOCK TABLES `entrega` WRITE;
/*!40000 ALTER TABLE `entrega` DISABLE KEYS */;
INSERT INTO `entrega` VALUES (25432417334460430,'2018-01-13','2018-01-13','documentos/entregas/E25432417334460430_2018-01-13','documentos/cartas/C25432417334460430_2018-01-13',25432417334460428,2147483647),(25432417334460432,'2018-01-31','2018-01-31','documentos/entregas/E25432417334460432_2018-01-31','documentos/cartas/C25432417334460432_2018-01-31',25432417334460428,25457839984082945),(25449906592284678,'2018-01-25','0000-00-00','documentos/entregas/E25449906592284678_2018-01-25','documentos/cartas/C25449906592284678_2018-01-25',25449906592284676,25449906592284677),(25449906592284681,'2018-01-25','0000-00-00','documentos/entregas/E25449906592284681_2018-01-25','documentos/cartas/C25449906592284681_2018-01-25',25449906592284679,25449906592284680),(25466606918303751,'2018-02-06','2018-02-06','documentos/entregas/E25466606918303751_2018-02-06','documentos/cartas/C25466606918303751_2018-02-06',25466606918303749,25466606918303750),(25466657048625155,'2018-02-06','2018-02-06','documentos/entregas/E25466657048625155_2018-02-06','documentos/cartas/C25466657048625155_2018-02-06',25466657048625153,25466657048625154),(25467227742404611,'2018-02-06','2018-02-06','documentos/entregas/E25467227742404611_2018-02-06','documentos/cartas/C25467227742404611_2018-02-06',25467227742404609,25467227742404610),(25467373033095171,'2018-02-06','2018-02-06','documentos/entregas/E25467373033095171_2018-02-06','documentos/cartas/C25467373033095171_2018-02-06',25467373033095169,25467373033095170),(25467373033095175,'2018-02-06','2018-02-06','documentos/entregas/E25467373033095175_2018-02-06','documentos/cartas/C25467373033095175_2018-02-06',25467373033095173,25467373033095174),(25467373033095179,'2018-02-06','2018-02-06','documentos/entregas/E25467373033095179_2018-02-06','documentos/cartas/C25467373033095179_2018-02-06',25467373033095177,25467373033095178),(25467945438150660,'2018-02-07','2018-02-15','documentos/entregas/E25467945438150660_2018-02-07','documentos/cartas/C25467945438150660_2018-02-07',25467945438150658,25467945438150659),(25467945438150664,'2018-02-07','2018-02-15','documentos/entregas/E25467945438150664_2018-02-07','documentos/cartas/C25467945438150664_2018-02-07',25467945438150662,25467945438150663);
/*!40000 ALTER TABLE `entrega` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facultad`
--

DROP TABLE IF EXISTS `facultad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facultad` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facultad`
--

LOCK TABLES `facultad` WRITE;
/*!40000 ALTER TABLE `facultad` DISABLE KEYS */;
INSERT INTO `facultad` VALUES (4,'Ing Informatica'),(5,'Ing Electronica');
/*!40000 ALTER TABLE `facultad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fecha_entrega`
--

DROP TABLE IF EXISTS `fecha_entrega`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fecha_entrega` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `convocatoria_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_fecha_entrega_convocatoria1_idx` (`convocatoria_id`),
  CONSTRAINT `fecha_entrega_convocatoria_FK` FOREIGN KEY (`convocatoria_id`) REFERENCES `convocatoria` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25466606918303748 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fecha_entrega`
--

LOCK TABLES `fecha_entrega` WRITE;
/*!40000 ALTER TABLE `fecha_entrega` DISABLE KEYS */;
INSERT INTO `fecha_entrega` VALUES (25432417334460420,'2018-01-04',25432417334460419),(25432417334460421,'2018-01-11',25432417334460419),(25432417334460422,'2018-01-28',25432417334460419),(25463934223908865,'2018-02-06',25463934223908864),(25463934223908866,'2018-02-15',25463934223908864),(25463934223908867,'2018-02-24',25463934223908864),(25466606918303745,'2018-02-09',25466606918303744),(25466606918303746,'2018-02-10',25466606918303744),(25466606918303747,'2018-02-11',25466606918303744);
/*!40000 ALTER TABLE `fecha_entrega` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fecha_reporte`
--

DROP TABLE IF EXISTS `fecha_reporte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fecha_reporte` (
  `fecha` datetime DEFAULT NULL,
  `id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fecha_reporte`
--

LOCK TABLES `fecha_reporte` WRITE;
/*!40000 ALTER TABLE `fecha_reporte` DISABLE KEYS */;
INSERT INTO `fecha_reporte` VALUES ('2018-02-09 11:40:22',1);
/*!40000 ALTER TABLE `fecha_reporte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `iditems` int(11) NOT NULL AUTO_INCREMENT,
  `contenido` longtext NOT NULL,
  `idActa` int(11) NOT NULL,
  `condicion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`iditems`),
  KEY `fk_items_acta_comite_investigacion1_idx` (`idActa`),
  CONSTRAINT `fk_items_acta_comite_investigacion1` FOREIGN KEY (`idActa`) REFERENCES `acta_comite_investigacion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (5,'Orden 2',1,'1'),(6,'Orden 3',1,'1'),(7,'Orden 4',1,'1'),(8,'Desarrollo 1',1,'2'),(9,'Desarrollo 2',1,'2'),(10,'Desarrollo 3',1,'2'),(11,'Orden 1',2,'1'),(12,'Orden 2',2,'1'),(13,'Orden 3',2,'1'),(14,'Desarrollo 1',2,'2'),(15,'Desarrollo 2',2,'2'),(16,'Desarrollo 3',2,'2'),(17,'fddfg',3,'1'),(18,'fddfgf',3,'1'),(19,'fddfgff',3,'1'),(20,'dfgdfg',3,'2'),(21,'dfgdfgdfg',3,'2'),(22,'dfgdfgdfgdfg',3,'2'),(23,'Reporte',3,'1'),(24,'Entrega del proyecto prueba 11 el dia 07-02-2018.\nEntrega del proyecto prueba 22 el dia 07-02-2018.\nEntrega del proyecto proyecto prueba 1 el dia 06-02-2018.\nEntrega del proyecto asdasd el dia 06-02-2018.\nEntrega del proyecto prueba anselmo 1 el dia 06-02-2018.\nEntrega del proyecto zxczc el dia 06-02-2018.\nEntrega del proyecto asdadasd el dia 06-02-2018.\nEntrega del proyecto asdad el dia 06-02-2018.\nAnotación al proyecto Proyecto 111 el dia 02-02-2018.Realizada por PEPE HERNANDEZ.\nAnotación al proyecto Proyecto 1 el dia 02-02-2018.Realizada por ANDREA JUAREZ.\nEntrega del proyecto Proyecto 111 el dia 31-01-2018.\nAnotación al proyecto Proyecto 1 el dia 25-01-2018.Realizada por PEPE HERNANDEZ.\nEntrega del proyecto proyecto 23 el dia 25-01-2018.\nEntrega del proyecto Proyecto 45 el dia 25-01-2018.\nEntrega del proyecto Proyecto 1 el dia 13-01-2018.\n',3,'2'),(25,'asdasd',4,'1'),(26,'asdasdsad',4,'1'),(27,'asdsad',4,'2'),(28,'asdsad',4,'2'),(29,'dfg',5,'1'),(30,'dfgdfg',5,'1'),(31,'sdfg',5,'2'),(32,'sdfgsdg',5,'2'),(33,'dfgdfg',6,'1'),(34,'dfgdfg',6,'1'),(35,'dffdf',6,'2'),(36,'dffdf',6,'2');
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permiso`
--

DROP TABLE IF EXISTS `permiso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permiso` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permiso`
--

LOCK TABLES `permiso` WRITE;
/*!40000 ALTER TABLE `permiso` DISABLE KEYS */;
INSERT INTO `permiso` VALUES (5,'Listar usuarios'),(6,'Registrar usuarios'),(7,'Editar usuarios'),(8,'Eliminar usuarios'),(9,'Listar convocatorias'),(10,'Registrar convocatorias'),(11,'Editar convocatorias'),(12,'Eliminar convocatorias'),(13,'Listar proyectos'),(14,'Registrar proyectos'),(15,'Editar proyectos'),(16,'Eliminar proyectos'),(17,'Listar estudiantes'),(18,'Listar Anotaciones'),(19,'Registrar Anotaciones'),(20,'Historial estudiante'),(21,'Nueva Entrega de Proyecto'),(22,'Modificar Estado Anotacion');
/*!40000 ALTER TABLE `permiso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persona` (
  `idpersona` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `cargo` varchar(100) NOT NULL,
  `condicion` varchar(45) NOT NULL,
  `idActa` int(11) NOT NULL,
  PRIMARY KEY (`idpersona`),
  KEY `fk_persona_acta_comite_investigacion1_idx` (`idActa`),
  CONSTRAINT `fk_persona_acta_comite_investigacion1` FOREIGN KEY (`idActa`) REFERENCES `acta_comite_investigacion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona`
--

LOCK TABLES `persona` WRITE;
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
INSERT INTO `persona` VALUES (5,'asistente 2','sda','1',1),(6,'asistente 3','sda','1',1),(7,'asdas','asdasd','2',1),(8,'asd','sdfsf','1',2),(9,'asdf','sdfsf','1',2),(10,'dfgdg','dfgd','3',2),(11,'ghjfgjh','fgfgh','2',2),(12,'ghjfgjh43','fgfgh','2',2),(13,'dfssd','dfdfg','1',3),(14,'dfssdsd','dfdfg','1',3),(15,'dfg','dfg','3',3),(16,'sawerw','sdfs','2',3),(17,'asdas','asdasd','1',4),(18,'asd','asdasda','1',5),(19,'sdf','sdf','3',5),(20,'sdfsdf','sdf','2',5),(21,'asdasd','asdasd','1',6),(22,'asdasddfdf','asdasd','1',6),(23,'sdfsdf','sdfsdf','3',6),(24,'sdfsdfsdf','sdfsdfds','2',6);
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proyecto`
--

DROP TABLE IF EXISTS `proyecto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proyecto` (
  `id` bigint(20) NOT NULL,
  `estado` varchar(45) NOT NULL,
  `fecha_entrega_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_proyecto_fecha_entrega1_idx` (`fecha_entrega_id`),
  CONSTRAINT `proyecto_fecha_entrega_FK` FOREIGN KEY (`fecha_entrega_id`) REFERENCES `fecha_entrega` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proyecto`
--

LOCK TABLES `proyecto` WRITE;
/*!40000 ALTER TABLE `proyecto` DISABLE KEYS */;
INSERT INTO `proyecto` VALUES (25432417334460428,'PROPUESTA',25432417334460422),(25449906592284674,'PROPUESTA',25432417334460422),(25449906592284676,'PROPUESTA',25432417334460422),(25449906592284679,'PROPUESTA',25432417334460422),(25466606918303749,'PROPUESTA',25463934223908865),(25466657048625153,'PROPUESTA',25463934223908865),(25467227742404609,'PROPUESTA',25463934223908865),(25467373033095169,'PROPUESTA',25463934223908865),(25467373033095173,'PROPUESTA',25463934223908865),(25467373033095177,'PROPUESTA',25463934223908865),(25467945438150658,'PROPUESTA',25463934223908866),(25467945438150662,'PROPUESTA',25463934223908866);
/*!40000 ALTER TABLE `proyecto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rol`
--

DROP TABLE IF EXISTS `rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rol` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol`
--

LOCK TABLES `rol` WRITE;
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;
INSERT INTO `rol` VALUES (4,'Admin'),(5,'Par'),(6,'Estudiante');
/*!40000 ALTER TABLE `rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rol_permiso`
--

DROP TABLE IF EXISTS `rol_permiso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rol_permiso` (
  `idRol` int(20) NOT NULL,
  `idPermiso` int(20) NOT NULL,
  PRIMARY KEY (`idRol`,`idPermiso`),
  KEY `fk2_idx` (`idPermiso`),
  CONSTRAINT `fkpermiso1` FOREIGN KEY (`idPermiso`) REFERENCES `permiso` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fkrol1` FOREIGN KEY (`idRol`) REFERENCES `rol` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol_permiso`
--

LOCK TABLES `rol_permiso` WRITE;
/*!40000 ALTER TABLE `rol_permiso` DISABLE KEYS */;
INSERT INTO `rol_permiso` VALUES (4,5),(4,6),(4,7),(4,8),(4,9),(4,10),(4,11),(4,12),(4,13),(4,14),(4,15),(4,16),(4,17),(4,18),(4,22),(5,9),(5,13),(5,17),(5,18),(5,19),(6,14),(6,17),(6,18),(6,20),(6,21);
/*!40000 ALTER TABLE `rol_permiso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id` bigint(20) NOT NULL,
  `nombres` varchar(45) NOT NULL,
  `apellidos` varchar(45) NOT NULL,
  `correo` varchar(80) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `idRol` int(20) NOT NULL,
  `idBanner` int(14) NOT NULL,
  `idFacultad` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk1_idx` (`idRol`),
  KEY `usuario_facultad_FK` (`idFacultad`),
  CONSTRAINT `fk1` FOREIGN KEY (`idRol`) REFERENCES `rol` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `usuario_facultad_FK` FOREIGN KEY (`idFacultad`) REFERENCES `facultad` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (4,'Admin','Admin','admin@a.co','$2y$10$UQitIIZPUsskaZ5baF9MTuJ28F.BKhunESWZx/zkS3gH4/MBGCl7u',4,7845,4),(25432417334460423,'Juan Andres','Perez','f@b.com','$2y$10$JqwU7xZCq0HjmcuiWaeZB.G6/uHSHSq7PM5K55qt8WWL0tqunFUCy',6,1245,4),(25432417334460431,'Pepe','Hernandez','p@h.co','$2y$10$pmuIqjmvBVhss/fC.3aDPO8vy/R6J6xWptj0JOEdATYbGT.lSQkyC',5,9856,4),(25449236241842176,'Andrea','Juarez','a@upb.co','$2y$10$O71NAoBf86J1pBIKmof0Yeg42sM1oSoPoDRPSaZYLGHbdZ.TxEVKy',5,4321,4),(25449906592284672,'Pepe','Cadena','n@m.co','$2y$10$tgEuf0YR6r3Ji1UJZKylCeNSz261fp8Se6PNmxPyr.zmw7FFNh1Be',6,1111,4),(25449906592284673,'maria','martinez','m@g.com','$2y$10$G87ed6qlVeNqJ9v4OjYxcOL14QRw71t/BLGXfYYlFG9DUe8jQMjKi',6,2222,4),(25466606918303748,'Donald Armando ','Torres Arteaga','donald@donald.com','$2y$10$vOMbBg75zuDbtXoJOD2Wf.nV4N8DFG2HTuMoto2OVNdPwF3sEpLl2',6,199031,4),(25466657048625152,'Anselmo','Anselmo','a@a.com','$2y$10$XNGzXPrDBkWwgU3b96Y3k.LSxjcAGaZ9qQD0jnZdkkqIh5OQG2vRG',6,84890,4),(25467227742404608,'donald','donald','a@a.com','$2y$10$dBRp763kOnuzpHwLPT4ObOEt1rTxVLlD9izVFUmh1oNyt3fH4Oenq',6,199032,4),(25467373033095168,'armando','armando','a@a.com','$2y$10$DOLETOYd6dRxsemuQh0BBe3cGvgjmIgAOloqnYOIEkOVWbaTcwF3.',6,199033,4),(25467373033095172,'torres','torres','a@a.com','$2y$10$KeWz1GnP187Qsrz4fFUd/eHMLo54GFKMZnCOlQcCT0kvvRWcq1wfG',6,199034,4),(25467373033095176,'arteaga','arteaga','a@a.com','$2y$10$onAK6fu6pBDzDJfG1NO2IOgsTlg1zpaLEHf8EN5xY7riAemsauiD2',6,199035,4),(25467945438150656,'aaaa','aaaa','q@a.com','$2y$10$i.HWktVLBS0opb1h0aEiKOAcLusJGn7gjo87ZN2oFWn3iks2MgP5.',6,9999,4),(25467945438150657,'aaaaa','aaaaa','a@a.com','$2y$10$TOn.TtDdsQtY6eVaQVcJ4e0m9K7hnxbdEDrrOWb0RiR01vPRV8jY6',6,6666,4),(25467945438150661,'falcao','falcao','a@a.com','$2y$10$6AbHw4PalCVkS7hvwk1ZR.5gFmXRGY3yXWrMTCbfp6AytCO5ocNVK',6,7777,4);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario_proyecto`
--

DROP TABLE IF EXISTS `usuario_proyecto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario_proyecto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id` bigint(20) NOT NULL,
  `proyecto_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_usuario_has_proyecto_usuario1_idx` (`usuario_id`),
  KEY `fk_usuario_proyecto_proyecto1_idx` (`proyecto_id`),
  CONSTRAINT `fk_usuario_has_proyecto_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_proyecto_proyecto1` FOREIGN KEY (`proyecto_id`) REFERENCES `proyecto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario_proyecto`
--

LOCK TABLES `usuario_proyecto` WRITE;
/*!40000 ALTER TABLE `usuario_proyecto` DISABLE KEYS */;
INSERT INTO `usuario_proyecto` VALUES (3,25432417334460423,25432417334460428),(4,25432417334460431,25432417334460428),(5,25449236241842176,25432417334460428),(8,25449236241842176,25449906592284676),(9,25449906592284673,25449906592284679),(10,25466606918303748,25466606918303749),(11,25466657048625152,25466657048625153),(12,25467227742404608,25467227742404609),(13,25467373033095168,25467373033095169),(14,25467373033095172,25467373033095173),(15,25467373033095176,25467373033095177),(16,25467945438150657,25467945438150658),(17,25467945438150661,25467945438150662);
/*!40000 ALTER TABLE `usuario_proyecto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'repositorio'
--
/*!50003 DROP PROCEDURE IF EXISTS `detalleEntregaUltimo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `detalleEntregaUltimo`(in idProyectoBuscar BIGINT)
BEGIN

select d.*
from detalle_proyecto as d 
inner join entrega as e 
on e.detalle_proyecto_id = d.id
where e.proyecto_id = idProyectoBuscar
and e.fechaEntrega is not NULL
order by e.fechaEntrega DESC
limit 1;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `estadoProyecto` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `estadoProyecto`(in idProyectoBuscar BIGINT)
BEGIN

select estado, e.fechaEntrega as fecha, nombres, apellidos, idBanner
from entrega as e 
inner join anotacion as a 
on e.id = a.entrega_id
inner join usuario as u
on a.usuario_id = u.id
where e.proyecto_id = idProyectoBuscar
order by a.fecha desc;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `estudiantesProyectos` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `estudiantesProyectos`()
BEGIN

select up.proyecto_id as idProyecto, nombres, apellidos, idBanner, f.nombre as facultad
from usuario as u 
inner join facultad as f
on u.idFacultad = f.id
inner join usuario_proyecto as up 
on u.id = up.usuario_id
where idRol = 6;
 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `historialProyecto` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `historialProyecto`(in idProyectoBuscar BIGINT)
BEGIN

select *
from
(select 1 as tipo, entrega.id as idHistoria, date_format(fechaEntrega, "%d-%m-%Y") as fecha,fechaEntrega as fechaOrdenar, date_format(fechaEntrega, "%l:%i") as hora , 0 as usuario_id, "" as nombres, entrega.documento, entrega.carta
from entrega
where proyecto_id = idProyectoBuscar
and detalle_proyecto_id is not NULL
UNION
SELECT 2 as tipo, a.id as idHistoria, date_format(a.fecha, "%d-%m-%Y") as fecha, a.fecha as fechaOrdenar, date_format(a.fecha, "%l:%i") as hora, usuario_id, concat(u.nombres, " ", u.apellidos) as nombres, a.documento, "" as carta
from anotacion as a
inner join entrega as e
on a.entrega_id = e.id
inner join usuario as u
on u.id = a.usuario_id
where e.proyecto_id = idProyectoBuscar) as historial
order by fechaOrdenar desc;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `historialProyectoFecha` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `historialProyectoFecha`(in fechaInicio Datetime)
BEGIN

select *
from
(select 1 as tipo, e.id as idHistoria, date_format(fechaEntrega, "%d-%m-%Y") as fecha,fechaEntrega as fechaOrdenar, date_format(fechaEntrega, "%l:%i") as hora , 0 as usuario_id, "" as nombres, dp.titulo
from entrega as e 
inner join repositorio.detalle_proyecto as dp 
on e.detalle_proyecto_id = dp.id
where detalle_proyecto_id is not NULL
UNION
SELECT 2 as tipo, a.id as idHistoria, date_format(a.fecha, "%d-%m-%Y") as fecha, a.fecha as fechaOrdenar, date_format(a.fecha, "%l:%i") as hora, usuario_id, concat(u.nombres, " ", u.apellidos) as nombres, dp.titulo 
from anotacion as a
inner join entrega as e
on a.entrega_id = e.id
inner join repositorio.detalle_proyecto as dp
on e.detalle_proyecto_id = dp.id
inner join usuario as u
on u.id = a.usuario_id) as historial
where fechaOrdenar >= fechaInicio
order by fechaOrdenar desc;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `listarActaId` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'IGNORE_SPACE,STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `listarActaId`(in idActaB int)
BEGIN

SET lc_time_names = 'es_CO';
select id, titulo, date_format(fecha,'%d de %M de %Y') as fecha, time_format(hora_inicio, '%h:%i %p') as hora_inicio,
time_format(hora_finalizacion, '%h:%i %p') as hora_finalizacion, lugar, date_format(proxima_reunion,'%d de %M de %Y') as proxima_reunion,
moderador, secretario, time_format(hora_proxima, '%h:%i %p') as hora_proxima 
from acta_comite_investigacion
where id = idActaB;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `listarActas` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'IGNORE_SPACE,STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `listarActas`()
BEGIN


SET lc_time_names = 'es_CO';
select id, titulo, date_format(fecha,'%d de %M de %Y') as fecha, time_format(hora_inicio, '%h:%i %p') as hora_inicio,
time_format(hora_finalizacion, '%h:%i %p') as hora_finalizacion, lugar, date_format(proxima_reunion,'%d de %M de %Y') as proxima_reunion,
moderador, secretario, time_format(hora_proxima, '%h:%i %p') as hora_proxima 
from acta_comite_investigacion;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `listarAnotacionId` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `listarAnotacionId`(in idAnotacionBuscar int)
BEGIN

select a.*, e.fechaEntrega, e.documento as documentoEntrega, e.carta, e.proyecto_id, dp.titulo, a.documento as documentoAnotacion
from anotacion as a 
inner join entrega as e 
on a.entrega_id = e.id
inner join detalle_proyecto as dp 
on dp.id = e.detalle_proyecto_id
where a.id = idAnotacionBuscar;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `listarConvocatoria` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `listarConvocatoria`()
BEGIN
select id as idConvocatoria, descripcion
from convocatoria;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `listarEntregaId` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `listarEntregaId`(in idEntregaBuscar BIGINT)
BEGIN

select e.id as idEntrega, documento, carta, fechaEntrega, titulo, p.estado, e.proyecto_id
from entrega as e
inner join repositorio.usuario_proyecto as up
on e.proyecto_id = up.proyecto_id
inner join repositorio.detalle_proyecto as dp
on dp.id = e.detalle_proyecto_id
inner join repositorio.proyecto as p
on p.id = e.proyecto_id
where e.id = idEntregaBuscar
group by e.id;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `listarEntregaInicial` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `listarEntregaInicial`()
BEGIN
select e.id as idEntrega, DATE_FORMAT(fechaInicio,"%d%-%m-%Y") as fechaInicio, DATE_FORMAT(fechaFin,"%d%-%m-%Y") as fechaFin, idConvocatoria,descripcion
from entrega_inicial as e
inner join convocatoria as c
on c.id = e.idConvocatoria;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `listarEntregaInicialActual` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `listarEntregaInicialActual`()
BEGIN
select fe.id as idFechaEntrega, DATE_FORMAT(fecha,"%d%-%m-%Y") as fecha, convocatoria_id,c.descripcion
from fecha_entrega as fe
inner join convocatoria as c
on c.id = fe.convocatoria_id
where fe.fecha > DATE_SUB(NOW(), interval 1 day);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `listarEntregaInicialId` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `listarEntregaInicialId`(idBuscar bigint)
BEGIN
select e.id as idEntrega, DATE_FORMAT(fechaInicio,"%d%-%m-%Y") as fechaInicio, DATE_FORMAT(fechaFin,"%d%-%m-%Y") as fechaFin, idConvocatoria,descripcion
from entrega_inicial as e
inner join convocatoria as c
on c.id = e.idConvocatoria
where e.id = idBuscar;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `listarEntregasParaRevision` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `listarEntregasParaRevision`(in idUsuarioBuscar BIGINT)
BEGIN

select e.id as idEntrega, documento, fechaEntrega, titulo, p.estado
from entrega as e
inner join repositorio.usuario_proyecto as up
on e.proyecto_id = up.proyecto_id
inner join usuario as u 
on up.usuario_id = u.id
inner join repositorio.detalle_proyecto as dp
on dp.id = e.detalle_proyecto_id
inner join repositorio.proyecto as p
on p.id = e.proyecto_id
where e.id not in 
(select entrega_id from repositorio.anotacion where usuario_id = idUsuarioBuscar)
and u.idRol = 5
and u.id = idUsuarioBuscar
and e.proyecto_id not in 
(select e.proyecto_id from anotacion as a inner join entrega as e on a.entrega_id = e.id where usuario_id = idUsuarioBuscar and estado = 3);

	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `listarEntregasProyecto` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `listarEntregasProyecto`(in idProyectoBuscar BIGINT)
BEGIN
select id as idEntrega, fechaEntrega, fechaFin, proyecto_id, detalle_proyecto_id
from entrega
where proyecto_id = idProyectoBuscar
order by fechaFin desc;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `listarFechaEntregaConvocatoria` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `listarFechaEntregaConvocatoria`(in idConvocatoriaBuscar bigint)
BEGIN

select id, DATE_FORMAT(fecha,"%d%-%m-%Y") as fecha from fecha_entrega 
where convocatoria_id = idConvocatoriaBuscar;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `listarIntegrantesProyecto` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `listarIntegrantesProyecto`(in idProyectoBuscar BIGINT, in idRolBuscar int)
BEGIN
select u.id as idUsuario, nombres, apellidos,correo,idRol, idBanner,
idFacultad, f.nombre as facultad
from usuario as u
inner join usuario_proyecto as up
on u.id = up.usuario_id
inner join proyecto as p 
on p.id = up.proyecto_id
inner join facultad as f
on f.id = u.idFacultad
where p.id = idProyectoBuscar
and u.idRol = idRolBuscar;	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `listarItemsActa` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'IGNORE_SPACE,STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `listarItemsActa`(in idActaB int, in condicionB varchar(45))
BEGIN
	
select i.*, @rownum := @rownum + 1 AS rank 
from items as i, 
       (SELECT @rownum := 0) as r
where idActa = idActaB
and condicion like condicionB;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `listarPersonasActa` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'IGNORE_SPACE,STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `listarPersonasActa`(in idActaB int, in condicionB varchar(45))
BEGIN

select * 
from persona
where idActa = idActaB
and condicion like condicionB;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `listarProyecto` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `listarProyecto`()
BEGIN
select d.proyecto_id as idProyecto, titulo, estado
from (
select uno.*
from entrega as uno
left join entrega as dos
on (uno.proyecto_id = dos.proyecto_id and uno.fechaEntrega < dos.fechaEntrega)
where dos.id is null
) as d
inner join detalle_proyecto as dp
on dp.id = d.detalle_proyecto_id
inner join proyecto as p
on d.proyecto_id = p.id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `listarProyectoEntre` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `listarProyectoEntre`(fechaInicio DATE, fechaFin DATE)
BEGIN
SELECT id, nombre, estado 
FROM proyecto 
INNER JOIN entrega_inicial 
WHERE fechaEntrega BETWEEN fechaInicio AND fechaFin;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `listarProyectoId` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `listarProyectoId`(id_proyecto BIGINT(20))
BEGIN
SELECT p.id as idProyecto, estado,
titulo, d.id as idDetalle
FROM proyecto as p 
inner join detalle_proyecto as d
on p.id = d.proyecto_id 
WHERE id = id_proyecto;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `listarUltimaAnotacionPares` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `listarUltimaAnotacionPares`(in idProyectoBuscar BIGINT)
BEGIN

declare idUltimaEntrega BIGINT;

select id into idUltimaEntrega from entrega where proyecto_id = idProyectoBuscar order by fechaEntrega desc limit 1;
	
select concat(u.nombres," ", u.apellidos) as par,a.id as idAnotacion, a.documento, IF(a.estado is null, "NO HA SIDO REVISADO" , IF(a.estado = 1,"APROBADO", IF(a.estado = 2, "APROBADO CON CORRECCIONES", "RECHAZADO"))) as estado
from usuario_proyecto as up
inner join usuario as u
on up.usuario_id = u.id
left join anotacion as a 
on a.usuario_id = up.usuario_id
where a.entrega_id = idUltimaEntrega
or (a.id is null and u.idRol = 5 and up.proyecto_id = idProyectoBuscar);



	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `listarUltimaEntrega` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `listarUltimaEntrega`(in idProyectoBuscar BIGINT)
BEGIN
select id as idEntrega, fechaEntrega, fechaFin, proyecto_id, detalle_proyecto_id
from entrega
where proyecto_id = idProyectoBuscar
order by fechaFin desc
limit 1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `listarUsuario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `listarUsuario`()
BEGIN
select u.id as idUsuario, nombres, apellidos,correo,idRol,idFacultad, idBanner, 
descripcion as rol, f.nombre as facultad
from usuario as u
inner join rol as r
on u.idRol = r.id
inner join facultad as f
on f.id = u.idFacultad;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `listarUsuarioId` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `listarUsuarioId`(idUsuarioBuscar BIGINT(20))
BEGIN
select u.id as idUsuario, nombres, apellidos,correo,idRol,idFacultad, idBanner,
descripcion as rol, f.nombre as facultad
from usuario as u
inner join rol as r
on u.idRol = r.id
inner join facultad as f
on f.id = u.idFacultad
where u.id = idUsuarioBuscar;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `listarUsuarioIdBanner` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `listarUsuarioIdBanner`(idBannerBuscar bigint(20))
BEGIN
select id as idUsuario, nombres, apellidos,correo,pass,idRol,idFacultad, idBanner, pass
from usuario
where idBanner = idBannerBuscar;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `listarUsuarioPorRol` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `listarUsuarioPorRol`(idRolBuscar int)
BEGIN
select u.id as idUsuario, nombres, apellidos,correo,pass,idRol, idBanner,
idFacultad, f.nombre as facultad
from usuario as u
inner join facultad as f
on f.id = u.idFacultad
where idRol = idRolBuscar;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `modificarContraseña` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`seguri_gbang`@`%` PROCEDURE `modificarContraseña`(idUsuario BIGINT(20),contraseña VARCHAR(100))
BEGIN
UPDATE usuario
SET pass = $contraseña
WHERE id = contraseña;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `nuevaEntregaProyecto` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `nuevaEntregaProyecto`(in idProyectoBuscar BIGINT)
BEGIN

select id
from entrega as e 
where fechaEntrega is NULL
and proyecto_id = idProyectoBuscar
and fechaFin > DATE_SUB(NOW(), interval 1 day);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `verProyectoDeIntegrante` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `verProyectoDeIntegrante`(in idUsuarioBuscar BIGINT)
BEGIN

select * from
(select p.id as idProyecto, dp.titulo, e.documento, p.estado, count(*) totalEntregas
from proyecto as p
inner join usuario_proyecto as up
on up.proyecto_id = p.id
inner join detalle_proyecto as dp
on dp.proyecto_id = p.id
inner join entrega as e
on e.detalle_proyecto_id = dp.id
where up.usuario_id = idUsuarioBuscar
order by e.fechaEntrega desc) as entregasProyecto
limit 1;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-11 13:57:33
