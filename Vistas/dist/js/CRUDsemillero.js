$(document).on('click', '[id*=inscritos]', function (e) {
    e.preventDefault();
    var url_id = $(this).attr('href');
    $.ajax({
        url: url_id,
        success: function (result) {
            if (result != '-1') {
                var valoresInscritos = JSON.parse(result);
                var cadena = ' <table class="table table-condensed" align="center">'
                cadena += '<tr>'
                cadena += '<th>ID</th>'
                cadena += '<th>Nombre</th>'
                cadena += '<th>Apellido</th>'
                cadena += '</tr>';
                for (var i = 0; i < valoresInscritos.length; i++) {
                    cadena += '<tr>';
                    cadena += '<td>' + valoresInscritos[i].idUsuario + '</td>';
                    cadena += '<td>' + valoresInscritos[i].nombre + '</td>';
                    cadena += '<td>' + valoresInscritos[i].apellido + '</td>';
                    cadena += '</td>';
                }
                cadena += '</table>';
                swal({
                    html: cadena,
                    confirmButtonText: 'OK',
                    confirmButtonClass: 'btn btn-warning',
                    buttonsStyling: false
                })
            } else {
                swal({
                    title: 'Informacion',
                    html: 'No hay inscritos en el semillero',
                    type: 'warning',
                    confirmButtonText: 'Confirmar',
                    confirmButtonClass: 'btn btn-warning',
                    buttonsStyling: false
                })
            }
        }
    });
});

// funcion en la cual se realiza la peticion de la informacion que sera recomendada en los imput del modal que se invoca en esta misma funcion.
var urlBase = '';
$(document).on('click', '[id*=updateSemillero]', function (e) {
    e.preventDefault();
    var url_id = $(this).attr('href');
    $.ajax({
        url: url_id,
        success: function (result) {
            if (result != '-1') {
                var valoresCursoLibre = JSON.parse(result);
                $("#nombre").val(valoresCursoLibre[0]['nombre']);
                $("#Contenido_Tematico").val(valoresCursoLibre[0]['contenido_tematico']);
                $("#idSemillero").val(valoresCursoLibre[0]['idsemillero']);
                urlBase = valoresCursoLibre[0]['url_base'];
                $.ajax({
                    url: valoresCursoLibre[0]['url_base'] + 'index.php/CursoLibre_C/obtenerDocentes',
                    success: function (result) {
                        var valoresDocentes = JSON.parse(result);
                        var cadena = '<select name="docente" class="form-control select2" style="width:100%;">';
                        for (var i = 0; i < valoresDocentes.length; i++) {
                            if (valoresDocentes[i].nombre + ' ' + valoresDocentes[i].apellido == valoresCursoLibre[0].nombre_docente + ' ' + valoresCursoLibre[0].apellido_docente) {
                                cadena += ' <option selected value=' + valoresDocentes[i].idUsuario + '>' + valoresDocentes[i].nombre + ' ' + valoresDocentes[i].apellido + '</option>';
                            } else {
                                cadena += ' <option value=' + valoresDocentes[i].idUsuario + '>' + valoresDocentes[i].nombre + ' ' + valoresDocentes[i].apellido + '</option>';
                            }
                        }
                        cadena += '</select>';
                        $('#docente').html(cadena);
                        $('.select2').select2();
                    }
                });
            } else {
                notificacionError();
            }
        }
    });
    $('#modal_modificar').modal('show');
    return false;
});

// luego de haber realizado la funcion inmediatamente anterior se procede a confirmar la informacion a modificar y se realiza via ajax para luego realizar la modificacion de los datos.
$(document).on('submit', '#formularioModificarSemillero', function (e) {
    e.preventDefault();
    swal({
        title: '¿Esta Seguro?',
        text: 'Se modificara la informacion del curso libre selecionado',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Confirmar',
        cancelButtonText: 'Cancelar',
        confirmButtonClass: 'btn btn-block btn-info',
        cancelButtonClass: 'btn btn-block btn-link',
        buttonsStyling: false
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: $(this).attr("action"),
                type: $(this).attr("method"),
                data: $(this).serialize(),
                success: function (result) {
                    if (result == 'correcto') {
                        notificacionCorrecto();
                        $('#modal_modificar').modal('toggle');
                        $('table').DataTable({
                            "destroy": true,
                            "language": {
                                "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                            },
                            'ajax': {
                                'method': 'POST',
                                'url': urlBase + 'index.php/Semilleros_C/listar_ajax'
                            },
                            "columns": [
                                { "data": "Nombre" },
                                { "data": "Inscritos" },
                                { "data": "Descripcion" },
                                { "data": "Docente" },
                                { "data": "Inscritos_Nombre" },
                                { "data": "Eliminar" },
                                { "data": "Modificar" }
                            ]
                        });
                    } else {
                        if (result == '-1') {
                            notificacionError();
                        } else {
                            swal({
                                title: 'Error',
                                html: result,
                                type: 'error',
                                confirmButtonText: 'Confirmar',
                                confirmButtonClass: 'btn btn-danger',
                                buttonsStyling: false
                            })
                        }
                    }
                }
            });
        }
    })
});

// Funcion eliminar Curso libre en la cual se realiza primero una peticion ajax para capturar los datos y verificar la selecion, luego se procede a realizar la eliminacion via ajax y retornando una notificacion de exito.
$(document).on('click', '[id*=deleteSemillero]', function (e) {
    e.preventDefault();
    var url_id = $(this).attr('href');
    $.ajax({
        url: url_id,
        success: function (result) {
            if (result != '-1') {
                var json = JSON.parse(result);
                swal({
                    title: '¿Esta Seguro?',
                    text: 'Se eliminara ' + json[0]['nombre'] + '!',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Confirmar',
                    cancelButtonText: 'Cancelar',
                    confirmButtonClass: 'btn btn-block btn-danger',
                    cancelButtonClass: 'btn btn-block btn-link',
                    buttonsStyling: false
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url: json[0]['url_base'] + 'index.php/Semilleros_C/eliminar/' + json[0]['idsemillero'],
                            success: function (result) {
                                notificacionCorrectoVar(result);
                                $('table').DataTable({
                                    "destroy": true,
                                    "language": {
                                        "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                                    },
                                    'ajax': {
                                        'method': 'POST',
                                        'url': json[0]['url_base'] + 'index.php/Semilleros_C/listar_ajax'

                                    },
                                    "columns": [
                                        { "data": "Nombre" },
                                        { "data": "Inscritos" },
                                        { "data": "Descripcion" },
                                        { "data": "Docente" },
                                        { "data": "Inscritos_Nombre" },
                                        { "data": "Eliminar" },
                                        { "data": "Modificar" }
                                    ]
                                });
                            }
                        });
                    }
                })
            } else {
                notificacionError();
            }
        }
    });

});

$(document).on('change', '#escenario', function (e) {
    e.preventDefault();
    if ($('#escenario').val() == '-1' || $('#escenario').val() == 'undefined') {
        $("#selectActividades").attr("class", "form-group has-error");
    } else {
        $("#selectActividades").attr("class", "form-group");
        $.ajax({
            url: base_url() + '/index.php/Escenario_C/listar_actividades_escenario/' + $('#escenario').val(),
            success: function (result) {
                var actividades = JSON.parse(result);
                if (actividades['data'][0]['nombre']) {
                    var select = '<label>Actividad(*)</label>';
                    select += ' <select id="actividades" name="actividad" class="form-control" style="width: 100%;">';
                    select += ' <option value="" hidden>Selecione una actividad</option>';                  
                    for (let index = 0; index < actividades['data'].length; index++) {
                        select += ' <option value="' + actividades['data'][index]['nombre'] + '">' + actividades['data'][index]['nombre'] + '</option>';
                    }
                    select += '</select>';
                    $("#selectActividades").html(select);
                }
                else {

                }
            }
        });
    }



});