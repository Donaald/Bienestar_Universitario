function base_url() {
    var base = window.location.href.split('/');
    return base[0] + '//' + base[2] + '/' + base[3];
}


$(document).ready(function () {

    $("select#cursosLibresDocente").change(function () {
        $('table').DataTable({
            "destroy": true,
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            },
            'ajax': {
                'method': 'POST',
                'url': base_url() + '/index.php/CursoLibre_C/estudiantesCursoLibre/' + $('select#cursosLibresDocente').val()
            }
        });
    });

    $(document).on('click', '#eliminarParticipante', function (e) {
        e.preventDefault();
        var url_listar = $(this).attr('href');

        $.ajax({
            url: url_listar,
            type: 'POST',
            success: function (resultado) {
                var datosConsulta = JSON.parse(resultado);
                swal({
                    html: 'Esta seguro que desea eliminar a: <b>' + datosConsulta['nombre'] + '</b> del curso libre?<br><br>',
                    confirmButtonText: 'Eliminar!',
                    confirmButtonClass: 'btn btn-danger',
                    showCancelButton: true,
                    cancelButtonText: 'Cancelar',
                    cancelButtonClass: 'btn btn-link',
                    type: "warning",
                    buttonsStyling: false
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: base_url() + '/index.php/CursoLibre_C/eliminarParticipanteCursoLibre/' + datosConsulta['idUsuario'] + '/' + datosConsulta['idCursoLibre'],
                            type: 'POST',
                            success: function (resultado) {
                                if (resultado !== '-1') {
                                    notificacionCorrectoVar(datosConsulta['nombre']);
                                    $('table').DataTable({
                                        "destroy": true,
                                        "language": {
                                            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                                        },
                                        'ajax': {
                                            'method': 'POST',
                                            'url': base_url() + '/index.php/CursoLibre_C/estudiantesCursoLibre/' + $('select#cursosLibresDocente').val()
                                        }
                                    });

                                } else {
                                    notificacionErrorVar(datosConsulta['nombre']);
                                }
                            }
                        });
                    }
                })
            }
        });

        return false;
    });
});