

$(document).on('change', '#escenario', function (e) {
    if ($('#escenario').val() == '-1' || $('#escenario').val() == 'undefined') {
        $("#contenedorlistaescenario").attr("class", "form-group has-error");
    } else {
        $("#contenedorlistaescenario").attr("class", "form-group");
        $.ajax({
            url: base_url() + '/index.php/Escenario_C/listar_id_escenario/' + $('#escenario').val(),
            success: function (resultado) {
                if (resultado != '-1') {
                    var json = JSON.parse(resultado);
                    var cadena = '<div class="box-body no-padding">';
                    cadena += '<div class="box box-solid with-border">'
                    cadena += '<div class="box-body">'
                    cadena += '<dl class="dl-horizontal">';
                    cadena += '<dt>Fecha:</dt>';
                    cadena += '<dd>' + json[0]['fecha'] + '</dd>';
                    cadena += '<dt>Duracion:</dt>';
                    cadena += '<dd>' + json[0]['duracion'] + '</dd>';
                    cadena += '<dt>Lugar:</dt>';
                    cadena += '<dd>' + json[0]['lugar'] + '</dd>';
                    cadena += '<dt>Justificacion:</dt>';
                    cadena += '<dd>' + json[0]['justificacion'] + '</dd>';
                    cadena += '</dl>';
                    cadena += '</div>';
                    cadena += '</div>';
                    cadena += '</div>';
                    $('#infoEscenario').html(cadena);
                }
            }
        });
        $('.table').DataTable({
            "destroy": true,
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            },
            'ajax': {
                'method': 'POST',
                'url': base_url() + '/index.php/Escenario_C/listar_actividades_escenario/' + $('#escenario').val(),
            },
            "columns": [
                { "data": "nombre" },
                { "data": "duracion" },
                { "data": "descripcion" },
                { "data": "fecha" },
                { "data": "hora" },
                { "data": "lugar" },
                { "data": "publicoObjetivo" },
                { "data": "eliminar" }
            ]
        });

    }

});

$(document).on('click', '.eliminarEscenario', function (e) {
    e.preventDefault();
    if ($('#escenario').val() == '-1' || $('#escenario').val() == 'undefined') {
        $("#contenedorlistaescenario").attr("class", "form-group has-error");
    } else {
        $("#contenedorlistaescenario").attr("class", "form-group");
        $.ajax({
            url: base_url() + '/index.php/Escenario_C/listar_id_escenario/' + $('#escenario').val(),
            success: function (resultado) {
                if (resultado != '-1') {
                    var json = JSON.parse(resultado);
                    swal({
                        title: '¿Esta Seguro?',
                        text: 'Se eliminara ' + json[0]['nombre'] + '!',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Confirmar',
                        cancelButtonText: 'Cancelar',
                        confirmButtonClass: 'btn btn-block btn-danger',
                        cancelButtonClass: 'btn btn-block btn-link',
                        buttonsStyling: false
                    }).then((result) => {
                        if (result.value) {
                            $.ajax({
                                url: base_url() + '/index.php/Escenario_C/eliminar/' + json[0]['idescenario'],
                                success: function (result) {
                                    if (result == 'correcto') {
                                        notificacionCorrectoVar(json[0]['nombre']);
                                        $.ajax({
                                            url: base_url() + '/index.php/Escenario_C/listar_ajax',
                                            success: function (result) {
                                                if (result != '-1') {
                                                    var jsonE = JSON.parse(result);
                                                    var select = ' <option value="-1" hidden>Selecione un Escenario</option>';
                                                    for (let index = 0; index < jsonE.length; index++) {
                                                        select += ' <option value="' + jsonE[index]['idescenario'] + '">' + jsonE[index]['nombre'] + '</option>';
                                                    }
                                                    $('#escenario').html(select);
                                                } else {
                                                    notificacionErrorVar(json[0]['nombre']);
                                                }
                                            }
                                        });
                                        $('.table').DataTable({
                                            "destroy": true,
                                            "language": {
                                                "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                                            },
                                            "columns": [
                                                { "data": "nombre" },
                                                { "data": "duracion" },
                                                { "data": "descripcion" },
                                                { "data": "fecha" },
                                                { "data": "hora" },
                                                { "data": "lugar" },
                                                { "data": "publicoObjetivo" },
                                                { "data": "eliminar" }
                                            ],
                                            "data": {
                                                "nombre": " ",
                                                "duracion": " ",
                                                "descripcion": " ",
                                                "fecha": " ",
                                                "hora": " ",
                                                "lugar": " ",
                                                "publicoObjetivo": " ",
                                                "eliminar": " "
                                            }
                                        });
                                        $('#infoEscenario').html('');
                                    } else {
                                        notificacionErrorVar(json[0]['nombre']);
                                    }

                                }
                            });
                        }
                    })
                } else {
                    notificacionError();
                }
            }
        });

    }






});

$(document).on('click', '#agregarActividad', function () {
    if ($('#escenario').val() == '-1' || $('#escenario').val() == 'undefined') {
        $("#contenedorlistaescenario").attr("class", "form-group has-error");
    } else {
        $("#contenedorlistaescenario").attr("class", "form-group");
        $('#idEscenario_HIDDEN').val($('#escenario').val());
        $('#agregar-actividad').modal("show");
    }

});

$(document).on('click', '.tomarAsistencia', function () {
    $('#idactividad').val($(this).attr('href'));
    $.ajax({
        url: base_url() + '/index.php/Escenario_C/listarUsuarios/',
        success: function (resultado) {
            var jsonUsuarios = JSON.parse(resultado);
            alert(result);

        }
    });
});


$(document).on('submit', '#formularioTomarAsistencia', function (e) {
    e.preventDefault();
    $.ajax({
        url: $(this).attr("action"),
        type: $(this).attr("method"),
        data: $(this).serialize(),
        success: function (result) {
            if (result == 'correcto') {
                notificacionCorrecto();
                $('#conId').modal('toggle');
                $('#sinId').modal('toggle');
            } else {
                swal({
                    title: 'Error!',
                    html: result,
                    confirmButtonText: 'Correcto!',
                    confirmButtonClass: 'btn btn-warning btn-simple',
                    buttonsStyling: false,
                    type: 'error'
                }).catch(swal.noop)
            }
        }
    });
});


$(document).on('submit', '#formularioNuevaActividad', function (e) {
    e.preventDefault();
    $.ajax({
        url: $(this).attr("action"),
        type: $(this).attr("method"),
        data: $(this).serialize(),
        success: function (result) {
            if (result == 'correcto') {
                notificacionCorrecto();
                $('#agregar-actividad').modal('toggle');
                $('#nombre').val('');
                $('#datepicker').val('');
                $('#lugar').val('');
                $('#justificacion').val('');
                $('#hora').val('');
                $('#publicoObjetivo').val('');
                $('.table').DataTable({
                    "destroy": true,
                    "language": {
                        "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                    },
                    'ajax': {
                        'method': 'POST',
                        'url': base_url() + '/index.php/Escenario_C/listar_actividades_escenario/' + $('#escenario').val(),
                    },
                    "columns": [
                        { "data": "nombre" },
                        { "data": "duracion" },
                        { "data": "descripcion" },
                        { "data": "fecha" },
                        { "data": "hora" },
                        { "data": "lugar" },
                        { "data": "publicoObjetivo" },
                        { "data": "eliminar" }
                    ]
                });
                $('#idEscenario_HIDDEN').val('');
            } else {
                swal({
                    title: 'Error!',
                    html: result,
                    confirmButtonText: 'Correcto!',
                    confirmButtonClass: 'btn btn-warning btn-simple',
                    buttonsStyling: false,
                    type: 'error'
                }).catch(swal.noop)
            }
        }
    });
});







$(document).on('click', '#deleteActividad', function (e) {
    e.preventDefault();
    var url_listarActividadID = $(this).attr('href');
    var tabla = $(this).val();
    $.ajax({
        url: url_listarActividadID,
        success: function (resultado) {
            if (resultado != '-1') {
                var json = JSON.parse(resultado);
                swal({
                    title: '¿Esta Seguro?',
                    text: 'Se eliminara ' + json[0]['nombre'] + '!',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Confirmar',
                    cancelButtonText: 'Cancelar',
                    confirmButtonClass: 'btn btn-block btn-danger',
                    cancelButtonClass: 'btn btn-block btn-link',
                    buttonsStyling: false
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url: base_url() + '/index.php/Escenario_C/eliminarActividad/' + json[0]['idactividad'],
                            success: function (result) {
                                if (result == 'correcto') {
                                    notificacionCorrectoVar(json[0]['nombre']);
                                    $('.table').DataTable({
                                        "destroy": true,
                                        "language": {
                                            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                                        },
                                        'ajax': {
                                            'method': 'POST',
                                            'url': base_url() + '/index.php/Escenario_C/listar_actividades_escenario/' + $('#escenario').val(),
                                        },
                                        "columns": [
                                            { "data": "nombre" },
                                            { "data": "duracion" },
                                            { "data": "descripcion" },
                                            { "data": "fecha" },
                                            { "data": "hora" },
                                            { "data": "lugar" },
                                            { "data": "publicoObjetivo" },
                                            { "data": "eliminar" }
                                        ]
                                    });
                                } else {
                                    notificacionErrorVar(json[0]['nombre']);;
                                }

                            }
                        });
                    }
                })
            } else {
                notificacionError();
            }
        }
    });
});




















