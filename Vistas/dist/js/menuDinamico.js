$(document).ready(function () {
    $.ajax({
        url: getUrl(true),
        success: function (result) {
            $("#menu").html(getMenu(result));
        }
    });
});

function getUrl(condicion) {
    if (condicion) {
        var urlCompleta = "" + window.location;
        var vectorURL = urlCompleta.split('/');
        return vectorURL[0] + '//' + vectorURL[1] + vectorURL[2] + '/' + vectorURL[3] + '/index.php/Menu_controller/getMenu';
    } else {
        var urlCompleta = "" + window.location;
        var vectorURL = urlCompleta.split('/');
        return vectorURL[0] + '//' + vectorURL[1] + vectorURL[2] + '/' + '/index.php/Menu_controller/getMenu';
    }
}

function getMenu(datos) {

    var menuJSON = JSON.parse(datos);
    var cadenaMenu = '';

    for (var j = 1; j <= Object.keys(menuJSON).length; j++) {
        if (menuJSON[j].item == 0) {
            if (menuJSON[j].direccion != '#') {
                cadenaMenu += '<li id=' + menuJSON[j].idItem + ' ><a href=' + menuJSON[j].direccion + '><i class="' + menuJSON[j].icono + '"></i><span>' + menuJSON[j].nombre + '</span></a></li>';
                $('#menu').html(cadenaMenu);
            } else {
                cadenaMenu += '<li class="treeview"><a href="' + menuJSON[j].direccion + '"><i class="' + menuJSON[j].icono + '"></i><span>' + menuJSON[j].nombre + '</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a><ul id=' + menuJSON[j].idItem + ' class="treeview-menu"></ul></li> ';
                $('#menu').html(cadenaMenu);
            }
        } else {
            if (menuJSON[j].direccion == '#') {
                $("#" + menuJSON[j].item + "").append('<li class="treeview"><a href="' + menuJSON[j].direccion + '"><i class="' + menuJSON[j].icono + '"></i><span>' + menuJSON[j].nombre + '</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a><ul id=' + menuJSON[j].idItem + ' class="treeview-menu"></ul></li>');
            } else {
                $("#" + menuJSON[j].item + "").append('<li><a href="' + menuJSON[j].direccion + '"><i class="' + menuJSON[j].icono + '"></i>' + menuJSON[j].nombre + '</a></li>');
            }
        }
    }
}