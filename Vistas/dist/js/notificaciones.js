function notificacionCorrectoVar(informacion) {
    $.notify({
        title: '<strong>Hecho!</strong>',
        message: 'Se realizo la operacion con exito sobre:' + informacion

    }, {
        newest_on_top: true,
        showProgressbar: true
    });
}
function notificacionCorrecto() {
    $.notify({
        title: '<strong>Hecho!</strong>',
        message: 'Se realizo la operacion con exito!'

    }, {
        newest_on_top: true,
        showProgressbar: true        
    });
}
function notificacionErrorVar(informacion) {
    $.notify({
        title: '<strong>Error!</strong>',
        message: 'Ha ocurrido un error en el proceso realizado sobre:' + informacion

    }, {
        newest_on_top: true,
        showProgressbar: true,
        type: 'error'
        
    });
}
function notificacionError() {
    $.notify({
        title: '<strong>Error!</strong>',
        message: 'No se realizo la operacion con exito!'

    }, {
        newest_on_top: true,
        showProgressbar: true,
        type: 'error'
    });
}

function verificacion(texto, url_id) {
    swal({
        title: '¿Estas Seguro?',
        text: 'Se eliminara <b>' + texto + '</b>!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Confirmar',
        cancelButtonText: 'Cancelar',
        confirmButtonClass: 'btn btn-block btn-danger',
        cancelButtonClass: 'btn btn-block btn-link',
        buttonsStyling: false
    }).then((result) => {
        if (result.value) {
            ajax_id(url_id);
        }
    })
}