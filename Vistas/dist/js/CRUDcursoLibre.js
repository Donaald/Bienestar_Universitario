$(document).ready(function () {
    // INICIO

    // Funcion eliminar Curso libre en la cual se realiza primero una peticion ajax para capturar los datos y verificar la selecion, luego se procede a realizar la eliminacion via ajax y retornando una notificacion de exito.
    $(document).on('click', '[id*=deleteCursoLibre]', function (e) {
        e.preventDefault();
        var url_id = $(this).attr('href');
        $.ajax({
            url: url_id,
            success: function (result) {
                if (result != '-1') {
                    var json = JSON.parse(result);
                    swal({
                        title: '¿Esta Seguro?',
                        text: 'Se eliminara ' + json[0]['nombre'] + '!',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Confirmar',
                        cancelButtonText: 'Cancelar',
                        confirmButtonClass: 'btn btn-block btn-danger',
                        cancelButtonClass: 'btn btn-block btn-link',
                        buttonsStyling: false
                    }).then((result) => {
                        if (result.value) {
                            $.ajax({
                                url: json[0]['url_base'] + 'index.php/CursoLibre_C/eliminar/' + json[0]['idCurso_libre'],
                                success: function (result) {
                                    notificacionCorrectoVar(result);
                                    $('table').DataTable({
                                        "destroy": true,
                                        "language": {
                                            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                                        },
                                        'ajax': {
                                            'method': 'POST',
                                            'url': json[0]['url_base'] + 'index.php/CursoLibre_C/listar_ajax'

                                        }
                                    });
                                }
                            });
                        }
                    })
                } else {
                    notificacionError();
                }
            }
        });

    });




    // funcion en la cual se realiza la peticion de la informacion que sera recomendada en los imput del modal que se invoca en esta misma funcion.
    var urlBase = '';
    $(document).on('click', '[id*=updateCursoLibre]', function (e) {
        e.preventDefault();
        var url_id = $(this).attr('href');
        $.ajax({
            url: url_id,
            success: function (result) {
                if (result != '-1') {
                    var valoresCursoLibre = JSON.parse(result);
                    $("#nombre").val(valoresCursoLibre[0]['nombre']);
                    $("#cantidad_cupos").val(valoresCursoLibre[0]['cupo_maximo']);
                    $("#cantidad_horas").val(valoresCursoLibre[0]['duracion']);
                    $("#Contenido_Tematico").val(valoresCursoLibre[0]['contenido_tematico']);
                    $("#idCusoLibre").val(valoresCursoLibre[0]['idCurso_libre']);
                    urlBase = valoresCursoLibre[0]['url_base'];
                    $.ajax({
                        url: valoresCursoLibre[0]['url_base'] + 'index.php/CursoLibre_C/obtenerDocentes',
                        success: function (result) {
                            var valoresDocentes = JSON.parse(result);
                            var cadena = '<select name="docente" class="form-control select2" style="width:100%;">';
                            for (var i = 0; i < valoresDocentes.length; i++) {
                                if (valoresDocentes[i].nombre + ' ' + valoresDocentes[i].apellido == valoresCursoLibre[0].nombre_docente + ' ' + valoresCursoLibre[0].apellido_docente) {
                                    cadena += ' <option selected value=' + valoresDocentes[i].idUsuario + '>' + valoresDocentes[i].nombre + ' ' + valoresDocentes[i].apellido + '</option>';
                                } else {
                                    cadena += ' <option value=' + valoresDocentes[i].idUsuario + '>' + valoresDocentes[i].nombre + ' ' + valoresDocentes[i].apellido + '</option>';
                                }
                            }
                            cadena += '</select>';
                            $('#docente').html(cadena);
                            $('.select2').select2();
                        }
                    });
                } else {
                    notificacionError();
                }
            }
        });
        $('#modal_modificar').modal('show');
        return false;
    });


    // luego de haber realizado la funcion inmediatamente anterior se procede a confirmar la informacion a modificar y se realiza via ajax para luego realizar la modificacion de los datos.
    $(document).on('submit', '#formularioModificar', function (e) {
        e.preventDefault();
        swal({
            title: '¿Esta Seguro?',
            text: 'Se modificara la informacion del curso libre selecionado',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Confirmar',
            cancelButtonText: 'Cancelar',
            confirmButtonClass: 'btn btn-block btn-info',
            cancelButtonClass: 'btn btn-block btn-link',
            buttonsStyling: false
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: $(this).attr("action"),
                    type: $(this).attr("method"),
                    data: $(this).serialize(),
                    success: function (result) {
                        if (result == 'correcto') {
                            notificacionCorrecto();
                            $('#modal_modificar').modal('toggle');
                            $('table').DataTable({
                                "destroy": true,
                                "language": {
                                    "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                                },
                                'ajax': {
                                    'method': 'POST',
                                    'url': urlBase + 'index.php/CursoLibre_C/listar_ajax'
                                }
                            });
                        } else {
                            if (result == '-1') {
                                notificacionError();
                            } else {
                                swal({
                                    title: 'Error',
                                    html: result,
                                    type: 'error',
                                    confirmButtonText: 'Confirmar',
                                    confirmButtonClass: 'btn btn-danger',
                                    buttonsStyling: false
                                })
                            }
                        }
                    }
                });
            }
        })
    });


    // FIN 
});